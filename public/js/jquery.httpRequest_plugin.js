/*CREADO POR: DARWIN EDER QUINTANA CHALA
 * FECHA CREACION: 13 DE JULIO DE 2020
 * */
(function ($) {
    $.fn.extend({
        /*METODO QUE AUTOMATIZA LA CREACION DE LOS DATATABLES*/
        drawDataTable: function (data, header, createRowCallback = function () {}, objectJson = {}, objectParametros = {}) {

            var defaultParametros = {
                'paging':   true,
                'ordering': true
            };

            /*PARAMETRO POR EL CUAL ENVIAN LOS DATOS PARA EL ORDEN DEL DATATABLE POR DEFECTO ES TRUE*/
            var defaultJson = {
                'orderColumn': 0,
                'order': 'desc'
            };

            var order_option = [];

            var cantidadDatos = Object.keys(objectJson).length; //CONTAMOS LA CANTIDAD DE DATOS QUE TIENE EL JSON

            if(cantidadDatos > 0) {
                /*RECORREMOS EL JSON ENVIADO POR EL PROGRAMADOR Y REEMPLAZAMOS LOS VALORES ENVIADOS CON LOS QUE ESTAN POR DEFECNTO
                * EN EL JSON defaultJson*/
                Object.keys(objectJson).forEach(function (idx) {
                    if(defaultJson.hasOwnProperty(idx)){
                        defaultJson[idx] = objectJson[idx];
                    }
                });
                if(defaultJson.hasOwnProperty('orderColumn') && defaultJson.orderColumn != '' && defaultJson.hasOwnProperty('order') && defaultJson.order != ''){
                    order_option = [[defaultJson.orderColumn, defaultJson.order]];
                }
            }

            var cantidadDatos2 = Object.keys(objectParametros).length; //CONTAMOS LA CANTIDAD DE DATOS QUE TIENE EL JSON
            if(cantidadDatos2 > 0) {
                Object.keys(objectParametros).forEach(function (idx) {
                    if (defaultParametros.hasOwnProperty(idx)) {
                        defaultParametros[idx] = objectParametros[idx];
                    }
                });
            }

            /*
            Se inicializa esta variable para la opcion de order vacio. Si viene la variable defaultJson con las propiedades orderColumn
            y order y sus respectivos valores no estan vacios, se define la ordenación. Si vienen definidos pero sus valores estan vacios,
            se deja la variable order_option vacio y asi se desactiva la ordenacion automatica por una columna
            */

            if(defaultJson.hasOwnProperty('orderColumn') && defaultJson.orderColumn != '' && defaultJson.hasOwnProperty('order') && defaultJson.order != ''){
                order_option = [[defaultJson.orderColumn, defaultJson.order]];
            }else{
                order_option = [[defaultJson.orderColumn, defaultJson.order]];
            }


            return $(this).DataTable({
                'aaData': data,
                'aoColumns': header,
                'language': { 'url': '../js/json/languageDatatable.json' },
                'processing': true,
                'serverSide': false,
                'deferLoading': 57,
                'bDestroy': true,
                'responsive': true,
                'ordering': defaultParametros.ordering,
                'order': order_option,
                'paging': defaultParametros.paging,
                'createdRow': function (row, data) {
                    $(row).attr('id', 'tr_' + data.id); // adicionamos valor a cada tr de la tabla
                    var idxVal = 0;
                    $.each(row.children, function (idx) { // adicionamos valor a los td de cada uno de los tr
                        $(row.children[idx]).attr('id', 'tr_' + data.id + '_td_' + idxVal);
                        idxVal++;
                    });
                },
                'initComplete': function(settings, json) { // CUANDO TERMINO DE CARGAR O PINTAR EL DATATABLE
                    $('[data-toggle="tooltip"]').tooltip();
                    $('[data-toggle="popover"]').popover();
                    return createRowCallback(); //DEVOLVEMOS UNA FUNCION PARA QUE EL DESARROLLADOR PUEDA HACER LO QUE NECESITE
                }
            });
        }
        /*FIN DEL METODO QUE AUTOMATIZA LA CREACION DE LOS DATATABLES*/
    });
})(jQuery);
/**
 * httpRequest
 * Método para la automatizacion de las peticiones AJAX
 * @author DARWIN EDER QUINTANA CHALA
 * @param string $url url de la peticion AJAX
 * @param json $objectJson (opcional) default {}
 * Attr Obj Json
 *   string method Método de la requisicion AJAX (POST, GET, etc...)
 *   json dataJson parametros que se van a enviar
 *   string dataType Tipo de la peticion AJAX (json, html, etc...)
 *   string msgNotification mensaje para mostrar en caso de exito de la petición (Opcional)
 *   string msgInfoNotification mensaje para mostrar en caso de solo informacion de la petición (Opcional)
 *   boolean hideTagError Bandera para indicar si se oculta el tag de mensajes de error (Opcional)
 *   boolean progressbar Bandera para indicar si se muestra la barra de progreso (Opcional)
 *   boolean blockUI Bandera para indicar si se muestra muestra la pantalla flotante de bloqueo (Opcional)
 * @param function $beforeSendCallback (opcional) default function () {}
 * @param function $callback (opcional) default function () {}
 */
jQuery.httpRequest = function (url, objectJson = {}, beforeSendCallback = function () {}, callback = function () {}) {
    /*PARAMETRIZAMOS LOS ELEMENTOS DEL OBJECTJSON PARA QUE NO SEA OBLIGATORIO Y CONTENGA UNOS DATOS POR DEFECTO*/
    /*JSON QUE CONTIENE LOS ELEMENTOS QUE VAMOS A MENJAR POR DEFECTO EN NUESTRAS PETICIONES*/
    var defaultJson = {
        'method' : 'GET', // METODO A UTILIZAR EN EL AJAX
        'dataJson': {}, // LOS DATOS QUE SE VAN A ENVIAR
        'dataType': 'json', // TIPO DE DATOS QUE NECESITAMOS QUE RETORNE LA PETICION
        'msgNotification': '', // EL MENSAJE DE NOTIFICACION SATISFACTORIO A MOSTRAR EN PANTALLA
        'msgInfoNotification': '', // EL MENSAJE DE NOTIFICACION INFORMATIVO A MOSTRAR EN PANTALLA
        'hideTagError': '', // ETIQUETA A OCULTAR DESPUES DE LA PETICION
        'progressbar': false, // BARRA PROGRASIVA QUE SE EJECUTARA CUANDO SEA TRUE Y ESTE MISMO VALOR LO CONTENGA EL ELEMENTO blockUI YA QUE SIN EL NO SE EJECUTARA
        'blockUI': false // PANTALLA DE BLOQUEO AL MOMENTO DE HACER LA PETICION
    };
    var cantidadDatos = Object.keys(objectJson).length; //CONTAMOS LA CANTIDAD DE DATOS QUE TIENE EL JSON
    if(cantidadDatos > 0) {
        /*RECORREMOS EL JSON ENVIADO POR EL PROGRAMADOR Y REEMPLAZAMOS LOS VALORES ENVIADOS CON LOS QUE ESTAN POR DEFECNTO
         * EN EL JSON defaultJson*/
        Object.keys(objectJson).forEach(function (idx) {
            if(defaultJson.hasOwnProperty(idx)){
                defaultJson[idx] = objectJson[idx];
            }
        });
    }
    /*FIN DE PARAMETRIZAR LOS ELEMENTOS DEL OBJECTJSON PARA QUE NO SEA OBLIGATORIO Y CONTENGA UNOS DATOS POR DEFECTO*/
    $.ajax({
        url: url, //Atributo que contiene la url
        method: defaultJson.method, //Atributo que contiene el metodo de requisicion
        data: defaultJson.dataJson, //Atributo que contiene los parametros de la requisicion
        dataType: defaultJson.dataType,//Atributo que contiene el tipo de respuesta
        async: true, //NO OBLIGAMOS A QUE EL DESARROLLO SE DETENGA HASTA QUE ESTE SE EJECUTE
        xhr: function () {
            var xhr = new window.XMLHttpRequest(); // creo el objeto XMLHttpRequest
            //Si el atributo progressbar viene definido como true, activamos la barra de progreso en la requisicion
            if(defaultJson.progressbar === true){
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        $('.progress-bar').css({
                            width: percentComplete * 100 + '%'
                        });
                        $('.progress-bar > span').text(percentComplete * 100 + '%');
                        if (percentComplete === 1) {
                            setTimeout(function(){
                                $.unblockUI(); //DESBLOQUEA LA PANTALLA TRANSPARENTE DEL blockUI
                            },1000); //Se retrasa el cierra de la capa flotante para que rimero se complete totalmente el proceso
                        }
                    }
                }, false);
                xhr.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        $('.progress-bar').css({
                            width: percentComplete * 100 + '%'
                        });
                        $('.progress-bar > span').text(percentComplete * 100 + '%');
                        if (percentComplete === 1) {
                            setTimeout(function(){
                                $.unblockUI(); //DESBLOQUEA LA PANTALLA TRANSPARENTE DEL blockUI
                            },1000); //Se retrasa el cierra de la capa flotante para que rimero se complete totalmente el proceso
                        }
                    }
                }, false);
            }
            return xhr;
        },
        beforeSend: function (xhr) { //Funcion para enviar js antes de la requisicion
            if(defaultJson.blockUI === true){ // Si el atributo blockUI viene definido en true, se muestra la pantalla transparente de bloqueo de pantalla
                if(defaultJson.progressbar === true){ //Definimos el div para la barra de progreso en caso de que el atributo progressbar este definido
                    $.blockUI({
                        message:'<div class="progress">' +
                                    '<div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">' +
                                        '<span>0%</span>' +
                                    '</div>' +
                                '</div>',
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#FFF','-webkit-border-radius': '10px','-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });
                }
                else{ // Si no viene definido el atributo progressbar, se cargan una imagen loading normal.
                    $.blockUI({
                        message: '<div id="div_spin"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><br><span>Cargando...</span></div>',
                        css: { border: 'none',
                            padding: '15px',
                            backgroundColor: '#000','-webkit-border-radius': '10px','-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });
                }
            }
            return beforeSendCallback(xhr); // ESTA OPCION LO QUE RETORNA ES PARA QUE EL PROGRAMADOR LANZE ALGO ANTES DE EJECUTARSE UNA RESPUESTA
        },
        success: function (data) {
            if(defaultJson.dataType === 'json'){
                var cantidadDatos = 0;
                if(data.data){
                    cantidadDatos = Object.keys(data.data).length; //CONTAMOS LA CANTIDAD DE DATOS QUE TIENE EL JSON
                }
                if(cantidadDatos === 0){
                    //Si esta definido el atributo msgInfoNotification, mostramos el mensaje de exito
                    if(defaultJson.hasOwnProperty('msgInfoNotification') && defaultJson.msgInfoNotification !== ''){
                        alertify.info(defaultJson.msgInfoNotification);
                    }
                    if(defaultJson.hasOwnProperty('hideTagError') && defaultJson.hideTagError !== ''){
                        $(defaultJson.hideTagError).hide();
                    }
                }else{
                    //Si esta definido el atributo msgNotification, mostramos el mensaje de exito
                    if(defaultJson.hasOwnProperty('msgNotification') && defaultJson.msgNotification !== ''){
                        alertify.success(defaultJson.msgNotification);
                    }
                }
            }else{
                //Si esta definido el atributo msgNotification, mostramos el mensaje de exito
                if(defaultJson.hasOwnProperty('msgNotification') && defaultJson.msgNotification !== ''){
                    alertify.success(defaultJson.msgNotification);
                }
            }
            if(defaultJson.blockUI === true){
                setTimeout(function(){
                    $.unblockUI(); //DESBLOQUEA LA PANTALLA TRANSPARENTE DEL blockUI
                },1000); //Se retrasa el cierra de la capa flotante para que rimero se complete totalmente el proceso
            }
            return callback(data); // RETORNAMOS LOS DATOS EN UN CALLBACK PARA QUE EL PROGRAMADOR TENGA CONTROL ABSOLUTO SOBRE LA RESPUESTA
        }
    }).fail( function( jqXHR, textStatus, errorThrown ) { // MANEJO DE ERRORES HTTP EN LAS PETICIONES
        if(defaultJson.hasOwnProperty('hideTagError') && defaultJson.hideTagError !== ''){
            $(defaultJson.hideTagError).hide();
        }
        if(defaultJson.blockUI === true){
            $.unblockUI(); //DESBLOQUEA LA PANTALLA TRANSPARENTE DEL blockUI
        }
        if(jqXHR.status === 500){ // ERROR 500: INTERNAL SERVER
            alertify.error("ERROR " + jqXHR.status + ": Error interno del servidor ... Verifique con el administrador del sistema");
        }
        else if(jqXHR.status === 403){ // ERROR 302: USUARIO NO TIENE PERMISOS SOBRE EL METODO QUE ESTA TRATANDO DE ACCIONAR
            alertify.error("ERROR " + jqXHR.status + ": El usuario no tiene permisos");
            //window.location.href = '/home';
        }
        else if(jqXHR.status === 401){ // ERROR 401: USUARIO NO ESTA LOGEADO
            alertify.error("ERROR " + jqXHR.status + ": El usuario no esta logeado");
            setTimeout(function () {
                window.location.href = '/';
            }, 3000);
        }
        else if(jqXHR.status === 404){ // ERROR 404: PAGINA NO ENCONTRADA
            alertify.error("ERROR " + jqXHR.status + ": Pagina no encontrada ... la url a la que intenta ingresar es: " + url);
        }
        else if(jqXHR.status === 422){ // ERROR 422: ENTIDAD SIN PROCESAR --- para el validator de laravel
            var jsonResponseText = $.parseJSON(jqXHR.responseText);
            messagge_error = "";
            $.each(jsonResponseText, function(name, val) {
                messagge_error += '<br><br>El campo ' + name + ' : ' + val;
            });
            alertify.error("ERROR " + jqXHR.status + ": ENTIDAD SIN PROCESAR" + messagge_error);
        }else {
            return callback(jqXHR.status);
        }
    });
};
/*FIN DE LA AUTOMATIZACION DE LAS PETICIONES AJAX*/