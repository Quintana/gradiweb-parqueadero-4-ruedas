<?php
/*
if (env('APP_ENV') == 'production'){
    //$binaryPdf = base_path('public/library/wkhtmltopdf_linux/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64');
    //$binaryImage = base_path('public/library/wkhtmltopdf_linux/wkhtmltoimage-amd64/bin/wkhtmltoimage-amd64');

    $binaryPdf = base_path('public/library/wkhtmltopdf_linux/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64');
    $binaryImage = base_path('public/library/wkhtmltopdf_linux/wkhtmltoimage-amd64/bin/wkhtmltoimage-amd64');
}else{
    //Local para los equipos de 86x;
    if(file_exists('C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe')){
        $binaryPdf = '"C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf"';
        $binaryImage = '"C:\Program Files\wkhtmltopdf\bin\wkhtmltoimage"';
    }
    else{
        $binaryPdf = base_path('public/library/wkhtmltopdf/bin/wkhtmltopdf.exe');
        $binaryImage = base_path('public/library/wkhtmltopdf/bin/wkhtmltoimage.exe');
    }

}
*/

/* ASI ESTABA EN EL SERVIDOR Y ESTA FUNCIONANDO YA QUE EL INFORME LO ESTA GENERANDO SIN PROBLEMA ALGUNO * */
if (env('APP_ENV') == 'production'){
    //$binaryPdf = base_path('public/library/wkhtmltopdf_linux/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64');
    //$binaryImage = base_path('public/library/wkhtmltopdf_linux/wkhtmltoimage-amd64/bin/wkhtmltoimage-amd64');

    $binaryPdf = base_path('vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64');
    $binaryImage = base_path('vendor/h4cc/wkhtmltoimage-amd64/bin/wkhtmltoimage-amd64');
}else{
    //Local para los equipos de 86x;
    if(file_exists('C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe')){
        $binaryPdf = '"C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf"';
        $binaryImage = '"C:\Program Files\wkhtmltopdf\bin\wkhtmltoimage"';
    }
    else{
        $binaryPdf = base_path('public/library/wkhtmltopdf/bin/wkhtmltopdf.exe');
        $binaryImage = base_path('public/library/wkhtmltopdf/bin/wkhtmltoimage.exe');
    }

}



return array(

    'pdf' => array(
        'enabled' => true,
        //esta ruta es para cuando se trabaja en win
        //'binary'  => base_path('public/library/wkhtmltopdf/bin/wkhtmltopdf.exe'),
        //esta ruta es para cuando se trabaja en ubuntu o linux
        //'binary' => base_path('vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'),
        'binary' => $binaryPdf,
        'timeout' => false,
        'options' => array(),
        'env'     => array(),
    ),
    'image' => array(
        'enabled' => true,
        //esta ruta es para cuando se trabaja en win
        //'binary'  => base_path('public/library/wkhtmltopdf/bin/wkhtmltoimage.exe'),
        //esta ruta es para cuando se trabaja en ubuntu o linux
        //'binary' => base_path(' vendor/h4cc/wkhtmltoimage-amd64/bin/wkhtmltoimage-amd64'),
        'binary' => $binaryImage,
        'timeout' => false,
        'options' => array(),
        'env'     => array(),
    ),


);
