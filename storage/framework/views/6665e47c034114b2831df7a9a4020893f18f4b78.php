<!-- Modal para la creacion y actualizacion de la tabla -->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
    <div class="modal-dialog modal-xl"  role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="col-md-12"></div>
            </div>

            <div class="modal-body">
                <div id="spinner" style="text-align: center">
                    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                    <span class="sr-only">Cargando...</span>
                </div>
                <div id="mostrar_info" style="display: none">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group col-md-3 ui-front">
                                <label for="nro_identificacion">Nro Identificaci&oacute;n <span style="color: red;">*</span></label>
                                <input type="text" id="nro_identificacion" name="nro_identificacion" placeholder="Nro de identificaci&oacute;n" class="form-control data-required2">
                                <input type="hidden" id="id_autocomplete" name="id_autocomplete" class="borrar data-required">
                            </div>

                            <div class="form-group col-md-3">
                                <label for="fk_id_tipo_identificacion">Tipo Identificaci&oacute;n <span style="color: red;">*</span></label>
                                <select id="fk_id_tipo_identificacion" name="fk_id_tipo_identificacion" class="form-control data-required2"></select>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="nombres">Nombres <span style="color: red;">*</span></label>
                                <input type="text" id="nombres" name="nombres" placeholder="Ingrese los nombres" class="form-control borrar data-required2">
                            </div>

                            <div class="form-group col-md-3">
                                <label for="apellidos">Apellidos <span style="color: red;">*</span></label>
                                <input type="text" id="apellidos" name="apellidos" placeholder="Ingrese los apellidos" class="form-control borrar data-required2">
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group col-md-3">
                                <label for="telefono">Tel&eacute;fono fijo o movil<span style="color: red;">*</span></label>
                                <input type="text" id="telefono" name="telefono" placeholder="Ingrese un tel&eacute;fono fijo" class="form-control borrar data-required2">
                            </div>

                            <div class="form-group col-md-3">
                                <label for="direccion">Direcci&oacute;n de residencia <span style="color: red;">*</span></label>
                                <input type="text" id="direccion" name="direccion" placeholder="Ingrese una direcci&oacute;n" class="form-control borrar data-required2">
                            </div>

                            <div class="form-group col-md-3">
                                <label for="temperatura">Temperatura <span style="color: red;">*</span></label>
                                <input type="text" id="temperatura" name="temperatura" placeholder="Ingrese la Temperatura" class="form-control borrar data-required2">
                            </div>

                            <div class="form-group col-md-3">
                                <label for="observaciones">Observaciones</label>
                                <textarea id="observaciones" name="observaciones" placeholder="Ingrese una Observaci&oacute;n" class="form-control borrar"></textarea>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer" id="footer">
                <button id="cerrar" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button class="btn btn-primary" id="guardar">Guardar</button>
                <input type="hidden" id="enviar_guardar">
                <?php echo e(csrf_field()); ?>

            </div>
        </div>
    </div>
</div>

<!-- fin Modal para la creacion y actualizacion de la informacion de la tabla -->

<!-- Modal para la Eliminacion de la informacion de la tabla -->

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="col-md-12"></div>
            </div>

            <div class="modal-body">
                <h2>Oprima el boton <strong>'Continuar'</strong> SI Desea Proceder con la eliminaci&oacute;n del registro?</h2>
            </div>

            <div class="modal-footer" id="footer2">
                <button id="cerrar" class="btn btn-default" data-dismiss="modal">Salir</button>
                <button class="btn btn-danger" id="eliminar" data-dismiss="modal">Continuar</button>
                <?php echo e(csrf_field()); ?>

            </div>
        </div>
    </div>
</div>
<!--FIN MODAL-->
