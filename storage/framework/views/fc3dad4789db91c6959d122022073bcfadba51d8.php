<script type="text/javascript">

    $(document).ready(function () {

        /*MODIFICAMOS EL LENGUAJE DEL DATEPICKER SOLO FALTA LLAMARLO EN LA CONFIGURACION DE CADA UNO DE LOS DATEPICKER*/
        $.fn.datepicker.dates['es'] = {
            days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
            daysShort: ["Dom", "Lun", "Mar", "Mier", "Jue", "Vie", "Sab"],
            daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
            today: "Hoy",
            clear: "Limpiar",
            weekStart: 0
        };

        /*DEFINIMOS LOS DATEPICKER*/
        $('.datepicker').datepicker({
            //startDate: '-3d',
            language: "es",
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        });

        $(document).on('click', '#mostrar_informe', function () {
            var validarDatos = true;

            $(".data-required").each(function(){
                if($(this).val() == ''){
                    validarDatos = false;
                }
            });
            /*SIN DE LA VALIDACION*/

            if(validarDatos === true) {
                window.open('/files/informe_pdf/' + $("#fecha_inicio").val() + "/" + $("#fecha_fin").val(), '_blank');
            }else{
                swal('Información', 'TODOS LOS CAMPOS SON OBLIGATORIOS', "info");
            }
        });

    });

</script>
