<script type="text/javascript">

    $(document).ready(function () {

        var json = []; // definimos la variable vacia que va a contener los datos para dibujar el datatable
        var id = ''; // definimos la variable que va a contener el id para la actualizacion del registro
        var idElimina = ''; // definimos la variable que va a contener el id para la eliminacion del registro

        function dibujarDatatable(data) {
            json = []; // siempre que se llame a esta funcion el va a limpiar la variable para volverla a llenar con la informacion de la tabla
            /*recorremos los y definimos los datos para dibujar en el datatable asi mismo creamos el objeto "opciones" que contiene las diferentes acciones que se van a realizar por cada registro*/
            $.each(data.data, function (idx, val) {
                json.push({
                    id: val.id_detalle,
                    nombre_completo: val.nombres + " " + val.apellidos,
                    nro_identificacion: val.sigla + ' - ' + val.nro_identificacion,
                    direccion: val.direccion,
                    telefono: val.telefono,
                    temperatura: val.temperatura,
                    fecha_ingreso: val.fecha_ingreso,
                    observaciones: val.observaciones,
                    opciones:   '<div class="form-inline">' +
                                    '<div data-toggle="modal" class="form-group" data-target="#deleteModal">' +
                                        '<button id="delete" class="delete btn btn-danger btn-circle btn-outline" data-toggle="tooltip"' +
                                            ' data-placement="top" title="Eliminar Registro"' +
                                            ' data-val="' + val.id_detalle + '">' +
                                            '<span class="fa fa-trash"></span>' +
                                        '</button>' +
                                        '</div>&nbsp;' +
                                    '</div>&nbsp;' +
                                '</div>'
                });
            });

            /*cabecera del datatable*/
            var columns = [
                {'data' : 'id'},
                {'data' : 'nombre_completo'},
                {'data' : 'nro_identificacion'},
                {'data' : 'direccion'},
                {'data' : 'telefono'},
                {'data' : 'temperatura'},
                {'data' : 'fecha_ingreso'},
                {'data' : 'observaciones'},
                {'data' : 'opciones'}
            ];

            /*llamamos el plugin para dibujar los datos en el datatable segun el id o class asignado*/
            $('#listado_aplicativo').drawDataTable(json, columns);
        }

        /*FUNCION PARA GENERAR EL LISTADO DE CONSULTA DE LA TABLA*/
        function listado() {
            /*LLAMAMOS EL PLUGIN Y LE PASAOS LOS PARAMETROS PARA DIBUJAR EL DATATABLE*/
            $.httpRequest('/query/listado/control_ingreso', {'blockUI': true}, function() {}, function (dataListado) {
                //console.log(dataListado);
                dibujarDatatable(dataListado); //enviamos los datos a la funcion anterior para procesar la informacion y mostrarla posteriormente
            });
        }

        listado(); // generamos la consulta para el listado de todos los datos de la tabla

        var contaMensaje = 0;

        /*ACTIVAMOS EL EVENTO CLICK DE LA CLASS "MOSTRAR" Y IDENTIFICAMOS SI ES PARA INSERTAR O PARA ACTUALIZAR UN REGITRO*/
        $(document).on('click', '.mostrar', function() {
            contaMensaje = 0;
            $('#spinner').show(); // activamos el spinner para que el usuario sepa que hay una carga de la informacion
            $('#mostrar_info').hide(); // ocultamos el div en donde se va a mostrar la informacion para actualizar o para insertar

            $.httpRequest('/query/listado/tipo_identificacion', {}, function() {}, function (dataTipoIdentificacion) {
                $('#fk_id_tipo_identificacion').empty();

                $('<option>').val('').text('Seleccione...').appendTo('#fk_id_tipo_identificacion');
                $.each(dataTipoIdentificacion.data, function (idx, val) {
                    $('<option>').val(val.id).text(val.sigla + ' - ' + val.descripcion).appendTo('#fk_id_tipo_identificacion');
                });

                var actualizaRegistro = $(this).data('val'); // variable para distinguir si es para actualizar o crear un registro

                // validamos que sea para actualizar la informacion sino entra al else para la insercion de la informacion
                if (actualizaRegistro) { // actualizamos la informacion
                    $('#enviar_guardar').val('act');
                    $.httpRequest('/query/listado/estados_generales/' + actualizaRegistro, {}, function () {}, function (dataEstadosGenerales) {
                        id = dataEstadosGenerales.data[0].id;
                        $('#spinner').hide();
                        $('#mostrar_info').show();
                        $('#descripcion').val(dataEstadosGenerales.data[0].nombre);
                    });
                } else { // insertamos la informacion
                    $('#enviar_guardar').val('new');
                    $('#spinner').hide();
                    $('#mostrar_info').show();
                    $('.borrar').val('');
                    $('#fk_id_tipo_identificacion > option[value="1"]').attr('selected', 'selected');
                }
            });
        });
        /*FIN DE ACTIVAMOS EL EVENTO CLICK DE LA CLASS "MOSTRAR" Y IDENTIFICAMOS SI ES PARA INSERTAR O PARA ACTUALIZAR UN REGITRO*/

        /*GUARDAMOS O  ACTUALIZAMOS LA INFORMACION*/
        $(document).on('click', '#guardar', function () {
            /*VALIDAMOS QUE TODOS LOS ELEMNTOS HTML QUE TENGAN LA CLASE 'data-required' CONTENGAN DATOS DE LOS CONTRARIO
            * NO PODRA ACTUALIZAR NI INSERTAR*/
            var validarDatos = true;

            $(".data-required2").each(function(){
                if($(this).val() == ''){
                    validarDatos = false;
                }
            });
            /*SIN DE LA VALIDACION*/

            if(validarDatos === true) {
                if ($('#enviar_guardar').val() === 'act') { // PROCESO DE ACTUALIZACION
                    /*json = {
                        '_token': $('#footer input[name=_token]').val(),
                        'data': {
                            'id': id,
                            'nombre': $('#descripcion').val(),
                        }
                    };
                    $.httpRequest('/query/control_ingreso', {'method': 'PATCH', 'dataJson': json}, function () {}, function (dataUpdated) {
                        /*LISTAMOS NUEVAMENTE EL DATATABLE
                        listado();
                        alertify.success(dataUpdated[0].msj);
                    });*/
                } else if ($('#enviar_guardar').val() === 'new') { // PROCESO PARA LA CREACION DE UN REGISTRO
                    json = {
                        '_token': $('#footer input[name=_token]').val(),
                        'data': {
                            'nombres': $('#nombres').val(),
                            'apellidos': $('#apellidos').val(),
                            'nro_identificacion': $('#id_autocomplete').val(),
                            'fk_id_tipo_identificacion': $('#fk_id_tipo_identificacion').val(),
                            'telefono': $('#telefono').val(),
                            'direccion': $('#direccion').val(),
                            'temperatura': $('#temperatura').val(),
                            'observaciones': $('#observaciones').val(),
                        }
                    };

                    $.httpRequest('/query/control_ingreso', {'method': 'POST', 'dataJson': json}, function () {}, function (dataInsert) {
                        /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                        listado();
                        alertify.success(dataInsert[0].msj);
                    });
                }
            }else{
                swal('Información', 'TODOS LOS CAMPOS SON OBLIGATORIOS', "info");
            }
        });
        /*FIN DE GUARDAMOS O  ACTUALIZAMOS LA INFORMACION*/

        /*ELIMINACION POR SOFTWARE DEL REGISTRO*/
        $(document).on('click', '.delete', function () {
            idElimina = $(this).data('val');
        });

        $(document).on('click', '#eliminar', function () {
            if(idElimina !== null){
                json = {
                    '_token': $('#footer2 input[name=_token]').val(),
                    'data': {
                        'id': idElimina
                    }
                };

                $.httpRequest('/query/control_ingreso', {'method': 'DELETE', 'dataJson': json}, function(){}, function (dataDelete) {
                    /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                    listado();
                    alertify.success(dataDelete[0].msj);
                });
            }
        });
        /*ELIMINACION POR SOFTWARE DEL REGISTRO*/



        /************************************AUTOCOMPLETAR PARA EL TERCERO************************************/
        var nombreCompleto = '';

        $('#nro_identificacion').autocomplete({
            source: function( request, response ) {
                $.ajax( {
                    url: "/query/control_ingreso/autocompletar",
                    method: "POST",
                    dataType: "json",
                    data: {'_token': "<?php echo e(csrf_token()); ?>", datos: request.term},
                    success: function( data ) {
                        $('#id_autocomplete').val("");
                        $('.borrar').val('');
                        $('#fk_id_tipo_identificacion option:selected').each(function () {
                            $(this).removeAttr('selected');
                        });
                        var dataAutocomplete = [];
                        if(data.data.length === 0) {
                            $("#id_autocomplete").val( $("#nro_identificacion").val() );
                            if (contaMensaje === 0) {
                                swal('Información', 'No existe ningún cliente con ese número de identificación, nombre o razón social que esta ingresando', "info");
                            }
                            contaMensaje++;
                        }else {
                            contaMensaje = 0;
                            $.each(data.data, function (idx, val) {
                                /*VALIDAMOS SI EN LA CONSULTA VIENE UN PARAMETRO LLAMADO VALUE
                                * ESTE PARAMETRO INDICA QUE LA CONSULTA NO ARROJO RESULTADOS*/

                                // EN CASO DE QUE SI EXISTA INFORMACION DEL TERCERO QUE SE ESTA CONSULTANDO
                                nombreCompleto = val.nombres + " " + val.apellidos;
                                dataAutocomplete.push(
                                    {
                                        id: val.id,
                                        value: val.nro_identificacion + ' | ' + nombreCompleto,
                                        fk_id_tipo_identificacion: val.fk_id_tipo_identificacion,
                                        nro_identificacion: val.nro_identificacion,
                                        nombres: val.nombres,
                                        apellidos: val.apellidos,
                                        telefono: val.telefono,
                                        direccion: val.direccion,
                                        temperatura: val.temperatura,
                                        observaciones: val.observaciones,
                                    }
                                );

                            });
                        }
                        response( dataAutocomplete );
                    }
                });
            },
            minLength: 1,
            select: function( event, ui ) {
                $('#fk_id_tipo_identificacion > option[value="' + ui.item.fk_id_tipo_identificacion + '"]').attr('selected', 'selected');
                $('#id_autocomplete').val(ui.item.nro_identificacion);
                $('#nro_identificacion').val(ui.item.nro_identificacion);
                $('#nombres').val(ui.item.nombres);
                $('#apellidos').val(ui.item.apellidos);
                $('#telefono').val(ui.item.telefono);
                $('#direccion').val(ui.item.direccion);
                $('#temperatura').val(ui.item.temperatura);
                $('#observaciones').val(ui.item.observaciones);
            }
        });
        /****************************** FIN DE AUTOCOMPLETAR PARA EL TERCERO**********************************/


        $(document).on('keyup', '#temperatura', function () {
            if(parseFloat($(this).val()) >= 38){
                swal('ATENCION!!!', 'LA TEMPERATURA INGRESADA ES MAYOR O IGUAL A 38, PORFAVOR VALIDAR PROTOCOLOS DE INGRESO DEL CIUDADANO', "warning");
            }
        });

    });

</script>
