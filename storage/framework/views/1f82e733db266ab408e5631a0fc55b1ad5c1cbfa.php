<div class="row">
    <div style="text-align: center">
        <i id="spinner_menu" class="fa fa-spinner fa-spin fa-5x"></i>
        <input type="hidden" id="accionar_menu" name="accionar_menu">
    </div>

    <div class="row col-md-12" id="crear_tree">

    </div>
</div>
<?php $__env->startSection('js_menu_usuario'); ?>
    <script type="text/javascript">
        var cargar_todo_el_menu = '';
        var cuenta_aplicativos = 0;
        var cuenta_modulos = 0;
        var cuenta_menu = 0;
        var jsonMenuUsuarios = []; // variable en donde vamos a almacenar el menu de cada uno de los usuarios
        var seleccionados = []; // variable que contiene los nodos seleccionados del arbol
        var adicionar_menu = '';
        var cuentaTree = 0; // variable que sirve para contar las veces que se va a dibujar el tree para asi mismo asignarle un consecutivo al id del div que se crea dinamicamente

        /*CREAMOS DINAMICAMENTE TODO EL APLICATIVO/MODULO/MENU*/
        /*ELIMINACION POR SOFTWARE DEL REGISTRO*/
        $(document).on('click', '#accionar_menu', function () {
            //$('#tree').jstree(true).deselect_node(seleccionados); // limpiamos el arbol de todos los nodos seleccionados

            //seleccionamos por defecto los nodos del tree que van a estar chequeados
            //$('#tree').jstree(true).select_node(['menu_2_metodo_post', 'menu_2_metodo_delete']);

            $.httpRequest('/query/listado/aplicativos', {}, function() {}, function (aplicativos) {
                $.httpRequest('/query/listado/modulos', {}, function() {}, function (modulos) {
                    $.httpRequest('/query/listado/menu', {}, function() {}, function (menu) {
                        $.httpRequest('/query/listado/asignacion_menu/' + adicionar_menu, {}, function() {}, function (menuPorId) {
                            $.each(aplicativos.data, function (idx, val) {
                                cargar_todo_el_menu += '<ul>';
                                    cargar_todo_el_menu += '<li id="aplicativo_'+val.id+'">' + val.descripcion;
                                        cargar_todo_el_menu += '<ul>';

                                        $.each(modulos.data, function (idx2, val2) {
                                            if(val.id === val2.fk_id_aplicativo) {
                                                cargar_todo_el_menu += '<li id="modulo_'+val2.id+'">' + val2.descripcion;
                                                    cargar_todo_el_menu += '<ul>';
                                                    $.each(menu.data, function (idx3, val3) {
                                                        if(val2.id === val3.id_modulo) {
                                                            cargar_todo_el_menu += '<li id="menu_'+val3.id+'">' + val3.descripcion;
                                                                cargar_todo_el_menu += '<ul>';
                                                                    cargar_todo_el_menu += '<li id="menu_'+val3.id+'_metodo_1"> INSERTAR </li>';
                                                                    cargar_todo_el_menu += '<li id="menu_'+val3.id+'_metodo_2"> ACTUALIZAR </li>';
                                                                    cargar_todo_el_menu += '<li id="menu_'+val3.id+'_metodo_3"> VISUALIZAR </li>';
                                                                    cargar_todo_el_menu += '<li id="menu_'+val3.id+'_metodo_4"> ELIMINAR </li>';
                                                                cargar_todo_el_menu += '</ul>';
                                                            cargar_todo_el_menu += '</li>';
                                                        }
                                                        cuenta_menu++;
                                                    });
                                                    cargar_todo_el_menu += '</ul>';
                                                cargar_todo_el_menu += '</li>';
                                            }
                                            cuenta_modulos++;
                                        });
                                        cargar_todo_el_menu += '</ul>';
                                    cargar_todo_el_menu += '</li>';
                                cargar_todo_el_menu += '</ul>';
                                cuenta_aplicativos++;
                            });
                            $('#crear_tree').empty().append('<div id="tree_'+cuentaTree+'" class="tree"></div>');
                            $('#tree_'+cuentaTree).empty().append(cargar_todo_el_menu);

                            /*INICIALIZAMOS EL TREEJS*/

                            $("#tree_"+cuentaTree).jstree({
                                "checkbox": {
                                    "keep_selected_style": false
                                },
                                "plugins": ["checkbox"]
                            });

                            var arregloDeMenuPorUsuario = [];
                            var permiso = '';
                            $.each(menuPorId.data, function (idx, val) {
                                arregloDeMenuPorUsuario.push('menu_' + val.fk_id_menu + '_metodo_' + val.fk_id_permiso);
                            });

                            $('#spinner_menu').hide();
                            $('.tree').show();

                            //seleccionamos por defecto los nodos del tree que van a estar chequeados
                            $('#tree_'+cuentaTree).jstree(true).deselect_node(seleccionados);
                            $('#tree_'+cuentaTree).jstree(true).select_node(arregloDeMenuPorUsuario);
                            cuentaTree++;
                            //$('#tree').jstree(true).deselect_node(seleccionados);
                        });
                    });
                });
            });
        });

        /*DETECTAMOS Y CAPTURAMOS EL NODO DEL ARBOL AL QUE HACEMOS CLICK*/
        $(document).on('click', '.tree', function () {
            $(this).bind("changed.jstree", function (e, data) {
                var menu = '';
                jsonMenuUsuarios = [];
                seleccionados = data.selected;
                $.each(data.selected, function (idx, val) {
                    menu = val.split('_');
                    if (menu[0] === 'menu' && menu.length === 4) {
                        jsonMenuUsuarios.push({
                            fk_id_usuario: adicionar_menu,
                            fk_id_menu: menu[1],
                            fk_id_permiso: menu[3]
                        });
                    }
                });
            });
        });

        /*GUARDAMOS LOS DATOS DE LA ASIGNACION DEL MENU A LOS USUARIOS*/
        $(document).on('click', '#asignar_menu', function () {
            $('#spinner_menu').show();
            $('.tree').hide();
            if(jsonMenuUsuarios.length === 0){
                alertify.warning('Debe seleccionar al menos una opcion del menu para poder continuar');
                $('#spinner_menu').hide();
                $('.tree').show();
            }else{
                var json = {
                    '_token': $('#footer2 input[name=_token]').val(),
                    'data': jsonMenuUsuarios
                };

                $.httpRequest('/query/asignacion_menu', {'method': 'POST', 'dataJson': json}, function(){}, function (data) {
                    /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                    alertify.success(data[0].msj);
                    $('#spinner_menu').hide();
                    $('.tree').show();
                });
            }
        });
    </script>
<?php $__env->stopSection(); ?>