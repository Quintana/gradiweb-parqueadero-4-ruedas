<div id="token">
    <input type="hidden" id="enviar_guardar">
    <?php echo e(csrf_field()); ?>

</div>
<div id="spinner_nueva_factura" style="text-align: center">
    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
    <span class="sr-only">Cargando...</span>
</div>

<div id="ver_datos_nueva_factura" style="display: none">
    <div class="row">
        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h4 class="text-on-pannel text-primary"><strong class="text-uppercase"> DATOS DEL CLIENTE O TERCERO </strong></h4>
                    <div class="row">
                        <div class="form-group col-md-2">
                            <label for="tercero_id">CEDULA O NIT DEL CLIENTE <span style="color: red;">*</span></label>
                            <input type="text" id="tercero_id" name="tercero_id" placeholder="Cedula o Nit" class="form-control borrar data-required2">
                            <input type="hidden" id="tercero_id_autocomplete" name="tercero_id_autocomplete" class="borrar data-required">
                        </div>

                        <div class="form-group col-md-2">
                            <label for="primer_nombre">Primer Nombre</label>
                            <input type="text" id="primer_nombre" name="primer_nombre" placeholder="Ingrese el primer nombre" class="form-control borrar borrar_datos_tercero">
                        </div>

                        <div class="form-group col-md-2">
                            <label for="segundo_nombre">Segundo Nombre</label>
                            <input type="text" id="segundo_nombre" name="segundo_nombre" placeholder="Ingrese el segundo nombre" class="form-control borrar borrar_datos_tercero">
                        </div>

                        <div class="form-group col-md-2">
                            <label for="primer_apellido">Primer Apellido</label>
                            <input type="text" id="primer_apellido" name="primer_apellido" placeholder="Ingrese el primer apellido" class="form-control borrar borrar_datos_tercero">
                        </div>

                        <div class="form-group col-md-2">
                            <label for="segundo_apellido">Segundo Apellido</label>
                            <input type="text" id="segundo_apellido" name="segundo_apellido" placeholder="Ingrese el segundo apellido" class="form-control borrar borrar_datos_tercero">
                        </div>

                        <div class="form-group col-md-2">
                            <label for="razon_social">Raz&oacute;n Social</label>
                            <input type="text" id="razon_social" name="razon_social" placeholder="Ingrese la raz&oacute;n social" class="form-control borrar borrar_datos_tercero">
                        </div>
                    </div>

                    <div class="row">

                        <div class="form-group col-md-2">
                            <label for="tipo_identificacion_id">Tipo Identificaci&oacute;n</label>
                            <select id="tipo_identificacion_id" name="tipo_identificacion_id" class="form-control data-required2 "></select>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="naturaleza_id">Tipo Naturaleza</label>
                            <select id="naturaleza_id" name="naturaleza_id" class="form-control data-required2 "></select>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="telefono">Tel&eacute;fono fijo</label>
                            <input type="text" id="telefono" name="telefono" placeholder="Ingrese un tel&eacute;fono fijo" class="form-control borrar borrar_datos_tercero">
                        </div>

                        <div class="form-group col-md-2">
                            <label for="nro_celular">Tel&eacute;fono Celular</label>
                            <input type="text" id="nro_celular" name="nro_celular" placeholder="Ingrese un tel&eacute;fono celular" class="form-control borrar  borrar_datos_tercero">
                        </div>

                        <div class="form-group col-md-2">
                            <label for="direccion">Direcci&oacute;n</label>
                            <input type="text" id="direccion" name="direccion" placeholder="Ingrese una direcci&oacute;n" class="form-control borrar  borrar_datos_tercero">
                        </div>

                        <div class="form-group col-md-2">
                            <label for="correo_electronico">Correo Electr&oacute;nico</label>
                            <input type="email" id="correo_electronico" name="correo_electronico" placeholder="Ingrese un Correo Electronico" class="form-control borrar  borrar_datos_tercero">
                        </div>

                    </div>

                    <div class="row" style="display: none" id="mostrar_boton_tercero">
                        <div class="form-group col-md-12" style="text-align: center">
                            <button class="btn btn-primary btn-lg" id="guardar_cliente">Guardar Cliente</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h4 class="text-on-pannel text-primary"><strong class="text-uppercase"> DATOS DEL PRODUCTO </strong></h4>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <label for="serial">Serial del producto <span style="color: red;">*</span></label>
                            <input type="text" id="serial" name="serial" placeholder="Serial del producto" class="form-control borrar">
                            <input type="hidden" id="serial_autocomplete" name="serial_autocomplete" class="borrar data-required">
                        </div>

                        <div class="form-group col-md-2">
                            <label for="desc_producto">Desc. Producto</label>
                            <div id="desc_producto" class="borrar_datos_productos2">-</div>
                        </div>

                        <div class="form-group col-md-1">
                            <label for="marca">Marca</label>
                            <div id="marca" class="borrar_datos_productos2">-</div>
                        </div>

                        <div class="form-group col-md-1">
                            <label for="color">Color</label>
                            <div id="color" class="borrar_datos_productos2">-</div>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="valor_minimo_venta">Val. Minimo Venta</label>
                            <div id="valor_minimo_venta" class="borrar_datos_productos2">-</div>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="valor_sugerido_venta">Val. Sugerido Venta</label>
                            <div id="valor_sugerido_venta" class="borrar_datos_productos2">-</div>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="valor_a_pagar">Val. a Pagar</label>
                            <input type="number" id="valor_a_pagar" name="valor_a_pagar" placeholder="Ingrese el valor unitario a pagar" class="form-control borrar borrar_datos_productos">
                        </div>

                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="observaciones_det_fact"></label>
                            <textarea id="observaciones_det_fact" name="observaciones_det_fact" placeholder="Colocar una observaci&oacute;n del producto" class="form-control borrar borrar_datos_productos"></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12" style="text-align: center">
                            <button class="btn btn-lg btn-success" id="agregar_producto">AGREGAR PRODUCTO</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h4 class="text-on-pannel text-primary"><strong class="text-uppercase"> LISTADO DE PRODUCTOS AGREGADOS </strong></h4>
                    <div class="row">
                        <div class="col-md-12">
                            <table id="listado" class="display responsive cell-border listado" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Producto</th>
                                    <th>Serial</th>
                                    <th>Marca</th>
                                    <th>Color</th>
                                    <th>Valor a pagar</th>
                                    <th>Opciones</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <?php echo $__env->make('facturacion.principal.modales_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
</div>

<?php echo $__env->make('facturacion.principal.jsNuevaFactura', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
