<html>
<head>
    <title></title>
    <style>
        body {
            font-family: Arial,
            Helvetica,
            sans-serif;
        }

        thead {
            display: table-header-group;
        }
        tfoot {
            display: table-row-group;
        }
        tr {
            page-break-inside: avoid;
        }

        .table {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            width: 100%;
            text-align: left;
            border-collapse: collapse;
            border: 1px;
        }

        .th {
            font-size: 12pt;
            font-weight: normal;
            padding: 8px;
            background: #b9c9fe;
            border-top: 4px solid #aabcfe;
            border-bottom: 1px solid #fff;
            color: #039;
        }

        .td {
            font-size: 11pt;
            padding: 8px;
            background: #e8edff;
            border-bottom: 1px solid #fff;
            color: #1b2746;
            border-top: 1px solid transparent;
        }

        .td-black {
            font-size: 11pt;
            padding: 8px;
            background: #e8edff;
            border-bottom: 1px solid #fff;
            color: #000000;
            border-top: 1px solid transparent;
        }

        .tr:hover .td {
            background: #d0dafd;
            color: #339;
        }

    </style>
</head>
<body>
<div class="row col-md-12">
    <table style="width: 100%" class="row col-md-12">
        <thead>
        <tr>
            <th colspan="10" style="text-align: center; font-size: 30px">REPORTE DE TODOS LOS SEGUIDORES **, $$ Y GERAL</th>
        </tr>
        <tr>
            <th colspan="10" style="text-align: center">PLATAFORMA WEB - GISSELA CONTIGO</th>
        </tr>
        <tr>
            <th colspan="10" style="text-align: left; font-size: 10px">Este informe no es un certificado y no sirve como prueba para ninguna validaci&oacute;n.</th>
        </tr>
        </thead>
    </table><br><br>
</div>
<?php if(count($data) > 0): ?>
    <table style="width: 100%" class="row col-md-12 table">
        <thead>
        <tr class="tr">
            <th class="th">Total de seguidores <?php echo e(count($data)); ?></th>
        </tr>
    </table>
    <table style="width: 100%" class="row col-md-12 table">
        <thead>
        <tr class="tr">
            <th class="th" colspan="6">Listado de los seguidores</th>
        </tr>
        <tr style="background-color: rgb(204, 204, 204);" class="tr">
            <th class="th">L&iacute;der</th>
            <th class="th">Seguidor(a)</th>
            <th class="th">Identificaci&oacute;n</th>
            <th class="th">Tipo Identificaci&oacute;n</th>
            <th class="th">Tel&eacute;fono Movil</th>
            <th class="th">Tel&eacute;fono Alterno</th>
            <th class="th">Email</th>
            <th class="th">Direcci&oacute;n</th>
            <th class="th">CC Registrada?</th>
            <th class="th">Lugar y mesa de Votaci&oacute;n</th>
        </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $idx => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr class="tr">
                <td style="text-align: left; font-size: 10px" class="td-black"><?php echo e(strtoupper($val -> nombres)); ?> <?php echo e(strtoupper($val -> apellidos)); ?></td>
                <td style="text-align: left; font-size: 10px" class="td-black"><?php echo e(strtoupper($val -> primer_nombre)); ?> <?php echo e(strtoupper($val -> segundo_nombre)); ?> <?php echo e(strtoupper($val -> primer_apellido)); ?> <?php echo e(strtoupper($val -> segundo_apellido)); ?></td>
                <td style="text-align: left; font-size: 10px" class="td-black"><?php echo e($val -> identificacion); ?></td>
                <td style="text-align: left; font-size: 10px" class="td-black"><?php echo e($val -> sigla_tipo_identificacion); ?>-<?php echo e($val -> desc_tipo_identificacion); ?></td>
                <td style="text-align: left; font-size: 10px" class="td-black"><?php echo e($val -> telefono); ?></td>
                <td style="text-align: left; font-size: 10px" class="td-black"><?php echo e($val -> telefono_alternativo); ?> </td>
                <td style="text-align: left; font-size: 10px" class="td-black"><?php echo e($val -> email); ?></td>
                <td style="text-align: left; font-size: 10px" class="td-black"><?php echo e(strtoupper($val -> direccion)); ?></td>
                <td style="text-align: left; font-size: 10px" class="td-black"><?php echo e($val -> cedula_registrada); ?></td>
                <td style="text-align: left; font-size: 10px" class="td-black"><?php echo e(strtoupper($val -> lugar_votacion)); ?> - MESA <?php echo e($val -> mesa_votacion); ?></td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table><br>
<?php endif; ?>
</body>
</html>