<div class="row">
    <div style="text-align: center">
        <i id="spinner2" class="fa fa-spinner fa-spin fa-5x" style="display: none"></i>
    </div>
    <div id="datos_usuario">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group col-md-3">
                    <label for="nombres" class="control-label">Nombres:</label>
                    <input id="nombres" type="text" class="form-control" name="nombres" value="" required>
                </div>

                <div class="form-group col-md-3">
                    <label for="apellidos" class="control-label">Apellidos:</label>
                    <input id="apellidos" type="text" class="form-control" name="apellidos" value="" required>
                </div>

                <div class="form-group col-md-3">
                    <label for="fk_id_tipo_identificacion">Tipo Identificaci&oacute;n *</label>
                    <select class="form-control borrar" id="fk_id_tipo_identificacion" name="fk_id_tipo_identificacion"></select>
                </div>

                <div class="form-group col-md-3">
                    <label for="fk_id_" class="control-label">Nro. Identificaci&oacute;n:</label>
                    <input id="identificacion" type="text" class="form-control" name="identificacion" value="" required>
                </div>

            </div>
            <div class="col-md-12">

                <div class="form-group col-md-3">
                    <label for="direccion" class="control-label">Direcci&oacute;n:</label>
                    <input id="direccion" type="text" class="form-control" name="direccion" value="" required>
                </div>

                <div class="form-group col-md-3">
                    <label for="telefono_fijo" class="control-label">Tel&eacute;fono Fijo:</label>
                    <input id="telefono_fijo" type="text" class="form-control" name="telefono_fijo" value="" required>
                </div>


                <div class="col-md-3">
                    <label for="telefono_movil" class="control-label">Tel&eacute;fono Movil:</label>
                    <input id="telefono_movil" type="text" class="form-control" name="telefono_movil" value="" required>
                </div>

                <div class="col-md-3">
                    <label for="email" class="control-label">Correo Electronico</label>
                    <input id="email" type="email" class="form-control" name="email" value="" required>
                </div>
            </div>

        </div>
    </div>
</div>

<?php $__env->startSection('js_usuario'); ?>
    <script type="text/javascript">

        $('#spinner2').show();
        $('#datos_usuario').hide();

        var id = null;

        $(document).on('click', '.mostrar', function () {
            id = $(this).data('val');
            //BUSCAMOS TODOS LOS DATOS DE LAS DIFERENTES TABLAS PARA MOSTRAR EN PANTALLA
            $.httpRequest('/query/listado/tipo_identificacion', {}, function() {}, function (dataTipoIdentificacion) {
                $('#fk_id_tipo_identificacion').empty();

                $('<option>').val('').text('Seleccione...').appendTo('#fk_id_tipo_identificacion');
                $.each(dataTipoIdentificacion.data, function (idx, val) {
                    $('<option>').val(val.id).text(val.sigla + ' - ' + val.descripcion).appendTo('#fk_id_tipo_identificacion');
                });
                /*consultamos los datos de usuario segun el id*/
                $.httpRequest("/query/listado/usuarios/" + id, {}, function () {
                }, function (data) {
                    $('#spinner2').hide();
                    $('#datos_usuario').show();
                    $('#nombres').val(data.data[0].nombres);
                    $('#apellidos').val(data.data[0].apellidos);
                    $('#fk_id_tipo_identificacion > option[value="' + data.data[0].fk_id_tipo_identificacion + '"]').attr('selected', 'selected');
                    $('#identificacion').val(data.data[0].identificacion);
                    $('#telefono_fijo').val(data.data[0].telefono_fijo);
                    $('#telefono_movil').val(data.data[0].telefono_movil);
                    $('#direccion').val(data.data[0].direccion);
                    $('#email').val(data.data[0].email);
                });
            });
        });


        /*ACTUALIZAMOS LOS REGISTROS DEL USUARIO*/
        $(document).on('click', '#actualizar', function () {
            json = {
                '_token': $('#footer input[name=_token]').val(),
                'data': {
                    'id' : id,
                    'nombres' : $('#nombres').val(),
                    'apellidos' : $('#apellidos').val(),
                    'fk_id_tipo_identificacion' : $('#fk_id_tipo_identificacion').val(),
                    'identificacion' : $('#identificacion').val(),
                    'telefono_fijo' : $('#telefono_fijo').val(),
                    'telefono_movil' : $('#telefono_movil').val(),
                    'direccion' : $('#direccion').val(),
                    'email' : $('#email').val()
                }
            };

            /*ACTUALIZAMOS LOS DATOS*/
            $.httpRequest("/query/modifica/usuarios/", {'method': 'PATCH', 'dataJson': json}, function (){}, function (data) {
                alertify.success(data[0].msj);
                cargaUsuario();
            });
        });
    </script>
<?php $__env->stopSection(); ?>