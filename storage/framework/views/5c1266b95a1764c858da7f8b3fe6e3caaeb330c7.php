<div class="ibox-title">
    <input type="hidden" id="tipo_filtro">
    <div style="float: left;">
        <h3>Listado de productos a Reparar</h3>
    </div>

    <div class="ibox-content">
        <div class="row">
            <table id="listado" class="display responsive cell-border listado" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Opciones</th>
                        <th>Nro. Identificaci&oacute;n</th>
                        <th>Tipo Naturaleza</th>
                        <th>Nombre Completo o Raz&oacute;n Social</th>
                        <th>Tel&eacute;fono</th>
                        <th>Dir&eacute;ccion</th>
                        <th>Correo Electr&oacute;nico</th>
                        <th>C&oacute;digo Producto</th>
                        <th>Nombre Producto</th>
                        <th>Observaciones</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>


    <div class="row">
        <?php echo $__env->make('reparaciones.principal.reparaciones.modales_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
</div>
