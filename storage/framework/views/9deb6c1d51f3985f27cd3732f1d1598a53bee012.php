<html>
<head>
    <title></title>
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        thead {
            display: table-header-group;
        }
        tfoot {
            display: table-row-group;
        }
        tr {
            page-break-inside: avoid;
        }

        /*.table {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            width: 100%;
            text-align: left;
            border-collapse: collapse;
            border: 1px;
        }

        .th {
            font-size: 12pt;
            font-weight: normal;
            padding: 8px;
            background: #b9c9fe;
            border-top: 4px solid #aabcfe;
            border-bottom: 1px solid #fff;
            color: #039;
        }

        .td {
            font-size: 11pt;
            padding: 8px;
            background: #e8edff;
            border-bottom: 1px solid #fff;
            color: #1b2746;
            border-top: 1px solid transparent;
        }

        .td-black {
            font-size: 11pt;
            padding: 8px;
            background: #e8edff;
            border-bottom: 1px solid #fff;
            color: #000000;
            border-top: 1px solid transparent;
        }

        .tr:hover .td {
            background: #d0dafd;
            color: #339;
        }*/

        .titulo_1{
            color: #2F75B5 !important;
        }

        .titulo_2 {
            background: #2F75B5 !important;
            color: white !important;
            font-size: 12pt;
        }

        td {
            font-size: 10pt;
        }


    </style>
</head>
<body>
<div class="row col-md-12">
    <table style="width: 100%" border="0">
        <thead>
        <tr>
            <th style="text-align: center" class="titulo_1"><h2>INFORME DE CONTROL DE INGRESO DE USUARIO</h2></th>
        </tr>
        </thead>
    </table>

    <br><br>

    <table style="width: 100%" border="3">
        <thead>
        <tr>
            <th style="text-align: center; max-width: 12px !important;" class="titulo_2"><b>NOMBRE COMPLETO</b></th>
            <th style="text-align: center; max-width: 12px !important;" class="titulo_2"><b>N&Uacute;MERO IDENTIFICACI&Oacute;N</b></th>
            <th style="text-align: center; max-width: 12px !important;" class="titulo_2"><b>DIRECCI&Oacute;N</b></th>
            <th style="text-align: center; max-width: 12px !important;" class="titulo_2"><b>TEL&Eacute;FONO</b></th>
            <th style="text-align: center; max-width: 12px !important;" class="titulo_2"><b>TEMPERATURA</b></th>
            <th style="text-align: center; max-width: 12px !important;" class="titulo_2"><b>FECHA Y HORA DE INGRESO</b></th>
            <th style="text-align: center; max-width: 12px !important;" class="titulo_2"><b>FUNCIONARIO</b></th>
            <th style="text-align: center; max-width: 12px !important;" class="titulo_2"><b>OBSERVACIONES</b></th>
        </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $data['datos']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $idx => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td style="max-width: 12px !important;"><?php echo e($val['nombres']); ?> <?php echo e($val['apellidos']); ?></td>
                <td style="max-width: 12px !important;"><?php echo e(number_format($val['nro_identificacion'])); ?></td>
                <td style="max-width: 12px !important;"><?php echo e($val['direccion']); ?></td>
                <td style="max-width: 12px !important;"><?php echo e($val['telefono']); ?></td>
                <td style="max-width: 12px !important;"><?php echo e($val['temperatura']); ?></td>
                <td style="max-width: 12px !important;"><?php echo e($val['fecha_ingreso']); ?></td>
                <td style="max-width: 12px !important;"><?php echo e($val['nombre_usuario']); ?> <?php echo e($val['apellido_usuario']); ?></td>
                <td style="max-width: 12px !important;"><?php echo e($val['observaciones']); ?></td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>


</div>
</body>
</html>