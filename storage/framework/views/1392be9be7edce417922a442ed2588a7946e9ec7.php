<?php if(!auth()->guest()): ?>
    <?php 
        $aplicativos = DB::table('global_tr_aplicativos as apli')
                ->join('global_tr_modulos as modu', 'modu.fk_id_aplicativo', 'apli.id')
                ->join('global_tr_menu as menu', 'menu.fk_id_modulo', 'modu.id')
                ->join('global_td_usuario_menu as usumenu', 'usumenu.fk_id_menu', 'menu.id')
                ->selectraw('distinct(apli.id) as id_aplicativo, apli.descripcion as desc_aplicativo,
                             apli.icono as icono_aplicativo')
                ->where('usumenu.fk_id_usuario', auth()->user()->id)
                ->wherenull('usumenu.deleted_at')
                ->orderby('apli.descripcion', 'asc')
                ->get();

     ?>

    <?php $__currentLoopData = $aplicativos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $aplicativo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php 
            $modulos = DB::table('global_tr_aplicativos as apli')
                    ->join('global_tr_modulos as modu', 'modu.fk_id_aplicativo', 'apli.id')
                    ->join('global_tr_menu as menu', 'menu.fk_id_modulo', 'modu.id')
                    ->join('global_td_usuario_menu as usumenu', 'usumenu.fk_id_menu', 'menu.id')
                    ->selectraw('distinct(modu.id) as id_modulo, modu.descripcion as desc_modulo,
                                 modu.icono as icono_modulo, modu.orden as orden_modulo, modu.fk_id_aplicativo as aplicativo_modulo')
                    ->where('usumenu.fk_id_usuario', auth()->user()->id)
                    ->where('apli.id', $aplicativo->id_aplicativo)
                    ->wherenull('usumenu.deleted_at')
                    ->orderby('modu.descripcion', 'asc')
                    ->get();
         ?>
        <li>
            <a href="#"><i class="<?php echo e($aplicativo->icono_aplicativo); ?>"></i>
                <span class="nav-label"><?php echo e($aplicativo->desc_aplicativo); ?></span>
                <?php if(count($modulos) > 0): ?>
                    <span class="fa arrow"></span>
                <?php endif; ?>
            </a>
            <?php if(count($modulos) > 0): ?>
                <ul class="nav nav-second-level">
                    <?php $__currentLoopData = $modulos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $modulo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php 
                            $menus = DB::table('global_tr_aplicativos as apli')
                                ->join('global_tr_modulos as modu', 'modu.fk_id_aplicativo', 'apli.id')
                                ->join('global_tr_menu as menu', 'menu.fk_id_modulo', 'modu.id')
                                ->join('global_td_usuario_menu as usumenu', 'usumenu.fk_id_menu', 'menu.id')
                                ->selectraw('distinct(menu.id) as id_menu, menu.descripcion as desc_menu, menu.url,
                                             menu.icono as icono_menu, menu.orden as orden_menu')
                                ->where('usumenu.fk_id_usuario', auth()->user()->id)
                                ->where('usumenu.fk_id_permiso', '3')
                                ->where('menu.mostrar_en_menu', 'si')
                                ->where('modu.id', $modulo->id_modulo)
                                ->wherenull('usumenu.deleted_at')
                                ->orderby('menu.orden', 'asc')
                                ->get();
                         ?>
                        <li>
                            <a href="#"><i class="<?php echo e($modulo->icono_modulo); ?>"></i>
                                <?php echo e($modulo->desc_modulo); ?>

                                <?php if(count($menus) > 0): ?>
                                    <span class="fa arrow"></span>
                                <?php endif; ?>
                            </a>
                            <?php if(count($menus) > 0): ?>
                                <ul class="nav nav-third-level">
                                    <?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li>
                                            <a href="<?php echo e(url($menu->url)); ?>"><i class="<?php echo e($menu->icono_menu); ?>"></i>
                                                <?php echo e($menu->desc_menu); ?>

                                            </a>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            <?php endif; ?>
        </li>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>