<script type="text/javascript">

    $(document).ready(function () {
        listado();//INICIALIZAMOS EL DATATABLE
        /*MOSTRAMOS EL SIPINNER Y OCULTAMOS LOS DATOS HASTA QUE CARGUEN*/
        $("#spinner_nueva_factura").show();
        $("#ver_datos_nueva_factura").hide();
        /*REALIZAMOS LAS PETICIONES DE TIPO DOCUMENTO Y TIPO NATURALEZA ANTES DE MOSTRAR LOS DATOS AL USUARIO*/
        $.httpRequest('/query/listado/tipo_identificacion', {}, function() {}, function (dataTipoIdentificacion) {
            $.httpRequest('/query/listado/tipo_naturaleza', {}, function () {}, function (dataTipoNaturaleza) {
                $('#tipo_identificacion_id').empty();
                $('#naturaleza_id').empty();

                $('<option>').val('').text('Seleccione...').appendTo('#tipo_identificacion_id');
                $.each(dataTipoIdentificacion.data, function (idx, val) {
                    $('<option>').val(val.id).text(val.sigla + ' - ' + val.descripcion).appendTo('#tipo_identificacion_id');
                });

                $('<option>').val('').text('Seleccione...').appendTo('#naturaleza_id');
                $.each(dataTipoNaturaleza.data, function (idx, val) {
                    $('<option>').val(val.id).text(val.sigla + ' - ' + val.descripcion).appendTo('#naturaleza_id');
                });

                $("#spinner_nueva_factura").hide();
                $("#ver_datos_nueva_factura").show();
            });
        });

        /************************************AUTOCOMPLETAR PARA EL TERCERO************************************/
        var nombreCompleto = '';
        var contaMensaje = 0;
        $('#tercero_id').autocomplete({
            source: function( request, response ) {
                $.ajax( {
                    url: "/query/reparaciones_ingreso/autocompletar_cliente",
                    method: "POST",
                    dataType: "json",
                    data: {'_token': "<?php echo e(csrf_token()); ?>", tercero_id: request.term},
                    success: function( data ) {
                        if ($("#id_factura").val() !== ''){
                            anularFactura( $("#id_factura").val() );
                        }
                        $('#mostrar_boton_tercero').hide();
                        $('#tercero_id_autocomplete').val("");
                        $('.borrar_datos_tercero').val('');
                        $('#tipo_identificacion_id option:selected').each(function () {
                            $(this).removeAttr('selected');
                        });
                        $('#naturaleza_id option:selected').each(function () {
                            $(this).removeAttr('selected');
                        });
                        var dataAutocomplete = [];
                        if(data.data.length === 0) {
                            $('#mostrar_boton_tercero').show();
                            if (contaMensaje === 0) {
                                swal('Información', 'No existe ningún cliente con ese número de identificación, nombre o razón social que esta ingresando', "info");
                            }
                            contaMensaje++;
                        }else {
                            $("#id_factura").val("");
                            $("#numero_factura").val("");
                            contaMensaje = 0;
                            $.each(data.data, function (idx, val) {
                                /*VALIDAMOS SI EN LA CONSULTA VIENE UN PARAMETRO LLAMADO VALUE
                                * ESTE PARAMETRO INDICA QUE LA CONSULTA NO ARROJO RESULTADOS*/

                                // EN CASO DE QUE SI EXISTA INFORMACION DEL TERCERO QUE SE ESTA CONSULTANDO
                                nombreCompleto = val.primer_nombre + " " + val.segundo_nombre + " " + val.primer_apellido + " " + val.segundo_apellido;
                                if (val.razon_social) {
                                    nombreCompleto = val.razon_social;
                                }

                                dataAutocomplete.push(
                                    {
                                        id: val.id,
                                        value: val.nro_identificacion + ' | ' + nombreCompleto,
                                        nombre_completo: nombreCompleto,
                                        primer_nombre: val.primer_nombre,
                                        segundo_nombre: val.segundo_nombre,
                                        primer_apellido: val.primer_apellido,
                                        segundo_apellido: val.segundo_apellido,
                                        razon_social: val.razon_social,
                                        tipo_identificacion_id: val.tipo_identificacion_id,
                                        naturaleza_id: val.naturaleza_id,
                                        desc_naturaleza: val.desc_naturaleza,
                                        desc_tipo_identificacion: val.desc_tipo_identificacion,
                                        telefono: val.telefono,
                                        direccion: val.direccion,
                                        correo_electronico: val.correo_electronico,
                                        nro_celular: val.nro_celular,
                                    }
                                );

                            });
                        }
                        response( dataAutocomplete );
                    }
                });
            },
            minLength: 1,
            select: function( event, ui ) {

                $('#mostrar_boton_tercero').hide();
                $('#tipo_identificacion_id > option[value="' + ui.item.tipo_identificacion_id + '"]').attr('selected', 'selected');
                $('#naturaleza_id > option[value="' + ui.item.naturaleza_id + '"]').attr('selected', 'selected');
                $('#tercero_id_autocomplete').val(ui.item.id);
                $('#primer_nombre').val(ui.item.primer_nombre);
                $('#segundo_nombre').val(ui.item.segundo_nombre);
                $('#primer_apellido').val(ui.item.primer_apellido);
                $('#segundo_apellido').val(ui.item.segundo_apellido);
                $('#razon_social').val(ui.item.razon_social);
                $('#telefono').val(ui.item.telefono);
                $('#nro_celular').val(ui.item.nro_celular);
                $('#direccion').val(ui.item.direccion);
                $('#correo_electronico').val(ui.item.correo_electronico);

            }
        });
        /****************************** FIN DE AUTOCOMPLETAR PARA EL TERCERO**********************************/


        /************************************AUTOCOMPLETAR PARA EL PRODUCTO************************************/
        var valor_minimo = 0;
        $('#serial').autocomplete({
            source: function( request, response ) {
                $.ajax( {
                    url: "/query/detalle_producto/autocompletar",
                    method: "POST",
                    dataType: "json",
                    data: {'_token': "<?php echo e(csrf_token()); ?>", serial: request.term},
                    success: function( data ) {
                        valor_minimo = 0;
                        $('#serial_autocomplete').val("");
                        $('.borrar_datos_productos').val('');
                        $('.borrar_datos_productos2').html('-');

                        var dataAutocomplete = [];
                        if(data.data.length === 0) {
                            $('#serial').val("");
                            $('#serial_autocomplete').val("");
                            alertify.info('No existe ningun producto con ese serial, porfavor verifique');
                        }else {
                            $.each(data.data, function (idx, val) {
                                /*VALIDAMOS SI EN LA CONSULTA VIENE UN PARAMETRO LLAMADO VALUE
                                * ESTE PARAMETRO INDICA QUE LA CONSULTA NO ARROJO RESULTADOS*/
                                valor_minimo = val.valor_minimo_venta;
                                dataAutocomplete.push(
                                    {
                                        id: val.id,
                                        value: val.serial + ' - ' + val.desc_producto,
                                        desc_producto: val.desc_producto,
                                        marca: val.marca,
                                        color: val.color,
                                        obs_producto: val.obs_producto,
                                        observaciones: val.observaciones,
                                        otros: val.otros,
                                        serial: val.serial,
                                        valor_minimo_venta: val.valor_minimo_venta,
                                        valor_sugerido_venta: val.valor_sugerido_venta,
                                    }
                                );

                            });
                        }
                        response( dataAutocomplete );
                    }
                });
            },
            minLength: 1,
            select: function( event, ui ) {
                $('#tipo_identificacion_id > option[value="' + ui.item.tipo_identificacion_id + '"]').attr('selected', 'selected');
                $('#naturaleza_id > option[value="' + ui.item.naturaleza_id + '"]').attr('selected', 'selected');
                $('#serial_autocomplete').val(ui.item.id);
                $('#desc_producto').html(ui.item.desc_producto);
                $('#marca').html(ui.item.marca);
                $('#color').html(ui.item.color);
                $('#valor_minimo_venta').html(addComas(ui.item.valor_minimo_venta));
                $('#valor_sugerido_venta').html(addComas(ui.item.valor_sugerido_venta));
                $('#valor_a_pagar').val(ui.item.valor_sugerido_venta);
            },
            change: function (event, ui) {
                if(!ui.item){
                    $('#serial').val("").focus();
                    $('#serial_autocomplete').val("");
                }
            }
        });
        /****************************** FIN DE AUTOCOMPLETAR PARA EL PRODUCTO**********************************/


    });

</script>