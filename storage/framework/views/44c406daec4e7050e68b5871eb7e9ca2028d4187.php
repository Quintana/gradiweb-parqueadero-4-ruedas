<?php $__env->startSection('css'); ?>
    <style>
        .modal-xl {
            min-width: 90%;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <!-- boton flotante -->
    <div class="row" id="small-chat">
        <div data-toggle="modal" data-target="#infoModal">
            <button id="mostrar" class="btn btn-circle btn-info btn-lg mostrar" data-toggle="tooltip" data-placement="top" title="Crear Registro">
                <i class="fa fa-plus"></i>
            </button>
        </div>
    </div>
    <!-- fin boton flotante -->

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h1 class="pull-left">Listado de Productos</h1>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <?php echo $__env->make('reparaciones.principal.productos.listado', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <?php echo $__env->make('reparaciones.principal.productos.js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>