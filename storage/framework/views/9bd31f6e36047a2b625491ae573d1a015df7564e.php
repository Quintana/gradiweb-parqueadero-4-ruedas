<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <form class="form-horizontal" method="POST">
                        <?php if(Session::has('message')): ?>
                            <p class="alert alert-info"><?php echo e(Session::get('message')); ?></p>
                        <?php endif; ?>
                    </form>
                    Bienvenido(a) a Llamado al Sistema

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-body" style="text-align: center">
                                <img src="<?php echo e(asset('img/imagen_principal.jpg')); ?>" style="width: 40%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>