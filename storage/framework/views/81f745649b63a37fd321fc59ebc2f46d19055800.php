<?php echo $__env->make('facturacion.principal.funciones_js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script type="text/javascript">

    $(document).ready(function () {

        //cuando le damos click al tab de facturacion
        $(document).on('click', '#nueva_factura', function () {
            if($("#id_factura").val() !== ""){
                anularFactura( $("#id_factura").val() );
            }
            tabFactura();
        });

        //al iniciar el programa inicializamos el tab de facturacion
        $('#nueva_factura').trigger('click');

        /*ELIMINACION POR SOFTWARE DEL REGISTRO*/
        $(document).on('click', '.delete', function () {
            idElimina = $(this).data('val');
        });

        $(document).on('click', '#eliminar', function () {
            if(idElimina !== null){
                json = {
                    '_token': "<?php echo e(csrf_token()); ?>",
                    'data': {
                        'id': idElimina
                    }
                };

                $.httpRequest('/query/facturacion/detalle', {'method': 'DELETE', 'dataJson': json}, function(){}, function (dataDelete) {
                    /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                    listado($("#id_factura").val());
                    alertify.success(dataDelete[0].msj);
                    $('#serial').val("");
                    $('#serial_autocomplete').val("");
                    $('.borrar_datos_productos').val('');
                    $('.borrar_datos_productos2').html('-');
                });
            }
        });
        /*ELIMINACION POR SOFTWARE DEL REGISTRO*/


        /*IMPRIMIMOS EN UNA NUEVA PESTAÑA LA FACTURA EN PDF*/
        $(document).on('click', '#mostrar_factura', function () {
            if ($("#id_factura").val() !== "") {
                $.httpRequest('/files/factura_pdf/' + $("#id_factura").val(), {}, function () {}, function (dataPDF) {
                    window.open('<?php echo e(Storage::url('')); ?>' + dataPDF.ruta, '_blank');
                    swal('Excelente', 'La factura se ha generado correctamente', "success");
                    tabFactura();
                });
            }else{
                swal('Atencion', 'No existe el número de factura a imprimir o no ha creado la factura, porfavor revise', "warning");
            }
        });

        /******************************************GUARDAR DATOS DE LOS CLIENTES O TERCEROS****************************/
        /*GUARDAMOS O  ACTUALIZAMOS LA INFORMACION*/
        $(document).on('click', '#guardar_cliente', function () {
            var nro_identificacion = $("#tercero_id").val();

            json = {
                '_token': "<?php echo e(csrf_token()); ?>",
                'data': {
                    'tipo_identificacion_id': $('#tipo_identificacion_id').val(),
                    'naturaleza_id': $('#naturaleza_id').val(),
                    'primer_nombre': $('#primer_nombre').val(),
                    'segundo_nombre': $('#segundo_nombre').val(),
                    'primer_apellido': $('#primer_apellido').val(),
                    'segundo_apellido': $('#segundo_apellido').val(),
                    'nro_identificacion': $('#tercero_id').val(),
                    'razon_social': $('#razon_social').val(),
                    'telefono': $('#telefono').val(),
                    'nro_celular': $('#nro_celular').val(),
                    'direccion': $('#direccion').val(),
                    'correo_electronico': $('#correo_electronico').val()
                }
            };
            $.httpRequest('/query/tercero', {'method': 'POST', 'dataJson': json}, function () {}, function (dataInsert) {
                alertify.success(dataInsert[0].msj);
                $('#mostrar_boton_tercero').hide();
                $('#tercero_id_autocomplete').val(dataInsert[0].data.id);
            });
        });
        /*FIN DE GUARDAMOS O  ACTUALIZAMOS LA INFORMACION*/
        /******************************************GUARDAR DATOS DE LOS CLIENTES O TERCEROS****************************/

        /********************************AGREGAMOS PRODUCTOS A LA FACTURA*************************************/
        var id_tercero = '';
        var id_serial = '';
        var id_factura = '';
        var valor_total_unitario = '';
        $(document).on('click', '#agregar_producto', function () {
            id_tercero = $("#tercero_id_autocomplete").val();
            id_serial = $("#serial_autocomplete").val();
            id_factura = $("#id_factura").val();
            valor_total_unitario = $("#valor_a_pagar").val();
            if(id_factura === "" && id_factura !== null && id_factura !== undefined) {
                if (id_tercero !== "" && id_tercero !== null && id_tercero !== undefined) {
                    if (id_serial !== "" && id_serial !== null && id_serial !== undefined ) {
                        json = {
                            '_token': "<?php echo e(csrf_token()); ?>",
                            'data': {
                                'tercero_id': id_tercero,
                                'detalle_producto_id': id_serial,
                                'valor_unitario_venta': valor_total_unitario,
                                'observacion': $("#observaciones_det_fact").val(),
                            }
                        };
                        $.httpRequest('/query/facturacion/crear_factura', {'method': 'POST', 'dataJson': json}, function () {}, function (dataInsert) {
                            $("#id_factura").val( dataInsert[0].data );
                            alertify.success(dataInsert[0].msj);
                            listado( dataInsert[0].data );
                            $('#serial').val("");
                            $('#serial_autocomplete').val("");
                            $('.borrar_datos_productos').val('');
                            $('.borrar_datos_productos2').html('-');
                        });
                    } else {
                        swal('Atencion', 'No ha seleccionado el producto a agregar, porfavor revisar', "warning");
                    }
                } else {
                    swal('Atencion', 'No ha seleccionado el cliente o tercero, porfavor revisar', "warning");
                }
            }else{ // EN CASO DE QUE EXISTA LA FACTURA SOLAMENTE SE DEBE INGRESAR EL DETALLE
                if (id_serial !== "" && id_serial !== null && id_serial !== undefined ) { // el producto debe venir ya seleccionado para poder realizar la insercion
                    jsonDetalle = {
                        '_token': "<?php echo e(csrf_token()); ?>",
                        'data': {
                            'factura_id': id_factura,
                            'detalle_producto_id': id_serial,
                            'valor_unitario_venta': valor_total_unitario,
                            'observacion': $("#observaciones_det_fact").val(),
                        }
                    };
                    $.httpRequest('/query/detalle_facturacion/agregar_producto_factura', {'method': 'POST', 'dataJson': jsonDetalle}, function () {}, function (dataDetalle) {
                        alertify.success(dataDetalle[0].msj);
                        listado(id_factura);
                        $('#serial').val("");
                        $('#serial_autocomplete').val("");
                        $('.borrar_datos_productos').val('');
                        $('.borrar_datos_productos2').html('-');
                    });
                }else {
                    swal('Atencion', 'No ha seleccionado el producto a agregar, porfavor revisar', "warning");
                }
            }
        });
        /************************** FIN DE AGREGAMOS PRODUCTOS A LA FACTURA*************************************/



    });

</script>
