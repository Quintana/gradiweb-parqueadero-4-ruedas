<script type="text/javascript">
    //FUNCION PARA ADICIONAR COMAS AL VALOR EN CIFRAS DE MILES
    function addComas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    //funcion que borra los datos almacenados de los tabs
    function borrarDatosTabs(){
        $('#tercero_id_autocomplete').val("");
        $('.borrar_datos_tercero').val('');
        $('.borrar_datos_productos').val('');
        $('.borrar_datos_productos2').html('-');
        $("#id_factura").val("");
        $("#numero_factura").val("");
    }

    // funcion que sirve para mostrar toda la vista de la aplicacion de la facturacion
    function tabFactura(){
        $.httpRequest('/query/facturacion/nueva_factura', {'dataType': 'html'}, function () {}, function (dataTabs) {
            borrarDatosTabs();
            $('#tabNuevaFactura').empty().html(dataTabs); // MOSTRAMOS LOS DATOS DENTRO DE LA ETIQUETA
        });
    }

    //FUNCION PARA MOSTRAR LOS DATOS EN EL DATATABLE
    function dibujarDatatable(data) {
        json = []; // siempre que se llame a esta funcion el va a limpiar la variable para volverla a llenar con la informacion de la tabla
        /*recorremos los y definimos los datos para dibujar en el datatable asi mismo creamos el objeto "opciones" que contiene las diferentes acciones que se van a realizar por cada registro*/
        $.each(data.data, function (idx, val) {
            json.push({
                id: val.id,
                desc_producto: val.desc_producto,
                serial: val.serial,
                marca: val.marca,
                color: val.color,
                valor_unitario_venta: addComas(val.valor_unitario_venta),
                opciones:   '<div class="form-inline">' +
                    '<div data-toggle="modal" class="form-group" data-target="#deleteModal">' +
                    '<button id="delete" class="delete btn btn-danger btn-circle btn-outline" data-toggle="tooltip"' +
                    ' data-placement="top" title="Eliminar Registro"' +
                    ' data-val="' + val.id + '">' +
                    '<span class="fa fa-trash"></span>' +
                    '</button>' +
                    '</div>&nbsp;' +
                    '</div>',

            });
        });

        /*cabecera del datatable*/
        var columns = [
            {'data' : 'id'},
            {'data' : 'desc_producto'},
            {'data' : 'serial'},
            {'data' : 'marca'},
            {'data' : 'color'},
            {'data' : 'valor_unitario_venta'},
            {'data' : 'opciones'},

        ];

        /*llamamos el plugin para dibujar los datos en el datatable segun el id o class asignado*/
        $('#listado').drawDataTable(json, columns);
    }

    /*FUNCION PARA GENERAR EL LISTADO DE CONSULTA DE LA TABLA*/
    function listado(idFactura = null) {
        /*LLAMAMOS EL PLUGIN Y LE PASAOS LOS PARAMETROS PARA DIBUJAR EL DATATABLE*/
        $.httpRequest('/query/detalle_facturacion/listado/' + idFactura, {'blockUI': true}, function() {}, function (dataListado) {
            //console.log(dataListado);
            dibujarDatatable(dataListado); //enviamos los datos a la funcion anterior para procesar la informacion y mostrarla posteriormente
        });
    }

    function anularFactura(nro_factura){
        json = {
            '_token': "<?php echo e(csrf_token()); ?>",
            'data': {
                'id': nro_factura
            }
        };
        $.httpRequest('/query/facturacion', {'method': 'DELETE', 'dataJson': json}, function(){}, function (dataDelete) {
            /*LISTAMOS NUEVAMENTE EL DATATABLE*/
        });
    }
</script>