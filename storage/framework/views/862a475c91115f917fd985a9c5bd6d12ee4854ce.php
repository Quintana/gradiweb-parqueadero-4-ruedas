<div class="ibox-title">
    <input type="hidden" id="tipo_filtro">
    <div style="float: left;">
        <h3>Listado de Categorias</h3>
    </div>

    <div class="ibox-content">
        <div class="row">
            <table id="listado" class="display responsive cell-border listado" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Descripci&oacute;n</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>


    <div class="row">
        <?php echo $__env->make('reparaciones.maestros.categorias.modales_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
</div>
