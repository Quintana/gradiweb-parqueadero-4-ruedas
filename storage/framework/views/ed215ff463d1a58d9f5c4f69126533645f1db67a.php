<html>
<head>
    <title></title>
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        thead {
            display: table-header-group;
        }
        tfoot {
            display: table-row-group;
        }
        tr {
            page-break-inside: avoid;
        }

        /*.table {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            width: 100%;
            text-align: left;
            border-collapse: collapse;
            border: 1px;
        }

        .th {
            font-size: 12pt;
            font-weight: normal;
            padding: 8px;
            background: #b9c9fe;
            border-top: 4px solid #aabcfe;
            border-bottom: 1px solid #fff;
            color: #039;
        }

        .td {
            font-size: 11pt;
            padding: 8px;
            background: #e8edff;
            border-bottom: 1px solid #fff;
            color: #1b2746;
            border-top: 1px solid transparent;
        }

        .td-black {
            font-size: 11pt;
            padding: 8px;
            background: #e8edff;
            border-bottom: 1px solid #fff;
            color: #000000;
            border-top: 1px solid transparent;
        }

        .tr:hover .td {
            background: #d0dafd;
            color: #339;
        }*/

        .titulo_1{
            color: #2F75B5 !important;
        }

        .titulo_2 {
            background: #2F75B5 !important;
            color: white !important;
            font-size: 15pt;
        }

    </style>
</head>
<body>
<div class="row col-md-12">
    <table style="width: 100%" border="0">
        <thead>
        <tr>
            <th style="text-align: left" class="titulo_1"><h2>Comunicaciones SMK</h2></th>
            <th style="text-align: right" class="titulo_1"><h1>FACTURA DE VENTA</h1></th>
        </tr>
        </thead>
    </table>

    <?php $__currentLoopData = $data['datos_factura']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $idx => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <table style="width: 100%" border="0">
        <thead>
        <tr>
            <th style="text-align: left">Direccion: </th>
            <th style="text-align: right">Fecha: <?php echo e(\Carbon\Carbon::now()); ?></th>
        </tr>
        <tr>
            <th style="text-align: left">Mariquita, Tolima</th>
            <th style="text-align: right">Factura: <?php echo e($val['numero_factura']); ?></th>
        </tr>
        <tr>
            <th style="text-align: left">Tel&eacute;fono: </th>
            <th style="text-align: right">Cliente ID: <?php echo e($val['tercero_id']); ?></th>
        </tr>
        <tr>
            <th style="text-align: left">Pagina Web: <a href="https://comunicacionessmk.llamadoalsistema.com/">https://comunicacionessmk.llamadoalsistema.com/</a></th>
            <th style="text-align: right">Vence: <?php echo e(\Carbon\Carbon::now()); ?></th>
        </tr>
        </thead>
    </table>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <br><br>
    <?php $__currentLoopData = $data['datos_tercero']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $idx => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <table style="width: 100%">
        <thead>
            <tr>
                <th style="text-align: left" class="titulo_2"><b>Datos del Cliente</b></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><b>Nombre: </b><?php echo e($val['primer_nombre']); ?> <?php echo e($val['segundo_nombre']); ?> <?php echo e($val['primer_apellido']); ?> <?php echo e($val['segundo_nombre']); ?></td>
            </tr>
            <tr>
                <td><b>Raz&oacute;n Social: </b><?php echo e($val['razon_social']); ?></td>
            </tr>
            <tr>
                <td><b>Identificaci&oacute;n: </b><?php echo e(number_format($val['nro_identificacion'])); ?></td>
            </tr>
            <tr>
                <td><b>Direcci&oacute;n: </b><?php echo e($val['direccion']); ?></td>
            </tr>
            <tr>
                <td><b>Tel&eacute;fono: </b><?php echo e($val['Telefono']); ?> - <?php echo e($val['nro_celular']); ?></td>
            </tr>
            <tr>
                <td><b>Correo Electr&oacute;nico: </b><?php echo e($val['correo_electronico']); ?></td>
            </tr>
        </tbody>
    </table>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <br><br>

    <table style="width: 100%">
        <thead>
        <tr>
            <th style="text-align: left" class="titulo_2"><b>Descripci&oacute;n del producto</b></th>
            <th style="text-align: left" class="titulo_2"><b>Observaciones</b></th>
            <th style="text-align: left" class="titulo_2"><b>Cantidad</b></th>
            <th style="text-align: left" class="titulo_2"><b>Valor Unitario</b></th>
            <th style="text-align: left" class="titulo_2"><b>Valor Total</b></th>
        </tr>
        </thead>
        <tbody>
        <?php 
            $total_a_pagar = 0;
         ?>
        <?php $__currentLoopData = $data['datos_detalle_factura']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $idx => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php 
                $total_a_pagar += $val['valor_unitario_venta'];
             ?>
        <tr>
            <td><?php echo e($val['desc_producto']); ?></td>
            <td><?php echo e($val['observacion']); ?></td>
            <td style="text-align: right !important;">1</td>
            <td style="text-align: right !important;"><?php echo e(number_format($val['valor_unitario_venta'])); ?></td>
            <td style="text-align: right !important;"><?php echo e(number_format($val['valor_unitario_venta'])); ?></td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
    <br>
    <table style="width: 100%">
        <thead>
        <tr>
            <th style="text-align: right !important; min-width: 90%"><h3><b>TOTAL A PAGAR:</b></h3></th>
            <td style="text-align: right !important; min-width: 10%"><h3><b><?php echo e(number_format($total_a_pagar)); ?></b></h3></td>
        </tr>
        </thead>
    </table>





</div>
</body>
</html>