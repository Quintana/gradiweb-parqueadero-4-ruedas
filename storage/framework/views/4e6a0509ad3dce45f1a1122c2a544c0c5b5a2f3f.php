<script type="text/javascript">

    $(document).ready(function () {

        var json = []; // definimos la variable vacia que va a contener los datos para dibujar el datatable
        var id = ''; // definimos la variable que va a contener el id para la actualizacion del registro
        var idElimina = ''; // definimos la variable que va a contener el id para la eliminacion del registro

        function dibujarDatatable(data) {
            json = []; // siempre que se llame a esta funcion el va a limpiar la variable para volverla a llenar con la informacion de la tabla
            /*recorremos los y definimos los datos para dibujar en el datatable asi mismo creamos el objeto "opciones" que contiene las diferentes acciones que se van a realizar por cada registro*/
            $.each(data.data, function (idx, val) {
                var nombre_completo = val.razon_social;
                if(val.razon_social == ""){
                    nombre_completo = val.primer_nombre + " " + val.segundo_nombre + " " + val.primer_apellido + " " + val.segundo_apellido;
                }
                json.push({
                    id: val.id,
                    opciones:   '<div class="form-inline">' +
                                    '<div data-toggle="modal" class="form-group" data-target="#deleteModal">' +
                                        '<button id="delete" class="delete btn btn-danger btn-circle btn-outline" data-toggle="tooltip"' +
                                        ' data-placement="top" title="Eliminar Registro"' +
                                        ' data-val="' + val.id + '">' +
                                            '<span class="fa fa-trash"></span>' +
                                        '</button>' +
                                    '</div>&nbsp;' +
                                    '<div data-toggle="modal" class="form-group" data-target="#infoModal">' +
                                        '<button id="mostrar" class="mostrar btn btn-success btn-circle btn-outline" data-toggle="tooltip"' +
                                        ' data-placement="top" title="Editar informaci&oacute;n detallada "' +
                                        ' data-val="' + val.id + '">' +
                                            '<span class="fa fa-pencil"></span>' +
                                        '</button>' +
                                    '</div>&nbsp;' +
                                '</div>',
                    nro_identificacion: val.sigla_tipo_iden + " " + val.nro_identificacion,
                    desc_naturaleza: val.desc_naturaleza,
                    nombre_completo: nombre_completo,
                    telefono: val.telefono,
                    direccion: val.direccion,
                    email: val.correo_electronico,
                    codigo_producto: val.codigo_producto,
                    nombre: val.nombre,
                    observacion: val.observacion,
                });
            });

            /*cabecera del datatable*/
            var columns = [
                {'data' : 'id'},
                {'data' : 'opciones'},
                {'data' : 'nro_identificacion'},
                {'data' : 'desc_naturaleza'},
                {'data' : 'nombre_completo'},
                {'data' : 'telefono'},
                {'data' : 'direccion'},
                {'data' : 'email'},
                {'data' : 'codigo_producto'},
                {'data' : 'nombre'},
                {'data' : 'observacion'},

            ];

            /*llamamos el plugin para dibujar los datos en el datatable segun el id o class asignado*/
            $('#listado').drawDataTable(json, columns);
        }

        /*FUNCION PARA GENERAR EL LISTADO DE CONSULTA DE LA TABLA*/
        function listado() {
            /*LLAMAMOS EL PLUGIN Y LE PASAOS LOS PARAMETROS PARA DIBUJAR EL DATATABLE*/
            $.httpRequest('/query/listado/reparaciones_ingreso', {'blockUI': true}, function() {}, function (dataListado) {
                //console.log(dataListado);
                dibujarDatatable(dataListado); //enviamos los datos a la funcion anterior para procesar la informacion y mostrarla posteriormente
            });
        }

        listado(); // generamos la consulta para el listado de todos los datos de la tabla

        /*ACTIVAMOS EL EVENTO CLICK DE LA CLASS "MOSTRAR" Y IDENTIFICAMOS SI ES PARA INSERTAR O PARA ACTUALIZAR UN REGITRO*/
        $(document).on('click', '.mostrar', function() {
            $('#spinner').show(); // activamos el spinner para que el usuario sepa que hay una carga de la informacion
            $('#mostrar_info').hide(); // ocultamos el div en donde se va a mostrar la informacion para actualizar o para insertar
            $('#mostrar_boton_tercero').hide(); // OCULTAMO EL BOTON DE CREAR CLIENTES O TERCEROS

            var actualizaRegistro = $(this).data('val'); // variable para distinguir si es para actualizar o crear un registro
            $.httpRequest('/query/listado/tipo_identificacion', {}, function() {}, function (dataTipoIdentificacion) {
                $.httpRequest('/query/listado/tipo_naturaleza', {}, function () {}, function (dataTipoNaturaleza) {
                    $('#tipo_identificacion_id').empty();
                    $('#naturaleza_id').empty();

                    $('<option>').val('').text('Seleccione...').appendTo('#tipo_identificacion_id');
                    $.each(dataTipoIdentificacion.data, function (idx, val) {
                        $('<option>').val(val.id).text(val.sigla + ' - ' + val.descripcion).appendTo('#tipo_identificacion_id');
                    });

                    $('<option>').val('').text('Seleccione...').appendTo('#naturaleza_id');
                    $.each(dataTipoNaturaleza.data, function (idx, val) {
                        $('<option>').val(val.id).text(val.sigla + ' - ' + val.descripcion).appendTo('#naturaleza_id');
                    });

                    // validamos que sea para actualizar la informacion sino entra al else para la insercion de la informacion
                    if(actualizaRegistro){ // actualizamos la informacion
                        $('#enviar_guardar').val('act');
                        $.httpRequest('/query/listado/reparaciones_ingreso/' + actualizaRegistro, {}, function() {}, function (data) {
                            id = data.data[0].id;
                            var nombre_completo_razon_social = data.data[0].razon_social;
                            if(data.data[0].razon_social == ""){
                                nombre_completo_razon_social = data.data[0].primer_nombre + " " + data.data[0].segundo_nombre + " " + data.data[0].primer_apellido + " " + data.data[0].segundo_apellido;
                            }
                            $('#spinner').hide();
                            $('#mostrar_info').show();
                            $('#tercero_id').val(data.data[0].nro_identificacion + " | " + nombre_completo_razon_social);
                            $('#tercero_id_autocomplete').val(data.data[0].tercero_id);
                            $('#tipo_identificacion_id > option[value="' + data.data[0].tipo_identificacion_id + '"]').attr('selected', 'selected');
                            $('#naturaleza_id > option[value="' + data.data[0].naturaleza_id + '"]').attr('selected', 'selected');
                            $('#razon_social').val(data.data[0].razon_social);
                            $('#primer_nombre').val(data.data[0].primer_nombre);
                            $('#segundo_nombre').val(data.data[0].segundo_nombre);
                            $('#primer_apellido').val(data.data[0].primer_apellido);
                            $('#segundo_apellido').val(data.data[0].segundo_apellido);
                            $('#telefono').val(data.data[0].telefono);
                            $('#direccion').val(data.data[0].direccion);
                            $('#nro_celular').val(data.data[0].nro_celular);
                            $('#correo_electronico').val(data.data[0].correo_electronico);
                            $('#codigo_producto').val(data.data[0].codigo_producto);
                            $('#nombre').val(data.data[0].nombre);
                            $('#observacion').val(data.data[0].observacion);

                        });
                    }else{ // insertamos la informacion
                        $('#enviar_guardar').val('new');
                        $('#spinner').hide();
                        $('#mostrar_info').show();
                        $('.borrar').val('');
                    }
                });
            });
        });
        /*FIN DE ACTIVAMOS EL EVENTO CLICK DE LA CLASS "MOSTRAR" Y IDENTIFICAMOS SI ES PARA INSERTAR O PARA ACTUALIZAR UN REGITRO*/

        /*GUARDAMOS O  ACTUALIZAMOS LA INFORMACION*/
        $(document).on('click', '#guardar', function () {
            /*VALIDAMOS QUE TODOS LOS ELEMNTOS HTML QUE TENGAN LA CLASE 'data-required' CONTENGAN DATOS DE LOS CONTRARIO
            * NO PODRA ACTUALIZAR NI INSERTAR*/
            var validarDatos = true;

            $(".data-required").each(function(){
                if($(this).val() == ''){
                    validarDatos = false;
                }
            });
            /*SIN DE LA VALIDACION*/

            if(validarDatos === true) {
                if ($('#enviar_guardar').val() === 'act') { // PROCESO DE ACTUALIZACION
                    json = {
                        '_token': $('#footer input[name=_token]').val(),
                        'data': {
                            'id': id,
                            'tercero_id': $('#tercero_id_autocomplete').val(),
                            'codigo_producto': $('#codigo_producto').val(),
                            'nombre': $('#nombre').val(),
                            'observacion': $('#observacion').val(),
                        }
                    };
                    $.httpRequest('/query/reparaciones_ingreso', {'method': 'PATCH', 'dataJson': json}, function () {}, function (dataUpdated) {
                        /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                        listado();
                        alertify.success(dataUpdated[0].msj);
                    });
                } else if ($('#enviar_guardar').val() === 'new') { // PROCESO PARA LA CREACION DE UN REGISTRO
                    json = {
                        '_token': $('#footer input[name=_token]').val(),
                        'data': {
                            'tercero_id': $('#tercero_id_autocomplete').val(),
                            'codigo_producto': $('#codigo_producto').val(),
                            'nombre': $('#nombre').val(),
                            'observacion': $('#observacion').val(),
                        }
                    };

                    $.httpRequest('/query/reparaciones_ingreso', {'method': 'POST', 'dataJson': json}, function () {}, function (dataInsert) {
                        /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                        listado();
                        alertify.success(dataInsert[0].msj);
                    });
                }
            }else{
                //alertify.warning('TODOS LOS CAMPOS SON OBLIGATORIOS');
                swal('ATENCION!!!', 'TODOS LOS CAMPOS SON OBLIGATORIOS', "warning")
            }
        });
        /*FIN DE GUARDAMOS O  ACTUALIZAMOS LA INFORMACION*/

        /*ELIMINACION POR SOFTWARE DEL REGISTRO*/
        $(document).on('click', '.delete', function () {
            idElimina = $(this).data('val');
        });

        $(document).on('click', '#eliminar', function () {
            if(idElimina !== null){
                json = {
                    '_token': $('#footer2 input[name=_token]').val(),
                    'data': {
                        'id': idElimina
                    }
                };

                $.httpRequest('/query/reparaciones_ingreso', {'method': 'DELETE', 'dataJson': json}, function(){}, function (dataDelete) {
                    /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                    listado();
                    alertify.success(dataDelete[0].msj);
                });
            }
        });
        /*ELIMINACION POR SOFTWARE DEL REGISTRO*/


        /************************************AUTOCOMPLETAR PARA EL TERCERO************************************/
        var nombreCompleto = '';
        var contaMensaje = 0;
        $('#tercero_id').autocomplete({
            source: function( request, response ) {
                $.ajax( {
                    url: "/query/reparaciones_ingreso/autocompletar_cliente",
                    method: "POST",
                    dataType: "json",
                    data: {'_token': $('#footer input[name=_token]').val(), tercero_id: request.term, datos: 'esta es una prueba'},
                    success: function( data ) {
                        $('#mostrar_boton_tercero').hide();
                        $('#tercero_id_autocomplete').val("");
                        $('.borrar_datos_tercero').val('');
                        $('#tipo_identificacion_id option:selected').each(function () {
                            $(this).removeAttr('selected');
                        });
                        $('#naturaleza_id option:selected').each(function () {
                            $(this).removeAttr('selected');
                        });
                        var dataAutocomplete = [];
                        if(data.data.length === 0) {
                            $('#mostrar_boton_tercero').show();
                            if (contaMensaje === 0) {
                                swal('Información', 'No existe ningún cliente con ese número de identificación, nombre o razón social que esta ingresando', "info");
                            }
                            contaMensaje++;
                        }else {
                            contaMensaje = 0;
                            $.each(data.data, function (idx, val) {
                                /*VALIDAMOS SI EN LA CONSULTA VIENE UN PARAMETRO LLAMADO VALUE
                                * ESTE PARAMETRO INDICA QUE LA CONSULTA NO ARROJO RESULTADOS*/

                                // EN CASO DE QUE SI EXISTA INFORMACION DEL TERCERO QUE SE ESTA CONSULTANDO
                                nombreCompleto = val.primer_nombre + " " + val.segundo_nombre + " " + val.primer_apellido + " " + val.segundo_apellido;
                                if (val.razon_social) {
                                    nombreCompleto = razon_social;
                                }

                                dataAutocomplete.push(
                                    {
                                        id: val.id,
                                        value: val.nro_identificacion + ' | ' + nombreCompleto,
                                        nombre_completo: nombreCompleto,
                                        primer_nombre: val.primer_nombre,
                                        segundo_nombre: val.segundo_nombre,
                                        primer_apellido: val.primer_apellido,
                                        segundo_apellido: val.segundo_apellido,
                                        razon_social: val.razon_social,
                                        tipo_identificacion_id: val.tipo_identificacion_id,
                                        naturaleza_id: val.naturaleza_id,
                                        desc_naturaleza: val.desc_naturaleza,
                                        desc_tipo_identificacion: val.desc_tipo_identificacion,
                                        telefono: val.telefono,
                                        direccion: val.direccion,
                                        correo_electronico: val.correo_electronico,
                                        nro_celular: val.nro_celular,
                                    }
                                );

                            });
                        }
                        response( dataAutocomplete );
                    }
                });
            },
            minLength: 1,
            select: function( event, ui ) {
                /*VALIDAMOS QUE LA INFORMACION DE ID SEA DIFERENTE A (sin_id)
                * YA QUE ESTA LE PERTENECE A LAS FICHAS QUE ESTAN TRATANDO DE CONSULTAR
                * PERO QUE NO TIENEN UN TITULO ASOCIADO*/

                /*if (ui.item.id === 'sin_id') {
                    $('#tercero_id_autocomplete').val(ui.item.id);
                    $('.borrar_datos_tercero').val('');
                    $('#tercero_id').autocomplete("search", "");
                    swal({
                        type: 'error',
                        title: 'Error en la busqueda',
                        text: 'No existe ningun cliente con ese numero de identifiaccion o nombre que esta ingresando'
                    });
                } else {*/
                    $('#mostrar_boton_tercero').hide();
                    $('#tipo_identificacion_id > option[value="' + ui.item.tipo_identificacion_id + '"]').attr('selected', 'selected');
                    $('#naturaleza_id > option[value="' + ui.item.naturaleza_id + '"]').attr('selected', 'selected');
                    $('#tercero_id_autocomplete').val(ui.item.id);
                    $('#primer_nombre').val(ui.item.primer_nombre);
                    $('#segundo_nombre').val(ui.item.segundo_nombre);
                    $('#primer_apellido').val(ui.item.primer_apellido);
                    $('#segundo_apellido').val(ui.item.segundo_apellido);
                    $('#razon_social').val(ui.item.razon_social);
                    $('#telefono').val(ui.item.telefono);
                    $('#nro_celular').val(ui.item.nro_celular);
                    $('#direccion').val(ui.item.direccion);
                    $('#correo_electronico').val(ui.item.correo_electronico);
                //}


            }
        });
        /****************************** FIN DE AUTOCOMPLETAR PARA EL TERCERO**********************************/


        /******************************************GUARDAR DATOS DE LOS CLIENTES O TERCEROS****************************/
        /*GUARDAMOS O  ACTUALIZAMOS LA INFORMACION*/
        $(document).on('click', '#guardar_cliente', function () {
            var nro_identificacion = $("#tercero_id").val();

            json = {
                '_token': $('#footer input[name=_token]').val(),
                'data': {
                    'tipo_identificacion_id': $('#tipo_identificacion_id').val(),
                    'naturaleza_id': $('#naturaleza_id').val(),
                    'primer_nombre': $('#primer_nombre').val(),
                    'segundo_nombre': $('#segundo_nombre').val(),
                    'primer_apellido': $('#primer_apellido').val(),
                    'segundo_apellido': $('#segundo_apellido').val(),
                    'nro_identificacion': $('#tercero_id').val(),
                    'razon_social': $('#razon_social').val(),
                    'telefono': $('#telefono').val(),
                    'nro_celular': $('#nro_celular').val(),
                    'direccion': $('#direccion').val(),
                    'correo_electronico': $('#correo_electronico').val()
                }
            };

            $.httpRequest('/query/tercero', {'method': 'POST', 'dataJson': json}, function () {}, function (dataInsert) {
                alertify.success(dataInsert[0].msj);
                $('#mostrar_boton_tercero').hide();
                $('#tercero_id_autocomplete').val(dataInsert[0].data.id);
            });

        });
        /*FIN DE GUARDAMOS O  ACTUALIZAMOS LA INFORMACION*/
        /******************************************GUARDAR DATOS DE LOS CLIENTES O TERCEROS****************************/


    });

</script>
