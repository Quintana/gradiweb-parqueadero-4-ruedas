<div class="ibox-title">
    <input type="hidden" id="tipo_filtro">
    <div style="float: left;">
        <h3>Listado de usuario ingresados</h3>
    </div>
    <!--<div style="float: right;">
        <div data-toggle="modal" data-target="#infoNormas">
            <button id="new" class="btn btn-primary mostrar"><i class="fa fa-plus"></i></button>
        </div>
    </div>-->
    <div class="ibox-content">
        <div class="row">
            <table id="listado_aplicativo" class="display responsive cell-border listado_aplicativo" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre Completo</th>
                        <th>C&eacute;dula</th>
                        <th>Direcci&oacute;n</th>
                        <th>Tel&eacute;fono</th>
                        <th>Temperatura</th>
                        <th>Fecha y hora de Ingreso</th>
                        <th>Observaciones</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>


    <div class="row">
        <?php echo $__env->make('urbes.control_ingreso_usu.modales_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
</div>
