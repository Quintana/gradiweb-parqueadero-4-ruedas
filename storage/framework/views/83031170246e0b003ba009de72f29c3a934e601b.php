<div class="ibox-title">
    <input type="hidden" id="tipo_filtro">
    <div style="float: left;">
        <h3>Listado de Productos</h3>
    </div>

    <div class="ibox-content">
        <div class="row">
            <table id="listado" class="display responsive cell-border listado" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Opciones</th>
                        <th>descripci&oacute;n</th>
                        
                        <th>Valor de Compra</th>
                        <th>Valor M&iacute;nimo Venta</th>
                        <th>Valor Sugerido Venta</th>
                        <th>Total Unidades</th>
                        <th>Observaciones</th>
                        <th>Marca</th>
                        <th>Color</th>
                        <th>Alto</th>
                        <th>Ancho</th>
                        <th>Otro</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>


    <div class="row">
        <?php echo $__env->make('reparaciones.principal.productos.modales_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
</div>
