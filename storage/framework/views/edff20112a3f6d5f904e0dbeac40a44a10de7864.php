<div class="ibox-title">
    <input type="hidden" id="tipo_filtro">
    <div style="float: left;">
        <h3>Listado de estados generales del sistema</h3>
    </div>
    <!--<div style="float: right;">
        <div data-toggle="modal" data-target="#infoNormas">
            <button id="new" class="btn btn-primary mostrar"><i class="fa fa-plus"></i></button>
        </div>
    </div>-->
    <div class="ibox-content">
        <div class="row">
            <table id="listado_aplicativo" class="display responsive cell-border listado_aplicativo" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Descripci&oacute;n</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>


    <div class="row">
        <?php echo $__env->make('global.parametrizar.estadosGenerales.modales_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
</div>
