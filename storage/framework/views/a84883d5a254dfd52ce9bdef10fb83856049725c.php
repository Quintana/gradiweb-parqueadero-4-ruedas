<div class="ibox-title">
    <input type="hidden" id="tipo_filtro">
    <div style="float: left;">
        <h3>Listado de Tipos de Naturaleza</h3>
    </div>

    <div class="ibox-content">
        <div class="row">
            <table id="listado_aplicativo" class="display responsive cell-border listado_aplicativo" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Descripci&oacute;n</th>
                        <th>Sigla</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>


    <div class="row">
        <?php echo $__env->make('global.parametrizar.tipoNaturaleza.modales_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
</div>
