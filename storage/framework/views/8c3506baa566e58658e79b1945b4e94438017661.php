<!-- Modal para la creacion y actualizacion de la tabla -->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
    <div class="modal-dialog" style="min-width: 60%" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="col-md-12"></div>
            </div>

            <div class="modal-body">
                <div id="spinner" style="text-align: center">
                    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                    <span class="sr-only">Cargando...</span>
                </div>
                <div id="mostrar_info" style="display: none">
                    <div class="row">

                        <div class="form-group col-md-3">
                            <label for="descripcion">Descripci&oacute;n <span style="color: red">*</span></label>
                            <input type="text" id="descripcion" name="descripcion" placeholder="Ingrese la descripci&oacute;n" class="form-control borrar data-required">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="valor_unitario_compra">Valor Unitario de la Compra <span style="color: red">*</span></label>
                            <input type="number" id="valor_unitario_compra" name="valor_unitario_compra" placeholder="Ingrese el valor unitario de la compra" class="form-control borrar data-required">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="total_unidades_compra">Total Unidades Compradas <span style="color: red">*</span></label>
                            <input type="number" id="total_unidades_compra" name="total_unidades_compra" placeholder="Ingrese Total de Unidades" class="form-control borrar data-required">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="valor_minimo_venta">Valor M&iacute;nimo Venta <span style="color: red">*</span></label>
                            <input type="number" id="valor_minimo_venta" name="valor_minimo_venta" placeholder="Ingrese el valor minimo de venta" class="form-control borrar data-required">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="valor_sugerido_venta">Valor Sugerido de Venta <span style="color: red">*</span></label>
                            <input type="number" id="valor_sugerido_venta" name="valor_sugerido_venta" placeholder="Ingrese valor sugerido de venta" class="form-control borrar data-required">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="observacion">Observaciones</label>
                            <input type="text" id="observacion" name="observacion" placeholder="Ingrese una Observaci&oacute;n" class="form-control borrar">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="marca">Marca</label>
                            <input type="text" id="marca" name="marca" placeholder="Ingrese la marca" class="form-control borrar">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="color">Color</label>
                            <input type="text" id="color" name="color" placeholder="Ingrese el Color" class="form-control borrar">
                        </div>

                    </div>
                    <div class="row">

                        <div class="form-group col-md-3">
                            <label for="ancho">Ancho</label>
                            <input type="text" id="ancho" name="ancho" placeholder="Ingrese el ancho" class="form-control borrar">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="alto">Alto</label>
                            <input type="text" id="alto" name="alto" placeholder="Ingrese el alto" class="form-control borrar">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="otros">Otros</label>
                            <input type="text" id="otros" name="otros" placeholder="Ingrese otra referencia" class="form-control borrar">
                        </div>

                        

                    </div>

                </div>
            </div>

            <div class="modal-footer" id="footer">
                <button id="cerrar" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button class="btn btn-primary" id="guardar">Guardar</button>
                <input type="hidden" id="enviar_guardar">
                <?php echo e(csrf_field()); ?>

            </div>
        </div>
    </div>
</div>

<!-- fin Modal para la creacion y actualizacion de la informacion de la tabla -->

<!-- Modal para la Eliminacion de la informacion de la tabla -->

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="col-md-12"></div>
            </div>

            <div class="modal-body">
                <h2>Oprima el boton <strong>'Continuar'</strong> SI Desea Proceder con la eliminaci&oacute;n del registro?</h2>
            </div>

            <div class="modal-footer" id="footer2">
                <button id="cerrar" class="btn btn-default" data-dismiss="modal">Salir</button>
                <button class="btn btn-danger" id="eliminar" data-dismiss="modal">Continuar</button>
                <?php echo e(csrf_field()); ?>

            </div>
        </div>
    </div>
</div>
<!--FIN MODAL-->



<!-- Modal para la creacion y actualizacion de la tabla de categorias de los productos-->
<div class="modal fade" id="categoriaModal" tabindex="-1" role="dialog" aria-labelledby="categoriaModalLabel">
    <div class="modal-dialog" style="min-width: 60%" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="col-md-12"></div>
            </div>

            <div class="modal-body">
                <div id="spinner2" style="text-align: center">
                    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                    <span class="sr-only">Cargando...</span>
                </div>
                <div id="mostrar_info2" style="display: none">
                    <div class="row">

                        <div class="form-group col-md-8">
                            <label for="categorias">Seleccione una Categoria <span style="color: red">*</span></label>
                            <select id="categorias" name="categorias" class="form-control borrar data-required2"></select>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="guardar"></label><br>
                            <button class="btn btn-primary btn-lg" id="guardar_categoria2">Guardar</button>
                        </div>

                    </div>

                    <div class="row">
                        <table id="listado_categorias" class="display responsive cell-border listado_categorias" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Categoria</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

            <div class="modal-footer" id="footer">
                <button id="cerrar" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <?php echo e(csrf_field()); ?>

            </div>
        </div>
    </div>
</div>

<!-- Modal para la Eliminacion de la informacion de la tabla -->

<div class="modal fade" id="deleteCategoriaModal" tabindex="-1" role="dialog" aria-labelledby="deleteCategoriaModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="col-md-12"></div>
            </div>

            <div class="modal-body">
                <h2>Oprima el boton <strong>'Continuar'</strong> SI Desea Proceder con la eliminaci&oacute;n del registro?</h2>
            </div>

            <div class="modal-footer" id="footer2">
                <button id="cerrar" class="btn btn-default" data-dismiss="modal">Salir</button>
                <button class="btn btn-danger" id="eliminar_categoria2" data-dismiss="modal">Continuar</button>
                <?php echo e(csrf_field()); ?>

            </div>
        </div>
    </div>
</div>

<!-- fin Modal para la creacion y actualizacion de la informacion de la tabla de categorias de los productos-->

<!--  Modal para la creacion y actualizacion de la tabla de DETALLE DE PRODUCTOS-->
<div class="modal fade" id="detalleProductoModal" tabindex="-1" role="dialog" aria-labelledby="detalleProductoModalLabel">
    <div class="modal-dialog" style="min-width: 60%" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="col-md-12">
                    <h2><div>Cantidad de art&iacute;culos comprados del producto:</div> <div style="float: left" id="nombre_producto"></div></h2>
                </div>
            </div>

            <div class="modal-body">
                <div id="spinner3" style="text-align: center">
                    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                    <span class="sr-only">Cargando...</span>
                </div>
                <div id="mostrar_info3" style="display: none">

                    <div class="row">
                        <table id="listado_detalles_productos" class="display responsive cell-border listado_detalles_productos" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Serial</th>
                                <th>Otros</th>
                                <th>Observaciones</th>
                                <th>Usuario creador</th>
                                <th>Fecha de creaci&oacute;n</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>

            <div class="modal-footer" id="footer">
                <button id="cerrar" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <?php echo e(csrf_field()); ?>

            </div>
        </div>
    </div>
</div>

<!-- Modal para la Eliminacion de la informacion de la tabla -->

<div class="modal fade" id="deleteModalDetalleProducto" tabindex="-1" role="dialog" aria-labelledby="deleteDetProdModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="col-md-12"></div>
            </div>

            <div class="modal-body">
                <h2>Oprima el boton <strong>'Continuar'</strong> SI Desea Proceder con la eliminaci&oacute;n del registro?</h2>
            </div>

            <div class="modal-footer" id="footer2">
                <button id="cerrar" class="btn btn-default" data-dismiss="modal">Salir</button>
                <button class="btn btn-danger" id="eliminar_det_prod" data-dismiss="modal">Continuar</button>
                <?php echo e(csrf_field()); ?>

            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="infoModalDetalleProducto" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="col-md-12"></div>
            </div>

            <div class="modal-body">
                <div id="spinner4" style="text-align: center">
                    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                    <span class="sr-only">Cargando...</span>
                </div>
                <div id="mostrar_info4" style="display: none">
                    <div class="row">

                        <div class="form-group col-md-6">
                            <label for="serial">Serial</label>
                            <input type="text" id="serial" name="serial" placeholder="Ingrese el serial del producto" class="form-control borrar">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="otros_detalle">Otros</label>
                            <input type="text" id="otros_detalle" name="otros_detalle" placeholder="Ingrese otras caracteristicas del pructo" class="form-control borrar">
                        </div>

                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="observaciones">Observaciones</label>
                            <textarea id="observaciones" name="observaciones" class="form-control borrar"></textarea>
                        </div>
                    </div>

                </div>
            </div>

            <div class="modal-footer" id="footer">
                <button id="cerrar" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button class="btn btn-primary" id="guardar_detalle">Actualizar</button>
                <input type="hidden" id="enviar_guardar_detalle">
                <?php echo e(csrf_field()); ?>

            </div>
        </div>
    </div>
</div>
