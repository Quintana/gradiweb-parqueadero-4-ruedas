<?php $__env->startSection('css'); ?>
    <style>
        .modal-xl {
            min-width: 90%;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <!-- boton flotante -->
    <div class="row" id="small-chat">

        <button id="mostrar_informe" class="btn btn-circle btn-info btn-lg mostrar" data-toggle="tooltip" data-placement="top" title="Mostrar Informe en PDF">
            <i class="fa fa-check"></i>
        </button>

    </div>
    <!-- fin boton flotante -->

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h1 class="pull-left">SISTEMAS DE REPORTES PARA EL CONTROL DE INGRESO DE USUARIO</h1>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="form-group col-md-6">
                                    <label for="fecha_inicio">Fecha Inicio</label>
                                    <input data-date-format="yyyy-mm-dd" id="fecha_inicio" name="fecha_inicio" placeholder="Ingrese Fecha de inicio" class="datepicker form-control borrar data-required">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="fecha_fin">Fecha Fin</label>
                                    <input data-date-format="yyyy-mm-dd" id="fecha_fin" name="fecha_fin" placeholder="Ingrese Fecha de Fin" class="datepicker form-control borrar data-required">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <?php echo $__env->make('urbes.informes.js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>