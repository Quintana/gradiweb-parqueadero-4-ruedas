<?php $__env->startSection('css'); ?>
    <style>
        .modal-xl {
            min-width: 90%;
        }

        .text-on-pannel {
            background: #fff none repeat scroll 0 0;
            height: auto;
            margin-left: 20px;
            padding: 5px 7px;
            position: absolute;
            margin-top: -47px;
            border: 1px solid #337ab7;
            border-radius: 8px;
        }

        .panel {
            /* for text on pannel */
            margin-top: 27px !important;
        }

        .panel-body {
            padding-top: 30px !important;
        }

        .container {
            width: 100% !important;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <!-- boton flotante-->
    <div class="row" id="small-chat">
        <button id="mostrar_factura" class="btn btn-circle btn-info btn-lg mostrar_factura" data-toggle="tooltip" data-placement="top" title="Crear Factura">
            <i class="fa fa-plus"></i>
        </button>
    </div>
    <!-- fin boton flotante

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h1 class="pull-left">Plataforma de Facturaci&oacute;n</h1>
        </div>
    </div>-->

    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="row wrapper border-bottom white-bg page-heading">
                    <h1 style="text-align: center"><b>Plataforma de Facturaci&oacute;n</b></h1>
                    <input type="hidden" id="id_factura" name="id_factura">
                    <input type="hidden" id="numero_factura" name="numero_factura">
                    <?php echo $__env->make('facturacion.principal.menuTabs', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <?php echo $__env->make('facturacion.principal.js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>