<!-- Modal para la Eliminacion de la informacion de la tabla -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="col-md-12"></div>
            </div>

            <div class="modal-body">
                <h2>Oprima el boton <strong>'Continuar'</strong> SI Desea Proceder con la eliminaci&oacute;n del registro?</h2>
            </div>

            <div class="modal-footer" id="footer2">
                <button id="cerrar" class="btn btn-default" data-dismiss="modal">Salir</button>
                <button class="btn btn-danger" id="eliminar" data-dismiss="modal">Continuar</button>
                <?php echo e(csrf_field()); ?>

            </div>
        </div>
    </div>
</div>
<!--FIN MODAL-->
