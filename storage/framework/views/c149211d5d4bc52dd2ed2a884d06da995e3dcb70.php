<!-- Modal para la creacion y actualizacion de la tabla -->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
    <div class="modal-dialog" style="min-width: 60%" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="col-md-12"></div>
            </div>

            <div class="modal-body">
                <div id="spinner" style="text-align: center">
                    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                    <span class="sr-only">Cargando...</span>
                </div>
                <div id="mostrar_info" style="display: none">
                    <div class="row">

                        <div class="form-group col-md-3">
                            <label for="primer_nombre">Primer Nombre <span style="color: red">*</span></label>
                            <input type="text" id="primer_nombre" name="primer_nombre" placeholder="Ingrese el primer nombre" class="form-control borrar data-required">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="segundo_nombre">Segundo Nombre</label>
                            <input type="text" id="segundo_nombre" name="segundo_nombre" placeholder="Ingrese el segundo nombre" class="form-control borrar">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="primer_apellido">Primer Apellido <span style="color: red">*</span></label>
                            <input type="text" id="primer_apellido" name="primer_apellido" placeholder="Ingrese el primer apellido" class="form-control borrar data-required">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="segundo_apellido">Segundo Apellido</label>
                            <input type="text" id="segundo_apellido" name="segundo_apellido" placeholder="Ingrese el segundo apellido" class="form-control borrar">
                        </div>

                    </div>
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="nro_identificacion">Nro. Documento de Identificaci&oacute;n<span style="color: red">*</span></label>
                            <input type="text" id="nro_identificacion" name="nro_identificacion" placeholder="Ingrese su n&uacute;mero de identificaci&oacute;n" class="form-control borrar data-required">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="razon_social">Raz&oacute;n Social</label>
                            <input type="text" id="razon_social" name="razon_social" placeholder="Ingrese la Raz&oacute;n Social" class="form-control borrar">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="tipo_identificacion_id">Tipo Documento <span style="color: red">*</span></label>
                            <select id="tipo_identificacion_id" name="tipo_identificacion_id" class="form-control borrar data-required"></select>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="naturaleza_id">Naturaleza <span style="color: red">*</span></label>
                            <select id="naturaleza_id" name="naturaleza_id" class="form-control borrar data-required"></select>
                        </div>

                    </div>
                    <div class="row">

                        <div class="form-group col-md-3">
                            <label for="telefono">Tel&eacute;fono fijo</label>
                            <input type="text" id="telefono" name="telefono" placeholder="Ingrese un tel&eacute;fono fijo" class="form-control borrar">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="nro_celular">Tel&eacute;fono Celular</label>
                            <input type="text" id="nro_celular" name="nro_celular" placeholder="Ingrese un tel&eacute;fono celular" class="form-control borrar">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="direccion">Direcci&oacute;n</label>
                            <input type="text" id="direccion" name="direccion" placeholder="Ingrese una direcci&oacute;n" class="form-control borrar">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="correo_electronico">Correo Electr&oacute;nico</label>
                            <input type="email" id="correo_electronico" name="correo_electronico" placeholder="Ingrese un Correo Electronico" class="form-control borrar">
                        </div>

                    </div>

                </div>
            </div>

            <div class="modal-footer" id="footer">
                <button id="cerrar" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button class="btn btn-primary" id="guardar">Guardar</button>
                <input type="hidden" id="enviar_guardar">
                <?php echo e(csrf_field()); ?>

            </div>
        </div>
    </div>
</div>

<!-- fin Modal para la creacion y actualizacion de la informacion de la tabla -->

<!-- Modal para la Eliminacion de la informacion de la tabla -->

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="col-md-12"></div>
            </div>

            <div class="modal-body">
                <h2>Oprima el boton <strong>'Continuar'</strong> SI Desea Proceder con la eliminaci&oacute;n del registro?</h2>
            </div>

            <div class="modal-footer" id="footer2">
                <button id="cerrar" class="btn btn-default" data-dismiss="modal">Salir</button>
                <button class="btn btn-danger" id="eliminar" data-dismiss="modal">Continuar</button>
                <?php echo e(csrf_field()); ?>

            </div>
        </div>
    </div>
</div>
<!--FIN MODAL-->
