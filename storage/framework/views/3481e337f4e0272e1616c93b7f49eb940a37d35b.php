<script type="text/javascript">
    var editor; // use a global for the submit and return data rendering in the examples
    $(document).ready(function () {
        var json = []; // definimos la variable vacia que va a contener los datos para dibujar el datatable
        var id = ''; // definimos la variable que va a contener el id para la actualizacion del registro
        var idElimina = ''; // definimos la variable que va a contener el id para la eliminacion del registro

        function dibujarDatatable(data) {
            json = []; // siempre que se llame a esta funcion el va a limpiar la variable para volverla a llenar con la informacion de la tabla
            /*recorremos los y definimos los datos para dibujar en el datatable asi mismo creamos el objeto "opciones" que contiene las diferentes acciones que se van a realizar por cada registro*/
            $.each(data.data, function (idx, val) {
                json.push({
                    id: val.id,
                    opciones:   '<div class="form-inline">' +
                                    '<div data-toggle="modal" class="form-group" data-target="#deleteModal">' +
                                        '<button id="delete" class="delete btn btn-danger btn-circle btn-outline" data-toggle="tooltip"' +
                                        ' data-placement="top" title="Eliminar Registro"' +
                                        ' data-val="' + val.id + '">' +
                                            '<span class="fa fa-trash"></span>' +
                                        '</button>' +
                                    '</div>&nbsp;' +
                                    '<div data-toggle="modal" class="form-group" data-target="#infoModal">' +
                                        '<button id="mostrar" class="mostrar btn btn-success btn-circle btn-outline" data-toggle="tooltip"' +
                                        ' data-placement="top" title="Editar informaci&oacute;n detallada "' +
                                        ' data-val="' + val.id + '">' +
                                            '<span class="fa fa-pencil"></span>' +
                                        '</button>' +
                                    '</div>&nbsp;' +
                                    '<div data-toggle="modal" class="form-group" data-target="#categoriaModal">' +
                                        '<button id="categoria" class="categoria btn btn-info btn-circle btn-outline" data-toggle="tooltip"' +
                                        ' data-placement="top" title="Asignar Categorias al producto"' +
                                        ' data-val="' + val.id + '">' +
                                            '<span class="fa fa-plus"></span>' +
                                        '</button>' +
                                    '</div>&nbsp;' +
                                    '<div data-toggle="modal" class="form-group" data-target="#detalleProductoModal">' +
                                        '<button id="detalle_producto" class="detalle_producto btn btn-warning btn-circle btn-outline" data-toggle="tooltip"' +
                                        ' data-placement="top" title="Detalle del producto"' +
                                        ' data-val="' + val.id + '" data-nombre_producto="' + val.descripcion +'">' +
                                            '<span class="fa fa-search-plus"></span>' +
                                        '</button>' +
                                    '</div>&nbsp;' +
                                '</div>',
                    descripcion : val.descripcion,
                    //desc_categoria: val.desc_categoria,
                    valor_unitario_compra: val.valor_unitario_compra,
                    valor_minimo_venta: val.valor_minimo_venta,
                    valor_sugerido_venta: val.valor_sugerido_venta,
                    total_unidades_compra: val.total_unidades_compra,
                    observacion : val.observacion,
                    marca: val.marca,
                    color: val.color,
                    alto: val.alto,
                    ancho: val.ancho,
                    otros: val.otros
                });
            });

            /*cabecera del datatable*/
            var columns = [
                {'data' : 'id'},
                {'data' : 'opciones'},
                {'data' : 'descripcion'},
                //{'data' : 'desc_categoria'},
                {'data' : 'valor_unitario_compra'},
                {'data' : 'valor_minimo_venta'},
                {'data' : 'valor_sugerido_venta'},
                {'data' : 'total_unidades_compra'},
                {'data' : 'observacion'},
                {'data' : 'marca'},
                {'data' : 'color'},
                {'data' : 'alto'},
                {'data' : 'ancho'},
                {'data' : 'otros'}
            ];

            /*llamamos el plugin para dibujar los datos en el datatable segun el id o class asignado*/
            $('#listado').drawDataTable(json, columns);
        }

        /*FUNCION PARA GENERAR EL LISTADO DE CONSULTA DE LA TABLA*/
        function listado() {
            /*LLAMAMOS EL PLUGIN Y LE PASAOS LOS PARAMETROS PARA DIBUJAR EL DATATABLE*/
            $.httpRequest('/query/listado/productos', {'blockUI': true}, function() {}, function (dataListado) {
                //console.log(dataListado);
                dibujarDatatable(dataListado); //enviamos los datos a la funcion anterior para procesar la informacion y mostrarla posteriormente
            });
        }

        listado(); // generamos la consulta para el listado de todos los datos de la tabla

        /*ACTIVAMOS EL EVENTO CLICK DE LA CLASS "MOSTRAR" Y IDENTIFICAMOS SI ES PARA INSERTAR O PARA ACTUALIZAR UN REGITRO*/
        $(document).on('click', '.mostrar', function() {
            $('#spinner').show(); // activamos el spinner para que el usuario sepa que hay una carga de la informacion
            $('#mostrar_info').hide(); // ocultamos el div en donde se va a mostrar la informacion para actualizar o para insertar

            var actualizaRegistro = $(this).data('val'); // variable para distinguir si es para actualizar o crear un registro

            // validamos que sea para actualizar la informacion sino entra al else para la insercion de la informacion
            if(actualizaRegistro){ // actualizamos la informacion
                $('#enviar_guardar').val('act');
                $.httpRequest('/query/listado/productos/' + actualizaRegistro, {}, function() {}, function (data) {
                    id = data.data[0].id;
                    $('#spinner').hide();
                    $('#mostrar_info').show();

                    $('#descripcion').val(data.data[0].descripcion);
                    $('#valor_unitario_compra').val(data.data[0].valor_unitario_compra);
                    $('#valor_sugerido_venta').val(data.data[0].valor_sugerido_venta);
                    $('#valor_minimo_venta').val(data.data[0].valor_minimo_venta);
                    $('#total_unidades_compra').val(data.data[0].total_unidades_compra);
                    $('#observacion').val(data.data[0].observacion);
                    $('#marca').val(data.data[0].marca);
                    $('#color').val(data.data[0].color);
                    $('#alto').val(data.data[0].alto);
                    $('#ancho').val(data.data[0].ancho);
                    $('#otros').val(data.data[0].otros);
                });
            }else{ // insertamos la informacion
                $('#enviar_guardar').val('new');
                $('#spinner').hide();
                $('#mostrar_info').show();
                $('.borrar').val('');
            }
        });
        /*FIN DE ACTIVAMOS EL EVENTO CLICK DE LA CLASS "MOSTRAR" Y IDENTIFICAMOS SI ES PARA INSERTAR O PARA ACTUALIZAR UN REGITRO*/

        /*GUARDAMOS O  ACTUALIZAMOS LA INFORMACION*/
        $(document).on('click', '#guardar', function () {
            /*VALIDAMOS QUE TODOS LOS ELEMNTOS HTML QUE TENGAN LA CLASE 'data-required' CONTENGAN DATOS DE LOS CONTRARIO
            * NO PODRA ACTUALIZAR NI INSERTAR*/
            var validarDatos = true;

            $(".data-required").each(function(){
                if($(this).val() == ''){
                    validarDatos = false;
                }
            });
            /*SIN DE LA VALIDACION*/

            if(validarDatos === true) {
                if($('#total_unidades_compra').val() > 0) {
                    if ($('#enviar_guardar').val() === 'act') { // PROCESO DE ACTUALIZACION
                        json = {
                            '_token': $('#footer input[name=_token]').val(),
                            'data': {
                                'id': id,
                                'descripcion': $('#descripcion').val(),
                                'valor_unitario_compra': $('#valor_unitario_compra').val(),
                                'valor_sugerido_venta': $('#valor_sugerido_venta').val(),
                                'valor_minimo_venta': $('#valor_minimo_venta').val(),
                                'total_unidades_compra': $('#total_unidades_compra').val(),
                                'observacion': $('#observacion').val(),
                                'marca': $('#marca').val(),
                                'color': $('#color').val(),
                                'alto': $('#alto').val(),
                                'ancho': $('#ancho').val(),
                                'otros': $('#otros').val()
                            }
                        };
                        $.httpRequest('/query/productos', {'method': 'PATCH', 'dataJson': json}, function () {}, function (dataUpdated) {
                            /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                            listado();
                            alertify.success(dataUpdated[0].msj);
                        });
                    } else if ($('#enviar_guardar').val() === 'new') { // PROCESO PARA LA CREACION DE UN REGISTRO
                        json = {
                            '_token': $('#footer input[name=_token]').val(),
                            'data': {
                                'descripcion': $('#descripcion').val(),
                                'valor_unitario_compra': $('#valor_unitario_compra').val(),
                                'valor_sugerido_venta': $('#valor_sugerido_venta').val(),
                                'valor_minimo_venta': $('#valor_minimo_venta').val(),
                                'total_unidades_compra': $('#total_unidades_compra').val(),
                                'observacion': $('#observacion').val(),
                                'marca': $('#marca').val(),
                                'color': $('#color').val(),
                                'alto': $('#alto').val(),
                                'ancho': $('#ancho').val(),
                                'otros': $('#otros').val()
                            }
                        };

                        $.httpRequest('/query/productos', {'method': 'POST', 'dataJson': json}, function () {}, function (dataInsert) {
                            /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                            listado();
                            alertify.success(dataInsert[0].msj);
                        });
                    }
                }else{
                    swal('ATENCION!!!', 'LA CANTIDAD DE PRODUCTOS COMPRADOS DEBE SER SUPERIOR A CERO', "warning");
                }
            }else{
                //alertify.warning('TODOS LOS CAMPOS SON OBLIGATORIOS');
                swal('ATENCION!!!', 'TODOS LOS CAMPOS CON EL * SON OBLIGATORIOS', "warning");
            }
        });
        /*FIN DE GUARDAMOS O  ACTUALIZAMOS LA INFORMACION*/

        /*ELIMINACION POR SOFTWARE DEL REGISTRO*/
        $(document).on('click', '.delete', function () {
            idElimina = $(this).data('val');
        });

        $(document).on('click', '#eliminar', function () {
            if(idElimina !== null){
                json = {
                    '_token': $('#footer2 input[name=_token]').val(),
                    'data': {
                        'id': idElimina
                    }
                };

                $.httpRequest('/query/productos', {'method': 'DELETE', 'dataJson': json}, function(){}, function (dataDelete) {
                    /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                    listado();
                    alertify.success(dataDelete[0].msj);
                });
            }
        });
        /*ELIMINACION POR SOFTWARE DEL REGISTRO*/


        /********************************************ASIGNAMOS LAS CATEGORIAS A LOS PRODUCTOS*************************/
        var json2 = [];
        var id_producto;
        var eliminaCategoria;
        function dibujarDatatable2(data) {
            json2 = []; // siempre que se llame a esta funcion el va a limpiar la variable para volverla a llenar con la informacion de la tabla
            /*recorremos los y definimos los datos para dibujar en el datatable asi mismo creamos el objeto "opciones" que contiene las diferentes acciones que se van a realizar por cada registro*/
            $.each(data.data, function (idx, val) {
                json2.push({
                    id: val.id,
                    descripcion : val.desc_categoria,
                    opciones:   '<div class="form-inline">' +
                                    '<div data-toggle="modal" class="form-group" data-target="#deleteCategoriaModal">' +
                                        '<button id="deleteCategoria" class="deleteCategoria btn btn-danger btn-circle btn-outline" data-toggle="tooltip"' +
                                        ' data-placement="top" title="Eliminar Registro"' +
                                        ' data-val="' + val.id + '">' +
                                            '<span class="fa fa-trash"></span>' +
                                        '</button>' +
                                    '</div>&nbsp;' +
                                '</div>',

                });
            });

            /*cabecera del datatable*/
            var columns = [
                {'data' : 'id'},
                {'data' : 'descripcion'},
                {'data' : 'opciones'},
            ];

            /*llamamos el plugin para dibujar los datos en el datatable segun el id o class asignado*/
            $('#listado_categorias').drawDataTable(json2, columns);
        }

        /*FUNCION PARA GENERAR EL LISTADO DE CONSULTA DE LA TABLA*/
        function listado2(id) {
            /*LLAMAMOS EL PLUGIN Y LE PASAOS LOS PARAMETROS PARA DIBUJAR EL DATATABLE*/
            $.httpRequest('/query/listado/producto_categoria/' + id, {'blockUI': true}, function() {}, function (dataListado) {
                //console.log(dataListado);
                dibujarDatatable2(dataListado); //enviamos los datos a la funcion anterior para procesar la informacion y mostrarla posteriormente
            });
        }

        $(document).on('click', '.categoria', function() {
            id_producto = $(this).data('val');
            $('#spinner2').show(); // activamos el spinner para que el usuario sepa que hay una carga de la informacion
            $('#mostrar_info2').hide(); // ocultamos el div en donde se va a mostrar la informacion para actualizar o para insertar
            listado2(id_producto);
            $.httpRequest('/query/listado/categorias', {}, function() {}, function (data) {
                $('#spinner2').hide();
                $('#mostrar_info2').show();

                $('#categorias').empty();

                $('<option>').val('').text('Seleccione...').appendTo('#categorias');
                $.each(data.data, function (idx, val) {
                    $('<option>').val(val.id).text(val.descripcion).appendTo('#categorias');
                });
            });
        });

        $(document).on('click', '#guardar_categoria2', function () {
            /*VALIDAMOS QUE TODOS LOS ELEMNTOS HTML QUE TENGAN LA CLASE 'data-required' CONTENGAN DATOS DE LOS CONTRARIO
            * NO PODRA ACTUALIZAR NI INSERTAR*/
            var validarDatos2 = true;

            $(".data-required2").each(function () {
                if ($(this).val() == '') {
                    validarDatos2 = false;
                }
            });

            if(validarDatos2 === true) {
                json = {
                    '_token': $('#footer input[name=_token]').val(),
                    'data': {
                        'producto_id': id_producto,
                        'categoria_id': $('#categorias').val()
                    }
                };

                $.httpRequest('/query/producto_categoria', {'method': 'POST', 'dataJson': json}, function () {}, function (dataInsert) {
                    /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                    listado2(id_producto);
                    alertify.success(dataInsert[0].msj);
                });
            }else{
                //alertify.warning('TODOS LOS CAMPOS SON OBLIGATORIOS');
                swal('ATENCION!!!', 'TODOS LOS CAMPOS CON EL * SON OBLIGATORIOS', "warning")
            }
        });

        /*ELIMINACION POR SOFTWARE DEL REGISTRO*/
        $(document).on('click', '.deleteCategoria', function () {
            eliminaCategoria = $(this).data('val');
        });

        $(document).on('click', '#eliminar_categoria2', function () {
            if(eliminaCategoria !== null){
                json = {
                    '_token': $('#footer2 input[name=_token]').val(),
                    'data': {
                        'id': eliminaCategoria
                    }
                };

                $.httpRequest('/query/producto_categoria', {'method': 'DELETE', 'dataJson': json}, function(){}, function (dataDelete) {
                    /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                    listado2(id_producto);
                    alertify.success(dataDelete[0].msj);
                });
            }
        });
        /*ELIMINACION POR SOFTWARE DEL REGISTRO*/
        /********************************************ASIGNAMOS LAS CATEGORIAS A LOS PRODUCTOS*************************/


        /****************************************CODIGO PARA EL DETALLE DEL PRODUCTO*********************************/
        var json = []; // definimos la variable vacia que va a contener los datos para dibujar el datatable

        function dibujarDatatableDetalleProducto(data) {
            json = []; // siempre que se llame a esta funcion el va a limpiar la variable para volverla a llenar con la informacion de la tabla
            /*recorremos los y definimos los datos para dibujar en el datatable asi mismo creamos el objeto "opciones" que contiene las diferentes acciones que se van a realizar por cada registro*/
            $.each(data.data, function (idx, val) {
                json.push({
                    id: val.id,
                    serial : val.serial,
                    otros: val.otros,
                    observaciones: val.observaciones,
                    usuario: val.nombres + ' ' + val.apellidos,
                    fecha_creacion: val.created_at,
                    opciones:   '<div class="form-inline">' +
                                    '<div data-toggle="modal" class="form-group" data-target="#deleteModalDetalleProducto">' +
                                        '<button id="delete_detalle" class="delete_detalle btn btn-danger btn-circle btn-outline" data-toggle="tooltip"' +
                                        ' data-placement="top" title="Eliminar Registro"' +
                                        ' data-val="' + val.id + '">' +
                                            '<span class="fa fa-trash"></span>' +
                                        '</button>' +
                                    '</div>&nbsp;' +
                                    '<div data-toggle="modal" class="form-group" data-target="#infoModalDetalleProducto">' +
                                        '<button id="mostrar_detalle" class="mostrar_detalle btn btn-success btn-circle btn-outline" data-toggle="tooltip"' +
                                        ' data-placement="top" title="Editar informaci&oacute;n"' +
                                        ' data-val="' + val.id + '">' +
                                            '<span class="fa fa-pencil"></span>' +
                                        '</button>' +
                                    '</div>&nbsp;' +
                                '</div>'
                });
            });

            /*cabecera del datatable*/
            var columns = [
                {'data' : 'id', name:'id'},
                {'data' : 'serial', name: 'serial'},
                {'data' : 'otros', name: 'otros'},
                {'data' : 'observaciones', name:'observaciones'},
                {'data' : 'usuario', name:'usuario'},
                {'data' : 'fecha_creacion', name:'fecha_creacion'},
                {'data' : 'opciones', name: 'opciones'},
            ];

            /*editor = new $.fn.dataTable.Editor( {
                ajax: "/query/listado/detalle_producto/prueba",
                table: "#listado_detalles_productos",
                fields: columns
            } );

            // Activate an inline edit on click of a table cell
            $('#listado_detalles_productos').on( 'click', 'tbody td:not(:first-child)', function (e) {
                editor.inline( table.cell( this ).index(), {
                    onBlur: 'submit'
                } );
            } );*/

            /*llamamos el plugin para dibujar los datos en el datatable segun el id o class asignado*/
            $('#listado_detalles_productos').drawDataTable(json, columns);
        }

        var actualizaDetalleProducto='';
        var idDetalleProducto = '';

        function listadoDetalle(id){
            $.httpRequest('/query/listado/detalle_producto/' + id, {}, function() {}, function (data) {
                if( Object.keys(data.data).length === 0 ){
                    swal('ATENCION!!!', 'NO HAY CANTIDAD DE PRODUCTOS A MOSTRAR', "warning");
                    $('#detalleProductoModal').modal('hide');
                }else {
                    idDetalleProducto = data.data[0].id;
                    dibujarDatatableDetalleProducto(data);
                    $('#spinner3').hide();
                    $('#mostrar_info3').show();
                }
            });
        }

        $(document).on('click', '.detalle_producto', function() {
            $('#spinner3').show(); // activamos el spinner para que el usuario sepa que hay una carga de la informacion
            $('#mostrar_info3').hide(); // ocultamos el div en donde se va a mostrar la informacion para actualizar o para insertar

            actualizaDetalleProducto = $(this).data('val'); // variable para distinguir si es para actualizar o crear un registro
            $("#nombre_producto").html( $(this).data('nombre_producto') );

            // validamos que sea para actualizar la informacion sino entra al else para la insercion de la informacion
            if(actualizaDetalleProducto){ // actualizamos la informacion
                $('#enviar_guardar').val('act');
                listadoDetalle(actualizaDetalleProducto);
            }else{ // insertamos la informacion
                $('#enviar_guardar').val('new');
                $('#spinner3').hide();
                $('#mostrar_info3').show();
                $('.borrar').val('');
            }
        });


        var idDetalle = '';
        var actualizaDetalle = '';
        var idProducto='';
        $(document).on('click', '.mostrar_detalle', function() {
            $('#spinner4').show(); // activamos el spinner para que el usuario sepa que hay una carga de la informacion
            $('#mostrar_info4').hide(); // ocultamos el div en donde se va a mostrar la informacion para actualizar o para insertar

            actualizaDetalle = $(this).data('val'); // variable para distinguir si es para actualizar o crear un registro

            // validamos que sea para actualizar la informacion sino entra al else para la insercion de la informacion
            if(actualizaDetalle){ // actualizamos la informacion
                $('#enviar_guardar').val('act');
                $.httpRequest('/query/listado/detalle_producto/detalle/' + actualizaDetalle, {}, function() {}, function (data) {

                    idDetalle = data.data[0].id;
                    idProducto = data.data[0].producto_id;
                    $('#spinner4').hide();
                    $('#mostrar_info4').show();

                    $('#serial').val(data.data[0].serial);
                    $('#otros_detalle').val(data.data[0].otros);
                    $('#observaciones').val(data.data[0].observaciones);

                });
            }else{ // insertamos la informacion
                $('#enviar_guardar').val('new');
                $('#spinner4').hide();
                $('#mostrar_info4').show();
                $('.borrar').val('');
            }
        });

        $(document).on('click', '#guardar_detalle', function () {

            json = {
                '_token': $('#footer input[name=_token]').val(),
                'data': {
                    'id': idDetalle,
                    'serial': $('#serial').val(),
                    'otros': $('#otros_detalle').val(),
                    'observaciones': $('#observaciones').val(),
                }
            };
            $.httpRequest('/query/detalle_productos', {'method': 'PATCH', 'dataJson': json}, function () {}, function (dataUpdated) {
                /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                listadoDetalle(idProducto);
                alertify.success(dataUpdated[0].msj);
            });

        });

        /*ELIMINACION POR SOFTWARE DEL REGISTRO*/
        var eliminarDetalle='';
        $(document).on('click', '.delete_detalle', function () {
            eliminarDetalle = $(this).data('val');
        });

        $(document).on('click', '#eliminar_det_prod', function () {
            if(eliminarDetalle !== null){
                json = {
                    '_token': $('#footer2 input[name=_token]').val(),
                    'data': {
                        'id': eliminarDetalle
                    }
                };

                $.httpRequest('/query/detalle_productos', {'method': 'DELETE', 'dataJson': json}, function(){}, function (dataDelete) {
                    /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                    listadoDetalle(actualizaDetalleProducto);
                    alertify.success(dataDelete[0].msj);
                });
            }
        });
        /*ELIMINACION POR SOFTWARE DEL REGISTRO*/

    });

</script>
