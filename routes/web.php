<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


Auth::routes();
Route::middleware(['auth'])->get('/home', 'HomeController@index')->name('home');

/*RUTAS ADMINISTRATIVAS LAS CUALES DEBEN ESTAR PARAMETRIZADAS EN LA BASE DE DATOS POR USUARIO PARA QUE PUEDAN SER ACCEDIDAS*/
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'menu']], function (){
    Route::get('aplicativo', 'Globales\Menu\aplicativosController@index');
    Route::get('modulos', 'Globales\Menu\modulosController@index');
    Route::get('menu', 'Globales\Menu\menuController@index');

    Route::get('usuarios', 'Globales\usuariosController@index');
    Route::get('estados_generales', 'Globales\estadoController@index');
    Route::get('tipo_documento', 'Globales\tipoIdentificacionController@index');
    Route::get('tipo_naturaleza', 'Globales\tipoNaturalezaController@index');
    Route::get('tipo_vehiculo', 'Parqueadero\parqueaderoTrTipoVehiculoController@index');
    Route::get('tercero_vehiculo', 'Globales\tercerosController@index');
    Route::get('demo', 'Globales\tercerosController@demo');
});
/*FIN DE RUTAS ADMINISTRATIVAS LAS CUALES DEBEN ESTAR PARAMETRIZADAS EN LA BASE DE DATOS POR USUARIO PARA QUE PUEDAN SER ACCEDIDAS*/

/*RUTAS PARA LAS DIFERENTES CONSULTAS NECESARIAS PARA CADA PANTALLA*/
Route::group(['prefix' => 'query', 'middleware' => ['auth', 'menu']], function (){
    /*CONSULTAS PARA LA TABLA DE USUARIOS*/
    Route::get('listado/usuarios', 'Globales\usuariosController@getAll');
    Route::get('listado/usuarios/{id}', 'Globales\usuariosController@getAllById');
    Route::patch('modifica/usuarios', 'Globales\usuariosController@update');
    Route::delete('usuarios', 'Globales\usuariosController@destroy');
    /*FIN DE LAS CONSULTAS PARA LA TABLA DE USUARIOS*/
    /*CONSULTAS PARA LA TABLA DE MENUS*/
    //APLICATIVOS
    Route::get('listado/aplicativos', 'Globales\Menu\aplicativosController@getAll');
    Route::get('listado/aplicativos/{id}', 'Globales\Menu\aplicativosController@getAllById');
    Route::post('aplicativos', 'Globales\Menu\aplicativosController@create');
    Route::patch('aplicativos', 'Globales\Menu\aplicativosController@update');
    Route::delete('aplicativos', 'Globales\Menu\aplicativosController@destroy');
    //MODULOS
    Route::get('listado/modulos', 'Globales\Menu\modulosController@getAll');
    Route::get('listado/modulos/{id}', 'Globales\Menu\modulosController@getAllById');
    Route::post('modulos', 'Globales\Menu\modulosController@create');
    Route::patch('modulos', 'Globales\Menu\modulosController@update');
    Route::delete('modulos', 'Globales\Menu\modulosController@destroy');
    Route::get('listado/modulos_por_aplicativo/{id}', 'Globales\Menu\modulosController@getByAplicativo');
    //MENUS
    Route::get('listado/menu', 'Globales\Menu\menuController@getAll');
    Route::get('listado/menu/{id}', 'Globales\Menu\menuController@getAllById');
    Route::post('menu', 'Globales\Menu\menuController@create');
    Route::patch('menu', 'Globales\Menu\menuController@update');
    Route::delete('menu', 'Globales\Menu\menuController@destroy');
    //ESTADOS GENERALES
    Route::get('listado/estados_generales', 'Globales\estadoController@getAll');
    Route::get('listado/estados_generales/{id}', 'Globales\estadoController@getAllById');
    Route::post('estados_generales', 'Globales\estadoController@create');
    Route::patch('estados_generales', 'Globales\estadoController@update');
    Route::delete('estados_generales', 'Globales\estadoController@destroy');
    //ASIGNACION DE MENU A LOS USUARIOS DEL SISTEMA
    Route::get('listado/asignacion_menu', 'Globales\Menu\asignacionMenuController@getAll');
    Route::get('listado/asignacion_menu/{id}', 'Globales\Menu\asignacionMenuController@getAllById');
    Route::post('asignacion_menu', 'Globales\Menu\asignacionMenuController@create');
    Route::patch('asignacion_menu', 'Globales\Menu\asignacionMenuController@update');
    Route::delete('asignacion_menu', 'Globales\Menu\asignacionMenuController@destroy');
    //TIPO IDENTIFICACION
    Route::get('listado/tipo_identificacion', 'Globales\tipoIdentificacionController@getAll');
    Route::get('listado/tipo_identificacion/{id}', 'Globales\tipoIdentificacionController@getAllById');
    Route::post('tipo_identificacion', 'Globales\tipoIdentificacionController@create');
    Route::patch('tipo_identificacion', 'Globales\tipoIdentificacionController@update');
    Route::delete('tipo_identificacion', 'Globales\tipoIdentificacionController@destroy');
    //TIPO NATURALEZA
    Route::get('listado/tipo_naturaleza', 'Globales\tipoNaturalezaController@getAll');
    Route::get('listado/tipo_naturaleza/{id}', 'Globales\tipoNaturalezaController@getAllById');
    Route::post('tipo_naturaleza', 'Globales\tipoNaturalezaController@create');
    Route::patch('tipo_naturaleza', 'Globales\tipoNaturalezaController@update');
    Route::delete('tipo_naturaleza', 'Globales\tipoNaturalezaController@destroy');
    //TIPO DE VEHICULO
    Route::get('listado/tipo_vehiculo', 'Parqueadero\parqueaderoTrTipoVehiculoController@getAll');
    Route::get('listado/tipo_vehiculo/{id}', 'Parqueadero\parqueaderoTrTipoVehiculoController@getAllById');
    Route::post('tipo_vehiculo', 'Parqueadero\parqueaderoTrTipoVehiculoController@create');
    Route::patch('tipo_vehiculo', 'Parqueadero\parqueaderoTrTipoVehiculoController@update');
    Route::delete('tipo_vehiculo', 'Parqueadero\parqueaderoTrTipoVehiculoController@destroy');
    //TERCEROS, CLIENTES O PROPIETARIOS E INGRESO DEL VEHICULO
    Route::get('listado/tercero', 'Globales\tercerosController@getAll');
    Route::get('listado/tercero/{id}', 'Globales\tercerosController@getAllById');
    Route::post('tercero', 'Globales\tercerosController@create');
    Route::patch('tercero', 'Globales\tercerosController@update');
    Route::delete('tercero', 'Globales\tercerosController@destroy');
    Route::post('tercero_vehiculo/autocompletar', 'Globales\tercerosController@getAutocomplete');
});
/*FIN DE RUTAS PARA LAS DIFERENTES CONSULTAS NECESARIAS PARA CADA PANTALLA*/