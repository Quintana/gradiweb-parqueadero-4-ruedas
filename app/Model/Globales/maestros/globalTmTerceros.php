<?php

namespace App\Model\Globales\maestros;

use App\Model\Parqueadero\parqueaderoTmVehiculo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class globalTmTerceros extends Model
{
    use SoftDeletes;

    public $table = 'global_tm_terceros';

    protected $primaryKey = 'id';

    public $fillable = ['fk_id_usuario',
                        'tipo_identificacion_id',
                        'naturaleza_id',
                        'primer_nombre',
                        'segundo_nombre',
                        'primer_apellido',
                        'segundo_apellido',
                        'razon_social',
                        'nro_identificacion',
                        'telefono',
                        'direccion',
                        'correo_electronico',
                        'nro_celular',
                        'created_at'];

    protected $hidden = ['updated_at', 'deleted_at'];

    public $timestamps = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /*CONSULTAMOS TODOS LOS DATOS DE LA TABLA*/
    public function listado()
    {
        $data = self::join('global_tr_tipo_identificacion as iden', 'iden.id', 'global_tm_terceros.tipo_identificacion_id')
            ->join('global_tr_naturaleza as nat', 'nat.id', 'global_tm_terceros.naturaleza_id')
            ->join('users as us', 'us.id', 'global_tm_terceros.fk_id_usuario')
            ->join('parqueadero_tm_vehiculo as par', 'global_tm_terceros.id', 'par.fk_id_tercero')
            ->join('parqueadero_tr_tipo_vehiculo as trpar', 'trpar.id', 'par.fk_id_tipo_vehiculo')
            ->select('global_tm_terceros.*', 'nat.descripcion as desc_naturaleza', 'nat.id as id_naturaleza', 'nat.sigla as sigla_naturaleza',
                'iden.id as id_tipo_identificacion', 'iden.descripcion as desc_tipo_identificacion', 'iden.sigla as sigla_tipo_identificacion',
                'us.nombres', 'us.apellidos', 'par.placa', 'par.marca', 'par.observacion as obs_vehiculo', 'trpar.descripcion as desc_tipo_vehiculo', 'par.id as id_vehiculo')
            ->orderBy('par.marca', 'desc')
            ->get();
        return $data;
    }

    /*CONSULTAMOS TODOS LOS DATOS DE LA TABLA POR ID*/
    public function listadoPorId($id)
    {
        $data = self::join('global_tr_tipo_identificacion as iden', 'iden.id', 'global_tm_terceros.tipo_identificacion_id')
            ->join('global_tr_naturaleza as nat', 'nat.id', 'global_tm_terceros.naturaleza_id')
            ->join('parqueadero_tm_vehiculo as par', 'global_tm_terceros.id', 'par.fk_id_tercero')
            ->join('parqueadero_tr_tipo_vehiculo as trpar', 'trpar.id', 'par.fk_id_tipo_vehiculo')
            ->select('global_tm_terceros.*', 'nat.descripcion as desc_naturaleza', 'nat.id as id_naturaleza', 'nat.sigla as sigla_naturaleza',
                'iden.id as id_tipo_identificacion', 'iden.descripcion as desc_tipo_identificacion', 'iden.sigla as sigla_tipo_identificacion',
                'par.placa', 'par.marca', 'par.observacion as obs_vehiculo', 'par.fk_id_tipo_vehiculo', 'trpar.descripcion as desc_tipo_vehiculo', 'par.id as id_vehiculo')
            ->where('par.id', '=', $id)
            ->get();
        return $data;
    }

    //CREAMOS EL REGISTRO EN LA TABLA
    public function crearRegistro($data){
        try {
            DB::beginTransaction();

            $consultaTercero = self::where('nro_identificacion', '=', $data['nro_identificacion'])->get();

            if(count($consultaTercero) == 0) { // CONTAMOS LOS REGISTROS Y SI NO HAY NINGUNO ES PORQUE NO EXIXTE EL TERCERO O PROPIETARIO Y TOCA CREARLO EN EL SISTEMA

                /*CREAMOS EL TERCERO, CLIENTE O PROPIETARIO*/
                $dataInsert = [
                    'tipo_identificacion_id' => $data['tipo_identificacion_id'],
                    'naturaleza_id' => $data['naturaleza_id'],
                    'primer_nombre' => trim(strtoupper($data['primer_nombre'])),
                    'segundo_nombre' => trim(strtoupper($data['segundo_nombre'])),
                    'primer_apellido' => trim(strtoupper($data['primer_apellido'])),
                    'segundo_apellido' => trim(strtoupper($data['segundo_apellido'])),
                    'razon_social' => trim(strtoupper($data['razon_social'])),
                    'nro_identificacion' => $data['nro_identificacion'],
                    'telefono' => $data['telefono'],
                    'direccion' => trim(strtoupper($data['direccion'])),
                    'correo_electronico' => trim(strtoupper($data['correo_electronico'])),
                    'nro_celular' => $data['nro_celular'],
                    'fk_id_usuario' => auth()->user()->id
                ];

                $insertTercero = self::insertGetId($dataInsert); // insertamos y consultamos el id ingresado
                /*FIN DE CREAMOS TERCERO, CLIENTE O PROPIETARIO*/
            }else{
                $insertTercero = $consultaTercero[0]->id;
            }

            /*CREAMOS EL REGISTRO DEL VEHICUO*/

            $dataInsertTmVehiculo = [
                'fk_id_usuario' => auth()->user()->id,
                'fk_id_tipo_vehiculo' => $data['fk_id_tipo_vehiculo'],
                'fk_id_tercero' => $insertTercero,
                'placa' => trim(strtoupper($data['placa'])),
                'marca' => trim(strtoupper($data['marca'])),
                'observacion' => trim(strtoupper($data['observacion']))
            ];

            $insertVehiculo = parqueaderoTmVehiculo::create($dataInsertTmVehiculo);

            /*FIN DE CREAMOS REGISTRO DEL VEHICULO*/


            DB::commit();
            return $insertVehiculo;
        }catch (Exception $e){
            DB::rollBack();
            return $e;
        }
    }

    /*ACTUALIZAMOS LA INFORMACION DE LA TABLA*/
    public function actualizaInfo($data) {
        try {
            DB::beginTransaction();

            /*ACTUALIZAMOS EL TERCERO, CLIENTE O PROPIETARIO*/
            $updateTercero = self::where('nro_identificacion', '=', $data['nro_identificacion'])
                ->update([
                    'tipo_identificacion_id' => $data['tipo_identificacion_id'],
                    'naturaleza_id' => $data['naturaleza_id'],
                    'primer_nombre' => trim(strtoupper($data['primer_nombre'])),
                    'segundo_nombre' => trim(strtoupper($data['segundo_nombre'])),
                    'primer_apellido' => trim(strtoupper($data['primer_apellido'])),
                    'segundo_apellido' => trim(strtoupper($data['segundo_apellido'])),
                    'razon_social' => trim(strtoupper($data['razon_social'])),
                    'nro_identificacion' => $data['nro_identificacion'],
                    'telefono' => $data['telefono'],
                    'direccion' => trim(strtoupper($data['direccion'])),
                    'correo_electronico' => trim(strtoupper($data['correo_electronico'])),
                    'nro_celular' => $data['nro_celular'],
                    'fk_id_usuario' => auth()->user()->id
                ]);

            /*ACTUALIZAMOS LOS DATOS DEL VEHICULO*/
            $updateVehiculo = parqueaderoTmVehiculo::where('id', '=', $data['id'])
                ->update([
                    'fk_id_usuario' => auth()->user()->id,
                    'fk_id_tipo_vehiculo' => $data['fk_id_tipo_vehiculo'],
                    'placa' => trim(strtoupper($data['placa'])),
                    'marca' => trim(strtoupper($data['marca'])),
                    'observacion' => trim(strtoupper($data['observacion']))
                ]);


            DB::commit();
            return $updateVehiculo;
        }catch (Exception $e){
            DB::rollBack();
            return $e;
        }
    }

    /*REALIZAMOS LA CONSULTA PARA EL AUTOCOMPLETAR Y ASI PODER REALIZAR EL FILTRO DE BUSQUEDA REQUERIDO*/
    public function autocompletar($filtro){

        $data = self::join('global_tr_tipo_identificacion as iden', 'iden.id', 'global_tm_terceros.tipo_identificacion_id')
            ->join('global_tr_naturaleza as nat', 'nat.id', 'global_tm_terceros.naturaleza_id')
            ->join('users as us', 'us.id', 'global_tm_terceros.fk_id_usuario')
            ->join('parqueadero_tm_vehiculo as par', 'global_tm_terceros.id', 'par.fk_id_tercero')
            ->join('parqueadero_tr_tipo_vehiculo as trpar', 'trpar.id', 'par.fk_id_tipo_vehiculo')
            ->select('global_tm_terceros.*', 'nat.descripcion as desc_naturaleza', 'nat.id as id_naturaleza', 'nat.sigla as sigla_naturaleza',
                'iden.id as id_tipo_identificacion', 'iden.descripcion as desc_tipo_identificacion', 'iden.sigla as sigla_tipo_identificacion',
                'us.nombres', 'us.apellidos', 'par.placa', 'par.marca', 'par.observacion as obs_vehiculo', 'trpar.descripcion as desc_tipo_vehiculo', 'par.id as id_vehiculo',
                'par.fk_id_tipo_vehiculo')
            ->where('global_tm_terceros.nro_identificacion', 'LIKE', $filtro.'%')
            ->orWhere('global_tm_terceros.primer_nombre', 'LIKE', $filtro.'%')
            ->orWhere('global_tm_terceros.segundo_nombre', 'LIKE', $filtro.'%')
            ->orWhere('global_tm_terceros.primer_apellido', 'LIKE', $filtro.'%')
            ->orWhere('global_tm_terceros.segundo_apellido', 'LIKE', $filtro.'%')
            ->orWhere('global_tm_terceros.razon_social', 'LIKE', $filtro.'%')
            ->orWhere('par.placa', 'LIKE', $filtro.'%')
            ->orWhere('par.marca', 'LIKE', $filtro.'%')
            ->get();

        return $data;
    }
}
