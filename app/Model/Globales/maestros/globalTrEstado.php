<?php

namespace App\Model\Globales;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class globalTrEstado extends Model
{
    use SoftDeletes;

    public $table = 'global_tr_estados';

    protected $primaryKey = 'id';

    public $fillable = ['nombre', 'fk_id_usuario'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public $timestamps = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /*CONSULTAMOS TODOS LOS DATOS DE LA TABLA*/
    public function listado()
    {
        $data = self::get();
        return $data;
    }

    /*CONSULTAMOS TODOS LOS DATOS DE LA TABLA POR ID*/
    public function listadoPorId($id)
    {
        $data = self::where('id', '=', $id)->get();
        return $data;
    }

    /*ACTUALIZAMOS LA INFORMACION DE LA TABLA*/
    public function actualizaInfo($data)
    {

        $data1 = self::where('id', '=', $data['id'])
            ->update([
                'nombre' => $data['nombre']
            ]);

        return $data1;
    }

    public function crearRegistro($data)
    {

        $dataInsert = [
            'nombre' => strtoupper($data['nombre']),
            'fk_id_usuario' => auth()->user()->id
        ];

        return self::create($dataInsert);
    }


    /*HACEMOS DELETE A LA TABLA*/
    public function eliminaInfo($data)
    {
        $data = self::where('id', '=', $data['id'])
            ->delete();

        return $data;
    }
}