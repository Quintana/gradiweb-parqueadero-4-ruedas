<?php

namespace App\Model\Globales;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class usuarios extends Model
{
    use SoftDeletes;

    public $table = 'users';

    protected $primaryKey = 'id';

    public $fillable = ['nombres', 'apellidos', 'fk_id_tipo_identificacion',
                        'identificacion', 'direccion', 'telefono_fijo', 'telefono_movil', 'email'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public $timestamps = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /*CONSULTAMOS TODOS LOS USUARIOS*/
    public function listado() {
        $data = DB::table('users as us')
            ->join('global_tr_tipo_identificacion as iden', 'iden.id', 'us.fk_id_tipo_identificacion')
            ->select('us.id','us.nombres', 'us.apellidos', 'us.fk_id_tipo_identificacion',
                     'us.identificacion', 'us.direccion', 'us.telefono_fijo', 'us.telefono_movil', 'us.email',
                     'iden.descripcion as des_tipo_identificacion', 'iden.sigla as sigla_tipo_identificacion')
            ->whereNull('us.deleted_at')
            ->orderBy('us.nombres', 'asc')->get();

        return $data;
    }

    /*CONSULTAMOS UN USUARIO POR ID*/
    public function usuario($id) {
        $data = self::where('id', $id)->whereNull('deleted_at')->get();

        return $data;
    }

    /*ACTUALIZAMOS LA INFORMACION DE LOS USUARIOS*/
    public function actualizaInfo($data) {

        $data1 = DB::table('users')
                    ->where('id', '=', $data['id'])
                    ->update([
                        'nombres' => $data['nombres'],
                        'apellidos' => $data['apellidos'],
                        'fk_id_tipo_identificacion' => $data['fk_id_tipo_identificacion'],
                        'identificacion' => $data['identificacion'],
                        'telefono_fijo' => $data['telefono_fijo'],
                        'telefono_movil' => $data['telefono_movil'],
                        'direccion' => $data['direccion'],
                        'email' => $data['email']
                    ]);

        return $data1;
    }

    /*HACEMOS DELETE A LA TABLA*/
    public function eliminaInfo($data){
        $data = self::where('id', '=', $data['id'])
                    ->delete();

        return $data;
    }
}
