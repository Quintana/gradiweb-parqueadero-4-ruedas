<?php

namespace App\Model\Globales\Menu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class globalTrMenu extends Model
{
    use SoftDeletes;

    public $table = 'global_tr_menu';

    protected $primaryKey = 'id';

    public $fillable = ['fk_id_modulo', 'descripcion', 'url', 'icono', 'orden', 'mostrar_en_menu'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public $timestamps = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /*CONSULTAMOS TODOS LOS DATOS DE LA TABLA*/
    public function listado()
    {
        $data = DB::table('global_tr_menu as menu')
            ->join('global_tr_modulos as modul', 'modul.id', 'menu.fk_id_modulo')
            ->join('global_tr_aplicativos as apli' , 'apli.id', 'modul.fk_id_aplicativo')
            ->select('menu.id', 'menu.descripcion', 'menu.url', 'menu.icono', 'menu.orden', 'menu.mostrar_en_menu',
                     'modul.id as id_modulo', 'modul.descripcion as descripcion_modulo',
                     'apli.id as id_aplicativo', 'apli.descripcion as descripcion_aplicativo')
            ->whereNull('menu.deleted_at')->get();
        return $data;
    }

    /*CONSULTAMOS TODOS LOS DATOS DE LA TABLA POR ID*/
    public function listadoPorId($id)
    {
        $data = DB::table('global_tr_menu as menu')
            ->join('global_tr_modulos as modul', 'modul.id', 'menu.fk_id_modulo')
            ->join('global_tr_aplicativos as apli' , 'apli.id', 'modul.fk_id_aplicativo')
            ->select('menu.id', 'menu.descripcion', 'menu.url', 'menu.icono', 'menu.orden', 'menu.mostrar_en_menu',
                     'modul.id as id_modulo', 'modul.descripcion as descripcion_modulo',
                     'apli.id as id_aplicativo', 'apli.descripcion as descripcion_aplicativo')
            ->where('menu.id', '=', $id)
            ->whereNull('menu.deleted_at')->get();
        return $data;
    }

    /*ACTUALIZAMOS LA INFORMACION DE LA TABLA*/
    public function actualizaInfo($data) {

        $data1 = self::where('id', '=', $data['id'])
            ->update([
                'fk_id_modulo' => $data['fk_id_modulo'],
                'descripcion' => trim($data['descripcion']),
                'url' => $data['url'],
                'icono' => $data['icono'],
                'orden' => $data['orden'],
                'mostrar_en_menu' => $data['mostrar_en_menu']
            ]);

        return $data1;
    }

    public function crearRegistro($data){

        $dataInsert = [
            'fk_id_modulo' => $data['fk_id_modulo'],
            'descripcion' => trim($data['descripcion']),
            'url' => $data['url'],
            'icono' => $data['icono'],
            'orden' => $data['orden'],
            'mostrar_en_menu' => $data['mostrar_en_menu']
        ];

        return self::create($dataInsert);
    }


    /*HACEMOS DELETE A LA TABLA*/
    public function eliminaInfo($data){
        $data = self::where('id', '=', $data['id'])
            ->delete();

        return $data;
    }
}
