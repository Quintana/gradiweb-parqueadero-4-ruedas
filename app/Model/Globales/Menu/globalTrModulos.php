<?php

namespace App\Model\Globales\Menu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class globalTrModulos extends Model
{
    use SoftDeletes;

    public $table = 'global_tr_modulos';

    protected $primaryKey = 'id';

    public $fillable = ['fk_id_aplicativo', 'descripcion','icono','orden'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public $timestamps = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /*CONSULTAMOS LOS MODULOS QUE EXISTEN POR APLICATIVO*/
    public function listadoPorAplicativo($idAplicativo){
        return self::select('id', 'descripcion', 'fk_id_aplicativo')
            ->where('fk_id_aplicativo', '=', $idAplicativo)
            ->whereNull('deleted_at')
            ->get();
    }

    /*CONSULTAMOS TODOS LOS DATOS DE LA TABLA*/
    public function listado()
    {
        $data = DB::table('global_tr_modulos as modul')
            ->join('global_tr_aplicativos as apli' , 'apli.id', 'modul.fk_id_aplicativo')
            ->select('modul.id', 'modul.fk_id_aplicativo', 'modul.descripcion', 'modul.icono', 'modul.orden',
                     'apli.descripcion as descripcion_aplicativo')
            ->whereNull('modul.deleted_at')->get();
        return $data;
    }

    /*CONSULTAMOS TODOS LOS DATOS DE LA TABLA POR ID*/
    public function listadoPorId($id)
    {
        $data = self::where('id', '=', $id)->get();
        return $data;
    }

    /*ACTUALIZAMOS LA INFORMACION DE LA TABLA*/
    public function actualizaInfo($data) {

        $data1 = self::where('id', '=', $data['id'])
            ->update([
                'fk_id_aplicativo' => $data['fk_id_aplicativo'],
                'descripcion' => $data['descripcion'],
                'icono' => $data['icono'],
                'orden' => $data['orden']
            ]);

        return $data1;
    }

    public function crearRegistro($data){

        $dataInsert = [
            'fk_id_aplicativo' => $data['fk_id_aplicativo'],
            'descripcion' => strtoupper($data['descripcion']),
            'icono' => $data['icono'],
            'orden' => $data['orden']
        ];

        return self::create($dataInsert);
    }


    /*HACEMOS DELETE A LA TABLA*/
    public function eliminaInfo($data){
        $data = self::where('id', '=', $data['id'])
            ->delete();

        return $data;
    }
}
