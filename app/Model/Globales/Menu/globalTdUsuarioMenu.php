<?php

namespace App\Model\Globales\Menu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class globalTdUsuarioMenu extends Model
{
    use SoftDeletes;

    public $table = 'global_td_usuario_menu';

    protected $primaryKey = 'id';

    public $fillable = ['fk_id_usuario','fk_id_menu','fk_id_permiso', 'estado'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public $timestamps = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /*CONSULTAMOS TODOS LOS DATOS DE LA TABLA*/
    /*CONSULTAMOS TODOS LOS DATOS DE LA TABLA POR ID*/
    public function listadoPorId($id)
    {
        $data = self::where('fk_id_usuario', '=', $id)->whereNull('deleted_at')->get();
        return $data;
    }

    /*ACTUALIZAMOS LA INFORMACION DE LA TABLA*/
    public function actualizaInfo($data) {

        $data1 = self::where('id', '=', $data['id'])
            ->update([
                'fk_id_usuario' => $data['fk_id_usuario'],
                'fk_id_menu' => $data['fk_id_menu'],
                'fk_id_permiso' => $data['fk_id_permiso'],
                'estado' => $data['estado']
            ]);

        return $data1;
    }

    public function crearRegistro($data){

        $usuario = '';

        /*PRIMERO RECORREMOS LOS DATOS DE LA ASIGNACION DEL MENU Y LOS ELIMINAMOS*/
        self::where('fk_id_usuario', '=', $data[0]['fk_id_usuario'])->whereNull('deleted_at')->delete();

        /*RECORREMOS TODA LA INFORMACION ENVIADA Y LA INSERTAMOS EN LA TABLA*/
        foreach ($data as $idx => $val){
            $usuario = $val['fk_id_usuario'];
            /*ACTUALIZAMOS TODOS LOS REGISTROS DEJANDOLOS INACTIVOS PARA LUEGO ELIMINARLOS POR EL SOFTDELETE*/
            //self::where('fk_id_usuario', '=', $usuario)->whereNull('deleted_at')->delete();
            /*PRIMERO ELIMINAMOS TODOS LOS REGISTROS QUE TENGA EL USUARIO PARA VOLVER Y ASIGNARLE CADA UNO DE LOS PERMISOS SOBRE LOS DIFERENTES MENU*/
            //self::where('fk_id_usuario', '=', $usuario)->whereNull('deleted_at')->delete();

            $dataInsert = [
                'fk_id_usuario' => $val['fk_id_usuario'],
                'fk_id_menu' => $val['fk_id_menu'],
                'fk_id_permiso' => $val['fk_id_permiso'],
                'estado' => 'A'
            ];

            self::create($dataInsert);
        }

        return self::where('id', '=', $usuario)->get();
    }


    /*HACEMOS DELETE A LA TABLA*/
    public function eliminaInfo($data){
        $data = self::where('id', '=', $data['id'])->delete();
        return $data;
    }
}
