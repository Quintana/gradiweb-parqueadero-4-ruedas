<?php

namespace App\Model\Parqueadero;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class parqueaderoTmVehiculo extends Model
{
    use SoftDeletes;

    public $table = 'parqueadero_tm_vehiculo';

    protected $primaryKey = 'id';

    public $fillable = ['fk_id_usuario', 'fk_id_tipo_vehiculo', 'fk_id_tercero', 'placa', 'marca', 'observacion', 'created_at'];

    protected $hidden = ['updated_at', 'deleted_at'];

    public $timestamps = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /*HACEMOS DELETE A LA TABLA*/
    public function eliminaInfo($data)
    {
        $data = self::where('id', '=', $data['id'])
            ->delete();

        return $data;
    }
}
