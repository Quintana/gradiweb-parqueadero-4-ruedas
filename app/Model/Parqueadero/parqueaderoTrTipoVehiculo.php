<?php

namespace App\Model\Parqueadero;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class parqueaderoTrTipoVehiculo extends Model
{
    use SoftDeletes;

    public $table = 'parqueadero_tr_tipo_vehiculo';

    protected $primaryKey = 'id';

    public $fillable = ['descripcion', 'fk_id_usuario'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public $timestamps = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /*CONSULTAMOS TODOS LOS DATOS DE LA TABLA*/
    public function listado()
    {
        $data = self::orderBy('descripcion', 'asc')->get();
        return $data;
    }

    /*CONSULTAMOS TODOS LOS DATOS DE LA TABLA POR ID*/
    public function listadoPorId($id)
    {
        $data = self::where('id', '=', $id)->get();
        return $data;
    }

    //creamos el registro en la tabla
    public function crearRegistro($data)
    {

        $dataInsert = [
            'descripcion' => trim(strtoupper($data['descripcion'])),
            'fk_id_usuario' => auth()->user()->id
        ];

        return self::create($dataInsert);
    }

    /*ACTUALIZAMOS LA INFORMACION DE LA TABLA*/
    public function actualizaInfo($data)
    {

        $data1 = self::where('id', '=', $data['id'])
            ->update([
                'descripcion' => trim(strtoupper($data['descripcion']))
            ]);

        return $data1;
    }

    /*HACEMOS DELETE A LA TABLA*/
    public function eliminaInfo($data)
    {
        $data = self::where('id', '=', $data['id'])
            ->delete();

        return $data;
    }
}
