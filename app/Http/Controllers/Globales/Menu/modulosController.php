<?php

namespace App\Http\Controllers\Globales\Menu;

use App\Model\Globales\Menu\globalTrModulos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class modulosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view('global.aplicativos');
        return view('global.menu.modulos.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $allRequest = $request->all();
        $getData = new globalTrModulos();

        $jsonRespuesta = array([
            'data' => $getData->crearRegistro($allRequest['data']),
            'msj' => 'Se han Insertado los Registros Exitosamente'
        ]);

        return response()->json($jsonRespuesta);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $allRequest = $request->all();
        $getData = new globalTrModulos();

        $jsonRespuesta = array([
            'data' => $getData->actualizaInfo($allRequest['data']),
            'msj' => 'Se han Actualizado los Registros Exitosamente'
        ]);

        return response()->json($jsonRespuesta);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = new globalTrModulos();
        $dataAll = $request->all();
        //$this->validator($request->data)->validate();

        $jsonRespuesta = array([
            'data' => $data->eliminaInfo($dataAll['data']),
            'msj' => 'Se han Actualizado los Registros Exitosamente'
        ]);

        return response()->json($jsonRespuesta);
    }

    /*CONSULTAR TODOS LOS DATOS DE LA TABLA*/
    public function getAll(){
        $data = new globalTrModulos();

        $jsonRespuesta = array('data' => $data->listado());

        return response()->json($jsonRespuesta);
    }
    /*FIN DE CONSULTAR TODOS LOS DATOS DE LA TABLA*/

    /*CONSULTAR TODOS LOS DATOS DE LA TABLA POR ID*/
    public function getAllById($id){
        $data = new globalTrModulos();

        $jsonRespuesta = array('data' => $data->listadoPorId($id));

        return response()->json($jsonRespuesta);
    }
    /*FIN DE CONSULTAR TODOS LOS DATOS DE LA TABLA*/

    /*CONSULTAR TODOS LOS DATOS DEPENDIENDO DEL APLICATIVO*/
    public function getByAplicativo($id){
        $data = new globalTrModulos();

        $jsonRespuesta = array('data' => $data->listadoPorAplicativo($id));

        return response()->json($jsonRespuesta);
    }
}
