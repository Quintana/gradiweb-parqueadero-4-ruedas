<?php

namespace App\Http\Controllers\Globales;

use App\Model\Globales\maestros\globalTmTerceros;
use App\Model\Parqueadero\parqueaderoTmVehiculo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class tercerosController extends Controller
{
    protected function validator(array $data)
    {
        $messages = [
            'required' => 'Este Campo es requerido',
            'max' => 'Maximo :max caracteres',
            'min' => 'Minimo :min caracteres'
        ];
        return Validator::make($data, [
            'tipo_identificacion_id' => 'required',
            'naturaleza_id' => 'required',
            'nro_identificacion' => 'required|string|min:2|max:120',
            'telefono_fijo' => 'max:10',
            'telefono_movil' => 'max:15',
            'direccion' => 'max:120',
            'email' => 'string|email|max:120'
        ],$messages);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('global.parametrizar.terceros.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validator($request->data)->validate();
        $allRequest = $request->all();
        $getData = new globalTmTerceros();

        $jsonRespuesta = array([
            'data' => $getData->crearRegistro($allRequest['data']),
            'msj' => 'Se han Insertado los Registros Exitosamente'
        ]);

        return response()->json($jsonRespuesta);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validator($request->data)->validate();
        $allRequest = $request->all();
        $getData = new globalTmTerceros();

        $jsonRespuesta = array([
            'data' => $getData->actualizaInfo($allRequest['data']),
            'msj' => 'Se han Actualizado los Registros Exitosamente'
        ]);

        return response()->json($jsonRespuesta);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = new parqueaderoTmVehiculo();
        $dataAll = $request->all();

        $jsonRespuesta = array([
            'data' => $data->eliminaInfo($dataAll['data']),
            'msj' => 'Se han Actualizado los Registros Exitosamente'
        ]);

        return response()->json($jsonRespuesta);
    }

    /*CONSULTAR TODOS LOS DATOS DE LA TABLA*/
    public function getAll(){
        $data = new globalTmTerceros();

        $jsonRespuesta = array('data' => $data->listado());

        return response()->json($jsonRespuesta);
    }
    /*FIN DE CONSULTAR TODOS LOS DATOS DE LA TABLA*/

    /*CONSULTAR TODOS LOS DATOS DE LA TABLA POR ID*/
    public function getAllById($id){
        $data = new globalTmTerceros();

        $jsonRespuesta = array('data' => $data->listadoPorId($id));

        return response()->json($jsonRespuesta);
    }
    /*FIN DE CONSULTAR TODOS LOS DATOS DE LA TABLA*/

    /*METODO PARA AUTOCOMPLETAR LOS DATOS DEL TERCERO*/
    public function getAutocomplete(Request $request) {
        if(request()->ajax()) {
            $data = new globalTmTerceros();

            $jsonRespuesta = array('data' => $data->autocompletar($request->all()['filtro']));

            return response()->json($jsonRespuesta);
        }
    }


    /************************************DEMO***************************************************/
    public function demo(){
        return view('parqueadero.demo.index');
    }
}
