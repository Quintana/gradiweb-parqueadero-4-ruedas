<?php

namespace App\Http\Controllers\Globales;

//use App\Model\Globales\siachTmTerceroModel;
use App\Model\Globales\usuarios;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class usuariosController extends Controller
{
    protected function validator(array $data)
    {
        $messages = [
            'required' => 'Este Campo es requerido',
            'max' => 'Maximo :max caracteres',
            'min' => 'Minimo :min caracteres'
        ];
        return Validator::make($data, [
            'nombres' => 'required|string|min:2|max:255',
            'apellidos' => 'required|string|min:2|max:255',
            'fk_id_tipo_identificacion' => 'required',
            'identificacion' => 'required|string|min:2|max:255',
            'telefono_fijo' => 'string|max:10',
            'telefono_movil' => 'string|max:15',
            'direccion' => 'string|min:5|max:255',
            'email' => 'required|string|email|max:255'
        ],$messages);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('global.usuarios');
    }

    /*
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /*
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /*
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /*
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /*
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //return $request->data;
        /*validamos la informacion recibida*/
        $data = new usuarios();
        $dataAll = $request->all();
        $this->validator($request->data)->validate();

        $jsonRespuesta = array([
            'data' => $data->actualizaInfo($dataAll['data']),
            'msj' => 'Se han Actualizado los Registros Exitosamente'
        ]);

        return response()->json($jsonRespuesta);
    }

    /*
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = new usuarios();
        $dataAll = $request->all();
        //$this->validator($request->data)->validate();

        $jsonRespuesta = array([
            'data' => $data->eliminaInfo($dataAll['data']),
            'msj' => 'Se han Actualizado los Registros Exitosamente'
        ]);

        return response()->json($jsonRespuesta);
    }

    /*RETORNAMOS TODOS LOS DATOS DE LOS USUARIOS*/
    public function getAll(){
        $data = new usuarios();

        $jsonRespuesta = array('data' => $data->listado());

        return response()->json($jsonRespuesta);
    }
    /*RETORNAMOS TODOS LOS DATOS DE LOS USUARIOS*/
    public function getAllById($id){
        $data = new usuarios();

        $jsonRespuesta = array('data' => $data->usuario($id));

        return response()->json($jsonRespuesta);
    }
}
