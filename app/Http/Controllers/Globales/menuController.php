<?php

namespace App\Http\Controllers\Globales;

use App\Model\Globales\siachTrMenu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class menuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('global.menu');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        dd($request->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /*RETORNAMOS TODOS LOS DATOS PARA DIBUJAR EL MENU*/
    public function getAll(){
        $data = new siachTrMenu();

        $jsonRespuesta = array('data' => $data->listarMenu());

        return json_encode($jsonRespuesta);
    }

    /*RETORNAMOS TODOS LOS DATOS DEL MENU PADRE*/
    public function menuPadre(){
        $data = new siachTrMenu();

        $jsonRespuesta = array('data' => $data->listarMenuPadre());

        return json_encode($jsonRespuesta);
    }


}
