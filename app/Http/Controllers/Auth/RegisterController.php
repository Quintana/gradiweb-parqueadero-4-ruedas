<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        Session::flash('message', 'El usuario se creo exitosamente.');

        return view('auth.register');

        /*COMENTARIANDO ESTAS LINEAS EVITAMOS EL LOGIN Y EL REDIRECCIONAMIENTO*/
        /*$this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());*/
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'menu']);
        //$this->middleware(['guest']);
    }


    /*
     * Where to redirect users after registration.
     *
     * @var string
     */
    /*protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *-
     * @return void
     */

    /*entra como invitado*/
    /*public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'required' => 'Este Campo es requerido',
            'max' => 'Maximo :max caracteres',
            'min' => 'Minimo :min caracteres',
            'password.confirmed' => 'El campo de confirmaci&oacute;n no coincide con la contrase&ntilde;a actual',
            'email.unique' => 'Este Email ya esta registrado en nuestra base de datos'
        ];
        return Validator::make($data, [
            'nombres' => 'required|string|min:2|max:255',
            'apellidos' => 'required|string|min:2|max:255',
            'fk_id_tipo_identificacion' => 'required',
            'identificacion' => 'required|string|min:5|max:255',
            'telefono_fijo' => 'string|max:10',
            'telefono_movil' => 'string|max:15',
            'direccion' => 'string|min:5|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ],$messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        /*$tercero = new siachTmTerceroModel();

        $insert = [
            'nombres' => $data['nombres'],
            'apellidos' => $data['apellidos'],
            'telefono_fijo' => $data['telefono_fijo'],
            'telefono_movil' => $data['telefono_movil'],
            'direccion' => $data['direccion']
        ];

        $createTercero = $tercero::create($insert);

        $consultaTerceros = DB::table('siach_tm_terceros')->get();

        $ultimoRegistro = $consultaTerceros->last();*/

        $insertUer = [
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'nombres' => $data['nombres'],
            'apellidos' => $data['apellidos'],
            'fk_id_tipo_identificacion' => $data['fk_id_tipo_identificacion'],
            'identificacion' => $data['identificacion'],
            'telefono_fijo' => $data['telefono_fijo'],
            'telefono_movil' => $data['telefono_movil'],
            'direccion' => $data['direccion'],
            'fk_id_usuario' => auth()->user()->id
        ];
        $crearUsuario = User::create($insertUer);

        return $crearUsuario;
    }
}