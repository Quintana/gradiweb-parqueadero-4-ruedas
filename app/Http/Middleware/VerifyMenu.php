<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class VerifyMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        //dd($request->getPathInfo()); // con esto validamos la url a la que estan tratando de hacer cualquier peticion y aqui es en donde debemos validar si tiene permisos para hacer o consumir el metodo

        if(Auth::check() === true){
            if (!$request->ajax()) { // validamos que las peticiones que no sean ajax se validen - esto aplicaria solamente para el menu
                if(Auth::check() === true) {
                    $data = DB::table('global_tr_menu as menu')
                        ->join('global_td_usuario_menu as usume', 'menu.id', 'usume.fk_id_menu')
                        ->select('menu.*')
                        ->where('usume.fk_id_usuario', auth()->user()->id)
                        ->where('menu.url', '=', $request->getPathInfo())
                        ->where('usume.estado', 'A')
                        ->whereNull('usume.deleted_at')
                        ->count();
                    if ($data === 0) {
                        Session::flash('message', 'No tiene permisos para la ruta que esta tratando de acceder');
                        return redirect('/home');
                    }
                }else{
                    return response('', 401);
                }
            }

            if ($request->ajax()) {
                if(Auth::check() === true) {
                    if ($request->server()['HTTP_REFERER']) { // este es la url que se visualiza en la parte de arriba del navegador
                        //dd(auth()->user()->id.' url: '.json_encode(explode('/',$request->server()['HTTP_REFERER'])));
                        // explotamos la ruta o url principal
                        $explotarRutaAbsoluta = explode('/', $request->server()['HTTP_REFERER']);
                        // consultamos todos los datos de la tabla menu que contiene todo nuestro menu globalizado que se muestra o no en la pantalla de menu
                        $dataMenu = DB::table('global_tr_menu as menu')->get();
                        // variable que nos va a guardar todas las coincidencias de los dos arreglos
                        $contenerdorMenu = array();
                        // recorremos la consulta del menu
                        foreach ($dataMenu as $menu) {
                            // explotamos cada una de las rutas de la consulta para tener nuestro segundo arreglo ya con esto podemos comaprar
                            $explotarMenu = explode('/', $menu->url);
                            // agregamos en un arreglo vacio los datos iguales que hay entre los 2 arreglos anteriores
                            array_push($contenerdorMenu, array_intersect($explotarRutaAbsoluta, $explotarMenu));
                        }
                        // variable que va a contener las diferentes rutas
                        $validarRutaFinal = '';
                        // arreglo vacio que me va a contener en la primera posicion la ruta final
                        $contieneRutaFinal = array();
                        // recorremos el arreglo con los valores iguales de los 2 arreglos anteriores
                        foreach ($contenerdorMenu as $formarMenu) {
                            $validarRutaFinal = ''; // siempre volvemos vacia la variable para que no sigla concatenando y asi poder hacer la validacion correctamente
                            // recorremos el segundo nivel del arreglo que contiene los valores iguales entre los 2 arreglos anteriores
                            foreach ($formarMenu as $formarMenu2) {
                                // comparamos que los valores no sean vacios
                                if ($formarMenu2 != '') {
                                    // concatenamos los valores para luego compararlos con los datos de la base de datos y asi poder saber que ruta estamos usando
                                    $validarRutaFinal .= '/' . $formarMenu2;
                                }
                            }
                            // recorremos nuevamente la consulta de la tabla menu
                            foreach ($dataMenu as $menu) {
                                // validamos que los datos en la base de datos sea iguales a los de la variable
                                if ($menu->url == $validarRutaFinal) {
                                    // si son iguales guardamos la ruta en un arreglo y asi saber cual es la ruta a la cual el usuario esta tratando de acceder
                                    // y asi poder hacer las validaciones en cuanto a permisos
                                    array_push($contieneRutaFinal, $validarRutaFinal);
                                }
                            }

                        }

                        /*cremos la variable que nos va contener el metodo url que se esta tratando de utilizar por parte del usuario*/
                        $metodo = '';
                        $desc = '';
                        if ($request->isMethod('get')) {
                            $metodo = 'GET';
                            $desc .= 'Consulta, ';
                        } elseif ($request->isMethod('post')) {
                            $metodo = 'POST';
                            $desc .= 'Insercion, ';
                        } elseif ($request->isMethod('patch')) {
                            $metodo = 'PATCH';
                            $desc .= 'Actualizacion, ';
                        } elseif ($request->isMethod('put')) {
                            $metodo = 'PUT';
                            $desc .= 'Actualizacion, ';
                        } elseif ($request->isMethod('delete')) {
                            $metodo = 'DELETE';
                            $desc .= 'Eliminacion ';
                        }

                        //dd($contieneRutaFinal[0]); // visualizar mejor en mozilla ya que en chrome no se refleja el resultado en mozilla si muestra el resultado
                        $dataMenuPermisos = DB::table('global_td_usuario_menu as usu')
                            ->join('global_tr_menu as menu', 'usu.fk_id_menu', 'menu.id')
                            ->join('global_tr_permisos as per', 'usu.fk_id_permiso', 'per.id')
                            ->selectRaw('distinct(menu.id), menu.url')
                            ->where('menu.url', $contieneRutaFinal[0])
                            ->where('usu.fk_id_usuario', '=', auth()->user()->id)
                            ->where('per.descripcion', $metodo)
                            ->where('usu.deleted_at', '=', null)
                            ->get();
                        // validamos que si la consulta arroja una cantidad de cero registros significa que no tiene permisos
                        // por consiguiente devuelve una respuesta con un error 403 para advertir al usuario que no tiene privilegios sobre el metodo a usar
                        if (count($dataMenuPermisos) === 0) {
                            //retornamos un status 403 que significa que no tiene permisos sobre el metodo que esta tratando de efectuar
                            return response('', 403);
                        }
                    }
                }else{
                    return response('', 401);
                }
            }
        }else{
            return response('', 401);
        }

        return $next($request);
    }
}
