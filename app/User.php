<?php

namespace App;

use App\Notifications\CambiarPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    public $table = 'users';

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    //protected $fillable = ['fk_id_tercero', 'email', 'password'];
    protected $fillable = ['nombres', 'apellidos', 'fk_id_tipo_identificacion',
                           'identificacion', 'telefono_fijo', 'telefono_movil', 'direccion', 'email', 'password', 'fk_id_usuario'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public $timestamps = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /*CONSULTAMOS TODOS LOS USUARIOS*/
    public function listado() {
        $data = DB::self()->whereNull('deleted_at')->orderBy('id', 'desc')->get();

        return $data;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CambiarPassword($token));
    }
}
