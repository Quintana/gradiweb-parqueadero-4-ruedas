<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class permisosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('global_tr_permisos')->insert([
            'descripcion' => 'POST'
        ]);
        DB::table('global_tr_permisos')->insert([
            'descripcion' => 'PATCH'
        ]);
        DB::table('global_tr_permisos')->insert([
            'descripcion' => 'GET'
        ]);
        DB::table('global_tr_permisos')->insert([
            'descripcion' => 'DELETE'
        ]);
    }
}
