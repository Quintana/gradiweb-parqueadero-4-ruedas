<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class usuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nombres' => 'Darwin Eder',
            'apellidos' => 'Quintana Chala',
            'fk_id_tipo_identificacion' => 1,
            'identificacion' => 1110493353,
            'direccion' => 'Cra 2a No. 5-22',
            'telefono_fijo' => '2624026',
            'telefono_movil' => '3164611659',
            'email' => 'darwinquintana44@gmail.com',
            'password' => bcrypt('secreto'),
            'remember_token' => bcrypt('secreto'),
            'fk_id_usuario' => '1'
        ]);
    }
}
