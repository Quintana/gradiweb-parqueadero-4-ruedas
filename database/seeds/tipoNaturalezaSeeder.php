<?php

use Illuminate\Database\Seeder;

class tipoNaturalezaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('global_tr_naturaleza')->insert([
            'fk_id_usuario' => '1',
            'descripcion' => 'NATURAL',
            'sigla' => 'NT'
        ]);

        DB::table('global_tr_naturaleza')->insert([
            'fk_id_usuario' => '1',
            'descripcion' => 'JURIDICA',
            'sigla' => 'JU'
        ]);
    }
}
