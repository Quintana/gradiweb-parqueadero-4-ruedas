<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class usuariosMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '1',
            'fk_id_permiso' => '1',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '1',
            'fk_id_permiso' => '2',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '1',
            'fk_id_permiso' => '3',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '1',
            'fk_id_permiso' => '4',
            'estado' => 'A',
        ]);


        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '2',
            'fk_id_permiso' => '3',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '2',
            'fk_id_permiso' => '1',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '2',
            'fk_id_permiso' => '4',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '2',
            'fk_id_permiso' => '2',
            'estado' => 'A',
        ]);


        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '3',
            'fk_id_permiso' => '1',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '3',
            'fk_id_permiso' => '2',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '3',
            'fk_id_permiso' => '3',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '3',
            'fk_id_permiso' => '4',
            'estado' => 'A',
        ]);

        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '4',
            'fk_id_permiso' => '1',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '4',
            'fk_id_permiso' => '2',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '4',
            'fk_id_permiso' => '3',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '4',
            'fk_id_permiso' => '4',
            'estado' => 'A',
        ]);

        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '5',
            'fk_id_permiso' => '1',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '5',
            'fk_id_permiso' => '2',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '5',
            'fk_id_permiso' => '3',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '5',
            'fk_id_permiso' => '4',
            'estado' => 'A',
        ]);

        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '6',
            'fk_id_permiso' => '1',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '6',
            'fk_id_permiso' => '2',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '6',
            'fk_id_permiso' => '3',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '6',
            'fk_id_permiso' => '4',
            'estado' => 'A',
        ]);

        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '7',
            'fk_id_permiso' => '1',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '7',
            'fk_id_permiso' => '2',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '7',
            'fk_id_permiso' => '3',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '7',
            'fk_id_permiso' => '4',
            'estado' => 'A',
        ]);

        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '8',
            'fk_id_permiso' => '1',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '8',
            'fk_id_permiso' => '2',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '8',
            'fk_id_permiso' => '3',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '8',
            'fk_id_permiso' => '4',
            'estado' => 'A',
        ]);

        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '9',
            'fk_id_permiso' => '1',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '9',
            'fk_id_permiso' => '2',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '9',
            'fk_id_permiso' => '3',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '9',
            'fk_id_permiso' => '4',
            'estado' => 'A',
        ]);

        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '10',
            'fk_id_permiso' => '1',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '10',
            'fk_id_permiso' => '2',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '10',
            'fk_id_permiso' => '3',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '10',
            'fk_id_permiso' => '4',
            'estado' => 'A',
        ]);

        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '11',
            'fk_id_permiso' => '1',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '11',
            'fk_id_permiso' => '2',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '11',
            'fk_id_permiso' => '3',
            'estado' => 'A',
        ]);
        DB::table('global_td_usuario_menu')->insert([
            'fk_id_usuario' => '1',
            'fk_id_menu' => '11',
            'fk_id_permiso' => '4',
            'estado' => 'A',
        ]);
    }
}
