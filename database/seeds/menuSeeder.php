<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class menuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('global_tr_menu')->insert([
            'fk_id_modulo' => '1',
            'descripcion' => 'Registro de usuarios',
            'url' => '/register',
            'icono' => 'fa fa-user-plus',
            'orden' => '1',
            'mostrar_en_menu' => 'NO',
        ]);
        DB::table('global_tr_menu')->insert([
            'fk_id_modulo' => '1',
            'descripcion' => 'Listar usuarios',
            'url' => '/admin/usuarios',
            'icono' => 'fa fa-list-ol',
            'orden' => '2',
            'mostrar_en_menu' => 'SI',
        ]);
        DB::table('global_tr_menu')->insert([
            'fk_id_modulo' => '2',
            'descripcion' => 'Aplicativos',
            'url' => '/admin/aplicativo',
            'icono' => 'fa fa-cog',
            'orden' => '1',
            'mostrar_en_menu' => 'SI',
        ]);
        DB::table('global_tr_menu')->insert([
            'fk_id_modulo' => '2',
            'descripcion' => 'Modulos',
            'url' => '/admin/modulos',
            'icono' => 'fa fa-cog',
            'orden' => '2',
            'mostrar_en_menu' => 'SI',
        ]);
        DB::table('global_tr_menu')->insert([
            'fk_id_modulo' => '2',
            'descripcion' => 'Menu',
            'url' => '/admin/menu',
            'icono' => 'fa fa-cog',
            'orden' => '3',
            'mostrar_en_menu' => 'SI',
        ]);
        DB::table('global_tr_menu')->insert([
            'fk_id_modulo' => '3',
            'descripcion' => 'Estados Generales',
            'url' => '/admin/estados_generales',
            'icono' => 'fa fa-list-ol',
            'orden' => '1',
            'mostrar_en_menu' => 'SI',
        ]);
        DB::table('global_tr_menu')->insert([
            'fk_id_modulo' => '3',
            'descripcion' => 'Tipo Documento',
            'url' => '/admin/tipo_documento',
            'icono' => 'fa fa-id-card-o',
            'orden' => '2',
            'mostrar_en_menu' => 'SI',
        ]);
        DB::table('global_tr_menu')->insert([
            'fk_id_modulo' => '3',
            'descripcion' => 'Tipo Naturaleza',
            'url' => '/admin/tipo_naturaleza',
            'icono' => 'fa fa-money',
            'orden' => '3',
            'mostrar_en_menu' => 'SI',
        ]);
        DB::table('global_tr_menu')->insert([
            'fk_id_modulo' => '4',
            'descripcion' => 'Listar',
            'url' => '/admin/tipo_vehiculo',
            'icono' => 'fa fa-bicycle',
            'orden' => '1',
            'mostrar_en_menu' => 'SI',
        ]);
        DB::table('global_tr_menu')->insert([
            'fk_id_modulo' => '5',
            'descripcion' => 'Listar',
            'url' => '/admin/tercero_vehiculo',
            'icono' => 'fa fa-id-card',
            'orden' => '1',
            'mostrar_en_menu' => 'SI',
        ]);
        DB::table('global_tr_menu')->insert([
            'fk_id_modulo' => '6',
            'descripcion' => 'Listar',
            'url' => '/admin/demo',
            'icono' => 'fa fa-info-circle',
            'orden' => '1',
            'mostrar_en_menu' => 'SI',
        ]);
    }
}
