<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(tipoIdentificacionSeeder::class);
        $this->call(usuariosSeeder::class);
        $this->call(permisosSeeder::class);
        $this->call(aplicativoSeeder::class);
        $this->call(moduloSeeder::class);
        $this->call(menuSeeder::class);
        $this->call(usuariosMenuSeeder::class);
        $this->call(estadosGeneralesSeeder::class);
        $this->call(tipoNaturalezaSeeder::class);
        $this->call(tipoVehiculoSeeder::class);
    }
}
