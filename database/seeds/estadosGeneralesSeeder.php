<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class estadosGeneralesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('global_tr_estados')->insert([
            'nombre' => 'ACTIVO',
            'fk_id_usuario' => '1',
        ]);
        DB::table('global_tr_estados')->insert([
            'nombre' => 'INACTIVO',
            'fk_id_usuario' => '1',
        ]);
    }
}
