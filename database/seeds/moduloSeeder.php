<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class moduloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('global_tr_modulos')->insert([
            'fk_id_aplicativo' => '1',
            'descripcion' => 'USUARIOS',
            'icono' => 'fa fa-user',
            'orden' => '1',
        ]);
        DB::table('global_tr_modulos')->insert([
            'fk_id_aplicativo' => '1',
            'descripcion' => 'HERRAMIENTAS',
            'icono' => 'fa fa-archive',
            'orden' => '2',
        ]);
        DB::table('global_tr_modulos')->insert([
            'fk_id_aplicativo' => '1',
            'descripcion' => 'PARAMETRIZAR',
            'icono' => 'fa fa-users',
            'orden' => '3',
        ]);
        DB::table('global_tr_modulos')->insert([
            'fk_id_aplicativo' => '2',
            'descripcion' => 'TIPO VEHICULO',
            'icono' => 'fa fa-keyboard-o',
            'orden' => '1',
        ]);
        DB::table('global_tr_modulos')->insert([
            'fk_id_aplicativo' => '2',
            'descripcion' => 'INGRESO DE VEHICULOS',
            'icono' => 'fa fa-user-plus',
            'orden' => '2',
        ]);

        DB::table('global_tr_modulos')->insert([
            'fk_id_aplicativo' => '2',
            'descripcion' => 'DEMO',
            'icono' => 'fa fa-info',
            'orden' => '2',
        ]);
    }
}
