<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class tipoVehiculoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('parqueadero_tr_tipo_vehiculo')->insert([
            'fk_id_usuario' => '1',
            'descripcion' => 'CARRO'
        ]);

        DB::table('parqueadero_tr_tipo_vehiculo')->insert([
            'fk_id_usuario' => '1',
            'descripcion' => 'MOTO'
        ]);
    }
}
