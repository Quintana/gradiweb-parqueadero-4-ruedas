<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class tipoIdentificacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('global_tr_tipo_identificacion')->insert([
            'descripcion' => 'CEDULA DE CIUDADANIA',
            'sigla' => 'CC',
            'fk_id_usuario' => '1',
        ]);
        DB::table('global_tr_tipo_identificacion')->insert([
            'descripcion' => 'CEDULA DE EXTRANJERIA',
            'sigla' => 'CE',
            'fk_id_usuario' => '1',
        ]);
        DB::table('global_tr_tipo_identificacion')->insert([
            'descripcion' => 'TARJETA DE IDENTIDAD',
            'sigla' => 'TI',
            'fk_id_usuario' => '1',
        ]);
    }
}
