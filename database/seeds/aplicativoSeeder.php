<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class aplicativoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('global_tr_aplicativos')->insert([
            'descripcion' => 'ADMINISTRACION',
            'icono' => 'fa fa-cog',
            'orden' => '1',
        ]);
        DB::table('global_tr_aplicativos')->insert([
            'descripcion' => 'PARQUEADERO',
            'icono' => 'fa fa-car',
            'orden' => '2',
        ]);
    }
}
