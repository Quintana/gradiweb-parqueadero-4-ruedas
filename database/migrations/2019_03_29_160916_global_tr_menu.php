<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GlobalTrMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_tr_menu', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('fk_id_modulo')->unsigned();
            $table->string('descripcion');
            $table->string('url');
            $table->string('icono');
            $table->integer('orden')->nullable();
            $table->string('mostrar_en_menu');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('fk_id_modulo')->references('id')->on('global_tr_modulos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_tr_menu');
    }
}
