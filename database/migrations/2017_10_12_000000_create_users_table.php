<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombres');
            $table->string('apellidos');
            $table->integer('fk_id_tipo_identificacion')->unsigned();
            $table->string('identificacion', 120);
            $table->string('direccion')->nullable();
            $table->string('telefono_fijo')->nullable();
            $table->string('telefono_movil')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('fk_id_usuario');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('fk_id_tipo_identificacion')->references('id')->on('global_tr_tipo_identificacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
