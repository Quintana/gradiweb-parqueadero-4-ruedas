<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ParqueaderoTrTipoVehiculo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parqueadero_tr_tipo_vehiculo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_id_usuario')->unsigned();
            $table->string('descripcion');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('fk_id_usuario')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parqueadero_tr_tipo_vehiculo');
    }
}
