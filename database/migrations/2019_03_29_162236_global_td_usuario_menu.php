<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GlobalTdUsuarioMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_td_usuario_menu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_id_usuario')->unsigned();
            $table->integer('fk_id_menu')->unsigned();
            $table->integer('fk_id_permiso')->unsigned();
            $table->string('estado');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('fk_id_usuario')->references('id')->on('users');
            $table->foreign('fk_id_menu')->references('id')->on('global_tr_menu');
            $table->foreign('fk_id_permiso')->references('id')->on('global_tr_permisos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_td_usuario_menu');
    }
}
