<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GlobalTrModulos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_tr_modulos', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('fk_id_aplicativo')->unsigned();
            $table->string('descripcion', 120);
            $table->string('icono');
            $table->string('orden');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('fk_id_aplicativo')->references('id')->on('global_tr_aplicativos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_tr_modulos');
    }
}
