<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ParqueaderoTmVehiculo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parqueadero_tm_vehiculo', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('fk_id_usuario')->unsigned();
            $table->integer('fk_id_tipo_vehiculo')->unsigned();
            $table->integer('fk_id_tercero')->unsigned();

            $table->string('placa', 120);
            $table->string('marca', 120);
            $table->string('observacion', 120);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('fk_id_usuario')->references('id')->on('users');
            $table->foreign('fk_id_tipo_vehiculo')->references('id')->on('parqueadero_tr_tipo_vehiculo');
            $table->foreign('fk_id_tercero')->references('id')->on('global_tm_terceros');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parqueadero_tm_vehiculo');
    }
}
