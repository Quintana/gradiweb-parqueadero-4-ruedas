<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GlobalTmTerceros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_tm_terceros', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('fk_id_usuario')->unsigned();
            $table->integer('tipo_identificacion_id')->unsigned();
            $table->integer('naturaleza_id')->unsigned();

            $table->string('primer_nombre', 120)->nullable();
            $table->string('segundo_nombre', 120)->nullable();
            $table->string('primer_apellido', 120)->nullable();
            $table->string('segundo_apellido', 120)->nullable();
            $table->string('razon_social', 120)->nullable();
            $table->string('nro_identificacion', 120)->unique();
            $table->string('telefono', 120)->nullable();
            $table->string('direccion', 120)->nullable();
            $table->string('correo_electronico', 120)->nullable();
            $table->string('nro_celular', 120)->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('fk_id_usuario')->references('id')->on('users');
            $table->foreign('tipo_identificacion_id')->references('id')->on('global_tr_tipo_identificacion');
            $table->foreign('naturaleza_id')->references('id')->on('global_tr_naturaleza');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_tm_terceros');
    }
}
