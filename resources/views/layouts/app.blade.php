@php
    if(!auth()->guest()){
        $data = DB::table('users as us')->select('us.nombres', 'us.apellidos')->where('us.id', auth()->user()->id)->first();
    }
@endphp
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Parqueadero 4 Ruedas</title>

    <!-- BOOTSTRAP 3.3.7 -->
    <link href="{{ asset('library/bootstrap-3.3.7-dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- DATATABLE -->
    <link href="{{ asset('library/dataTable-1.10.15/datatables.min.css') }}" rel="stylesheet">
    <!-- FONT-AWESOME -->
    <link href="{{ asset('library/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Toastr style -->
    <link href="{{ asset('css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
    <link rel="stylesheet" href="{{ asset('library/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">

    <!-- Gritter -->
    <link href="{{ asset('js/plugins/gritter/jquery.gritter.css') }}" rel="stylesheet">

    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <!--CSS PARA LAS NOTIFICACIONES-->
    <link rel="stylesheet" href="{{ asset('library/notificaciones/themes/alertify.core.css') }}" />
    <link rel="stylesheet" href="{{ asset('library/notificaciones/themes/alertify.default.css') }}" />

    <!--CSS UPLOADFILE, DROPZONE Y BLOCKUI-->
    <link rel="stylesheet" href="{{ asset('library/uploadfile/uploadfile.css') }}">
    <link rel="stylesheet" href="{{ asset('library/dropzone/dropzone.css') }}">
    <link rel="stylesheet" href="{{ asset('library/blockUI/blockUI.css') }}">
    <link rel="stylesheet" href="{{ asset('library/jquery.multifile.min/css/jquery.multifile.min.css') }}">

    <link rel="stylesheet" href="{{asset('library/jQuery-File-Upload-9.19.1/css/jquery.fileupload.css')}}">

    <!-- JQUERY UI -->
    <link rel="stylesheet" href="{{asset('library/jquery-ui-1.12.1/jquery-ui.min.css')}}">

    <!-- LIBRERIA DE SUBIR ARCHIVOS DE BOOTSTRAP -->
    <link href="{{asset('library/kartik-v-bootstrap-fileinput/css/fileinput.min.css')}}" media="all" rel="stylesheet" type="text/css" />

    @yield('css')

</head>
<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">Bienvenido(a)</strong>
                                @if(!auth()->guest())
                                    </span> <span class="text-muted text-xs block">{{$data->nombres}} {{$data->apellidos}} <b class="caret"></b></span> </span> </a>
                                @elseif(auth()->guest())
                                    </span> <span class="text-muted text-xs block"> Invitado <b class="caret"></b></span> </span> </a>
                                @endif
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        Salir
                                    </a>
                                </li>
                            </ul>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                        <div class="logo-element">
                            LOGO
                        </div>
                    </li>
<!--AQUI VA EL INCLUDE PARA EL MENU-->
                    @include('layouts.menu')
                </ul>

            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <span class="m-r-sm text-muted welcome-message">Bienvenido(a) a Parqueadero 4 Ruedas</span>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i> Salir
                            </a>
                        </li>
                    </ul>

                </nav>
            </div>
            <div class="wrapper wrapper-content fadeInRight">
                @yield('content')
            </div>
            <div class="footer">
                <div>
                    <strong>Copyright</strong> &copy; {{date("Y")}} - Todos los derechos Reservados
                </div>
            </div>

        </div>
    </div>

    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <!-- JQUERY UI -->
    <script type="text/javascript" src="{{asset('library/jquery-ui-1.12.1/jquery-ui.min.js')}}"></script>
    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('library/bootstrap-3.3.7-dist/js/bootstrap.min.js') }}"></script>

    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('library/dataTable-1.10.15/datatables.min.js') }}"></script>

    <!--libreria propia de SIACH-->
    <script type="text/javascript" src="{{ asset('js/jquery.httpRequest_plugin.js') }}"></script>

    <!-- Mainly scripts -->
    <script type="text/javascript" src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script type="text/javascript" src="{{ asset('js/inspinia.js') }}"></script>
    <!--PLUGIN QUE PARA DIBUJAR DATEPICKER-->
    <script type="text/javascript" src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

    <!--JS DE LAS NOTIFICACIONES-->
    <script type="text/javascript" src="{{ asset('library/notificaciones/lib/alertify.js') }}"></script>

    <!--JS DE UPLOADFILES, DROPZONE, BLOCKUI-->
    <script type="text/javascript" src="{{ asset('library/uploadfile/jquery.uploadfile.js') }}"></script>
    <script type="text/javascript" src="{{ asset('library/dropzone/dropzone.js') }}"></script>
    <script type="text/javascript" src="{{ asset('library/blockUI/jquery.blockUI.js') }}"></script>



    <!--librerias de cargue de archivos -->
    <script src="{{asset('library/jQuery-File-Upload-9.19.1/js/vendor/jquery.ui.widget.js')}}"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->

    <script type="text/javascript" src="{{asset('library/jQuery-File-Upload-9.19.1/js/jquery.iframe-transport.js')}}"></script>
    <!-- The basic File Upload plugin -->
    <script type="text/javascript" src="{{asset('library/jQuery-File-Upload-9.19.1/js/jquery.fileupload.js')}}"></script>
    <!-- The File Upload processing plugin -->
    <script type="text/javascript" src="{{asset('library/jQuery-File-Upload-9.19.1/js/jquery.fileupload-process.js')}}"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script type="text/javascript" src="{{asset('library/jQuery-File-Upload-9.19.1/js/jquery.fileupload-image.js')}}"></script>
    <!-- The File Upload audio preview plugin -->
    <script type="text/javascript" src="{{asset('library/jQuery-File-Upload-9.19.1/js/jquery.fileupload-audio.js')}}"></script>
    <!-- The File Upload video preview plugin -->
    <script type="text/javascript" src="{{asset('library/jQuery-File-Upload-9.19.1/js/jquery.fileupload-video.js')}}"></script>
    <!-- The File Upload validation plugin -->
    <script type="text/javascript" src="{{asset('library/jQuery-File-Upload-9.19.1/js/jquery.fileupload-validate.js')}}"></script>

    <!--CARGAMOS LA CDN YA QUE EN LA RUTA ELLA MISMA REDIRECCIONA A LA ULTIMA VERSION CONOCIDA
    DE TODOS MODOS HAY QUE ESTAR REVISANDO CONTINUAMENTE LAS ACTUALIZACIONES-->
    <script type="text/javascript" src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>


    <script src="{{asset('library/kartik-v-bootstrap-fileinput/js/plugins/piexif.min.js')}}" type="text/javascript"></script>
    <!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview.
        This must be loaded before fileinput.min.js -->
    <script src="{{asset('library/kartik-v-bootstrap-fileinput/js/plugins/sortable.min.js')}}" type="text/javascript"></script>
    <!-- purify.min.js is only needed if you wish to purify HTML content in your preview for
        HTML files. This must be loaded before fileinput.min.js -->
    <script src="{{asset('library/kartik-v-bootstrap-fileinput/js/plugins/purify.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('library/kartik-v-bootstrap-fileinput/js/fileinput.min.js')}}" type="text/javascript"></script>
    <!-- following theme script is needed to use the Font Awesome 5.x theme (`fas`) -->
    <!--< script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/themes/fas/theme.min.js"></script -->
    <!-- optionally if you need translation for your language then include the locale file as mentioned below (replace LANG.js with your language locale) -->
    <script src="{{asset('library/kartik-v-bootstrap-fileinput/js/locales/LANG.js')}}" type="text/javascript"></script>
    <script src="{{asset('library/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>


    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>

    <!-- activarmos tooltip -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover();
        })
    </script>

    @yield('js')
    @yield('js2')

</body>
</html>
