<div class="ibox-title">
    <input type="hidden" id="tipo_filtro">
    <div style="float: left;">
        <h3>Listado de tipos de veh&iacute;culos del sistema</h3>
    </div>

    <div class="ibox-content">
        <div class="row">
            <table id="listado" class="display responsive cell-border listado" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Descripci&oacute;n</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <div class="row">
        @include('parqueadero.tipo_vehiculo.modales_crud')
    </div>
</div>
