@extends('layouts.app')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h1 class="pull-left">DEMO - LECTURA DE ARREGLOS MULTIDIMENSIONALES</h1>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="descripcion">INGRESE UN ARREGLO DENTRO DE OTRO ARREGLO PARA SER PROCESADO</label>
                            <textarea rows="7" id="texto" name="texto" placeholder="Ingresar Aqui" class="form-control">
[
  ["2018-12-01","AM","ID123", 5000],
  ["2018-12-01","AM","ID545", 7000],
  ["2018-12-01","PM","ID545", 3000],
  ["2018-12-02","AM","ID545", 8000]
]
                            </textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-primary" id="enviar">PROCESAR INFORMACI&Oacute;N</button>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="respuesta">AQUI VA LA RESPUESTA</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @include('parqueadero.demo.js')
@endsection
