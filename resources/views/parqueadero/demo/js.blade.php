<script type="text/javascript">

    $(document).ready(function () {

        var arreglo = []; // variable que va a contener la informacion que venga del textarea
        var valor = '';
        var acumulaDatos = "";
        var acumulaDatos2 = "";
        var texto1 = "";
        var texto2 = "";
        var conta = 0;
        var json = "";
        var jsonDefinitivo = {};

        $(document).on('click', '#enviar', function () {
            //INICIALIZACION POR DEFECTO DE VARIABLES
            arreglo = JSON.parse( $("#texto").val() ); // convertimos el texto en arreglo para poder trabjarlo mejor
            valor = '';
            acumulaDatos = 0;
            conta = 0;
            json = '';
            // RECORREMOS EL ARREGLO POR PRIMERA VEZ PARA LUEGO IR COMPARANDO POSICION POR POSICION
            $.each(arreglo, function (idx, val) {
                if(valor !== val[0]) {
                    acumulaDatos = 0;
                    acumulaDatos2 = 0;
                    texto1 = 0;
                    texto2 = 0;
                    if (conta === 0) {
                        valor = val[0];

                        //RECORREMOS POR SEGUNDA VEZ EL ARREGLO COMPARANDO EL VALOR DE LAS POSICIONES
                        $.each(arreglo, function (idx2, val2) {

                            if (idx2 >= idx) {
                                if (valor === val2[0]) {
                                    if(val2[1] === "AM") {
                                        texto1 = val2[1];
                                        acumulaDatos += parseFloat(val2[3]);
                                    }else if(val2[1] === "PM"){
                                        texto2 = val2[1];
                                        acumulaDatos2 += parseFloat(val2[3]);
                                    }else{
                                        texto1 = null;
                                        acumulaDatos = null;
                                        texto2 = null;
                                        acumulaDatos2 = null;
                                    }
                                    conta++;
                                }
                            }
                        });
                        //ACUMULARMOS LOS VALORES OBTENIDOS EN CADA RECORRIDO EN UNA CADENA DE TEXTO QUE DESPUES SE CONVERTIRA EN JSON
                        json += '"'+valor+'" : { "'+texto1+'" : '+acumulaDatos+', "'+texto2+'": '+acumulaDatos2+' },';
                        conta = 0;
                    }
                }
            });

            // QUITAMOS LA ULTIMA COMA PARA QUE CUMPLA CON EL FORMATO ESTABLECIDO DE UN JSON
            json = JSON.parse('{'+json.slice(0,-1)+'}');

            //RECORREMOS EL JSON Y QUITAMOS LOS ELEMENTOS BASURA
            $.each(json, function (idx, val) {

                $.each(val, function (idx2, val2) {

                    if(idx2 == 0){
                        delete json[idx][idx2];
                    }

                })
            });


            //MOSTRAMOS EN EL ESPACIO ESTABLECIDO EL RESULTADO OBTENIDO
            $('#respuesta').empty().html('EL PROCESAMIENTO DE DATOS ARROJO LA SIGUIENTE INFORMACI&Oacute;N <br><br>'+JSON.stringify(json));

        });

    });

</script>
