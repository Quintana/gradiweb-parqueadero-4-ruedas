<div class="ibox-title">
    <input type="hidden" id="tipo_filtro">
    <div style="float: left;">
        <h3>Listado de Veh&iacute;culos y sus Propietarios</h3>
    </div>

    <div class="ibox-content">
        <div class="row">
            <table id="listado" class="display responsive cell-border listado" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Opciones</th>
                        <th>Placa Veh&iacute;culo</th>
                        <th>Marca</th>
                        <th>Tipo</th>
                        <th>Observaci&oacute;n</th>
                        <th>Nro identificaci&oacute;n</th>
                        <th>Tipo de Identificaci&oacute;n</th>
                        <th>Naturaleza</th>
                        <th>Primer Nombre</th>
                        <th>Segundo Nombre</th>
                        <th>Primer Apellido</th>
                        <th>Segundo Apellido</th>
                        <th>Raz&oacute;n Social</th>
                        <th>Tel&eacute;fono Fijo</th>
                        <th>Nro Celular</th>
                        <th>Direcci&oacute;n</th>
                        <th>Email</th>
                        <th>Usuario Creador</th>
                        <th>Fecha Creaci&oacute;n</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>


    <div class="row">
        @include('global.parametrizar.terceros.modales_crud')
    </div>
</div>
