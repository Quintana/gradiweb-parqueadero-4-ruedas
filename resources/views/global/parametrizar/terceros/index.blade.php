@extends('layouts.app')

@section('css')
    <style>
        .modal-xl {
            min-width: 90%;
        }
        .text-on-pannel {
            background: #fff none repeat scroll 0 0;
            height: auto;
            margin-left: 20px;
            padding: 5px 7px;
            position: absolute;
            margin-top: -47px;
            border: 1px solid #337ab7;
            border-radius: 8px;
        }

        .panel {
            /* for text on pannel */
            margin-top: 27px !important;
        }

        .panel-body {
            padding-top: 30px !important;
        }

        .container {
            width: 100% !important;
        }

    </style>
@endsection

@section('content')

    <!-- boton flotante -->
    <div class="row" id="small-chat">
        <div data-toggle="modal" data-target="#infoModal">
            <button id="mostrar" class="btn btn-circle btn-info btn-lg mostrar" data-toggle="tooltip" data-placement="top" title="Crear Registro">
                <i class="fa fa-plus"></i>
            </button>
        </div>
    </div>
    <!-- fin boton flotante -->

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h1 class="pull-left">Listado de Veh&iacute;culos y sus Propietarios</h1>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    @include('global.parametrizar.terceros.listado')
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @include('global.parametrizar.terceros.js')
@endsection
