<script type="text/javascript">

    $(document).ready(function () {

        var json = []; // definimos la variable vacia que va a contener los datos para dibujar el datatable
        var id = ''; // definimos la variable que va a contener el id para la actualizacion del registro
        var idElimina = ''; // definimos la variable que va a contener el id para la eliminacion del registro

        function dibujarDatatable(data) {
            json = []; // siempre que se llame a esta funcion el va a limpiar la variable para volverla a llenar con la informacion de la tabla
            /*recorremos los y definimos los datos para dibujar en el datatable asi mismo creamos el objeto "opciones" que contiene las diferentes acciones que se van a realizar por cada registro*/
            $.each(data.data, function (idx, val) {
                json.push({
                    id: val.id_vehiculo,
                    opciones:   '<div class="form-inline">' +
                                    '<div data-toggle="modal" class="form-group" data-target="#deleteModal">' +
                                        '<button id="delete" class="delete btn btn-danger btn-circle btn-outline" data-toggle="tooltip"' +
                                        ' data-placement="top" title="Eliminar Registro"' +
                                        ' data-val="' + val.id_vehiculo + '">' +
                                            '<span class="fa fa-trash"></span>' +
                                        '</button>' +
                                    '</div>&nbsp;' +
                                    '<div data-toggle="modal" class="form-group" data-target="#infoModal">' +
                                        '<button id="mostrar" class="mostrar btn btn-success btn-circle btn-outline" data-toggle="tooltip"' +
                                        ' data-placement="top" title="Editar informaci&oacute;n detallada "' +
                                        ' data-val="' + val.id_vehiculo + '">' +
                                            '<span class="fa fa-pencil"></span>' +
                                        '</button>' +
                                    '</div>&nbsp;' +
                                '</div>',
                    placa : val.placa ,
                    marca : val.marca ,
                    tipo_vehiculo : val.desc_tipo_vehiculo ,
                    observacion : val.obs_vehiculo,
                    nro_identificacion : val.nro_identificacion,
                    desc_tipo_identificacion : val.desc_tipo_identificacion ,
                    desc_naturaleza: val.desc_naturaleza,
                    primer_nombre: val.primer_nombre,
                    segundo_nombre: val.segundo_nombre,
                    primer_apellido: val.primer_apellido,
                    segundo_apellido: val.segundo_apellido,
                    razon_social: val.razon_social,
                    telefono: val.telefono,
                    nro_celular: val.nro_celular,
                    direccion: val.direccion,
                    correo_electronico: val.correo_electronico,
                    nombre_usuario: val.nombres + ' ' + val.apellidos,
                    fecha_creacion: val.created_at
                });
            });

            /*cabecera del datatable*/
            var columns = [
                {'data' : 'id'},
                {'data' : 'opciones'},
                {'data' : 'placa'},
                {'data' : 'marca'},
                {'data' : 'tipo_vehiculo'},
                {'data' : 'observacion'},
                {'data' : 'nro_identificacion'},
                {'data' : 'desc_tipo_identificacion'},
                {'data' : 'desc_naturaleza'},
                {'data' : 'primer_nombre'},
                {'data' : 'segundo_nombre'},
                {'data' : 'primer_apellido'},
                {'data' : 'segundo_apellido'},
                {'data' : 'razon_social'},
                {'data' : 'telefono'},
                {'data' : 'nro_celular'},
                {'data' : 'direccion'},
                {'data' : 'correo_electronico'},
                {'data' : 'nombre_usuario'},
                {'data' : 'fecha_creacion'},

            ];

            /*llamamos el plugin para dibujar los datos en el datatable segun el id o class asignado*/
            $('#listado').drawDataTable(json, columns);
        }

        /*FUNCION PARA GENERAR EL LISTADO DE CONSULTA DE LA TABLA*/
        function listado() {
            /*LLAMAMOS EL PLUGIN Y LE PASAOS LOS PARAMETROS PARA DIBUJAR EL DATATABLE*/
            $.httpRequest('/query/listado/tercero', {'blockUI': true}, function() {}, function (dataListado) {
                //console.log(dataListado);
                dibujarDatatable(dataListado); //enviamos los datos a la funcion anterior para procesar la informacion y mostrarla posteriormente
            });
        }

        listado(); // generamos la consulta para el listado de todos los datos de la tabla

        /*ACTIVAMOS EL EVENTO CLICK DE LA CLASS "MOSTRAR" Y IDENTIFICAMOS SI ES PARA INSERTAR O PARA ACTUALIZAR UN REGITRO*/
        $(document).on('click', '.mostrar', function() {
            $("#busqueda").val("");
            $('#spinner').show(); // activamos el spinner para que el usuario sepa que hay una carga de la informacion
            $('#mostrar_info').hide(); // ocultamos el div en donde se va a mostrar la informacion para actualizar o para insertar

            var actualizaRegistro = $(this).data('val'); // variable para distinguir si es para actualizar o crear un registro

            /* BUSCAMOS TODOS LOS DATOS DE LAS DIFERENTES TABLAS PARA MOSTRAR EN PANTALLA*/
            $.httpRequest('/query/listado/tipo_identificacion', {}, function() {}, function (dataTipoIdentificacion) {
                $.httpRequest('/query/listado/tipo_naturaleza', {}, function () {}, function (dataTipoNaturaleza) {
                    $.httpRequest('/query/listado/tipo_vehiculo', {}, function() {}, function (dataTipoVehiculo) {
                        $('#tipo_identificacion_id').empty();
                        $('#naturaleza_id').empty();
                        $('#fk_id_tipo_vehiculo').empty();

                        $('<option>').val('').text('Seleccione...').appendTo('#tipo_identificacion_id');
                        $.each(dataTipoIdentificacion.data, function (idx, val) {
                            $('<option>').val(val.id).text(val.sigla + ' - ' + val.descripcion).appendTo('#tipo_identificacion_id');
                        });

                        $('<option>').val('').text('Seleccione...').appendTo('#naturaleza_id');
                        $.each(dataTipoNaturaleza.data, function (idx, val) {
                            $('<option>').val(val.id).text(val.sigla + ' - ' + val.descripcion).appendTo('#naturaleza_id');
                        });

                        $('<option>').val('').text('Seleccione...').appendTo('#fk_id_tipo_vehiculo');
                        $.each(dataTipoVehiculo.data, function (idx, val) {
                            $('<option>').val(val.id).text(val.descripcion).appendTo('#fk_id_tipo_vehiculo');
                        });

                        // validamos que sea para actualizar la informacion sino entra al else para la insercion de la informacion
                        if(actualizaRegistro){ // actualizamos la informacion
                            $('#enviar_guardar').val('act');
                            $.httpRequest('/query/listado/tercero/' + actualizaRegistro, {}, function() {}, function (data) {
                                id = data.data[0].id_vehiculo;
                                $('#spinner').hide();
                                $('#mostrar_info').show();
                                /*DATOS DEL TERCERO, CLIENTE O PROPIETARIO*/
                                $('#tipo_identificacion_id > option[value="' + data.data[0].tipo_identificacion_id + '"]').attr('selected', 'selected');
                                $('#naturaleza_id > option[value="' + data.data[0].naturaleza_id + '"]').attr('selected', 'selected');
                                $('#primer_nombre').val(data.data[0].primer_nombre);
                                $('#segundo_nombre').val(data.data[0].segundo_nombre);
                                $('#primer_apellido').val(data.data[0].primer_apellido);
                                $('#segundo_apellido').val(data.data[0].segundo_apellido);
                                $('#razon_social').val(data.data[0].razon_social);
                                $('#nro_identificacion').val(data.data[0].nro_identificacion);
                                $('#telefono').val(data.data[0].telefono);
                                $('#nro_celular').val(data.data[0].nro_celular);
                                $('#direccion').val(data.data[0].direccion);
                                $('#correo_electronico').val(data.data[0].correo_electronico);

                                /*DATOS DEL VEHICULOS*/
                                $('#fk_id_tipo_vehiculo > option[value="' + data.data[0].fk_id_tipo_vehiculo + '"]').attr('selected', 'selected');
                                $('#placa').val(data.data[0].placa);
                                $('#placa_autocomplete').val(data.data[0].id_vehiculo);
                                $('#marca').val(data.data[0].marca);
                                $('#obs_vehiculo').val(data.data[0].obs_vehiculo);
                            });
                        }else{ // insertamos la informacion
                            $('#enviar_guardar').val('new');
                            $('#spinner').hide();
                            $('#mostrar_info').show();
                            $('.borrar').val('');
                        }
                    });
                });
            });
        });
        /*FIN DE ACTIVAMOS EL EVENTO CLICK DE LA CLASS "MOSTRAR" Y IDENTIFICAMOS SI ES PARA INSERTAR O PARA ACTUALIZAR UN REGITRO*/

        /*GUARDAMOS O  ACTUALIZAMOS LA INFORMACION*/
        $(document).on('click', '#guardar', function () {
            /*VALIDAMOS QUE TODOS LOS ELEMNTOS HTML QUE TENGAN LA CLASE 'data-required' CONTENGAN DATOS DE LOS CONTRARIO
            * NO PODRA ACTUALIZAR NI INSERTAR*/
            var validarDatos = true;

            $(".data-required").each(function(){
                if($(this).val() == ''){
                    validarDatos = false;
                }
            });
            /*SIN DE LA VALIDACION*/

            if(validarDatos === true) {
                if ($('#enviar_guardar').val() === 'act') { // PROCESO DE ACTUALIZACION
                    json = {
                        '_token': "{{ csrf_token() }}",
                        'data': {
                            'id': id,
                            'tipo_identificacion_id': $('#tipo_identificacion_id').val(),
                            'naturaleza_id': $('#naturaleza_id').val(),
                            'primer_nombre': $('#primer_nombre').val(),
                            'segundo_nombre': $('#segundo_nombre').val(),
                            'primer_apellido': $('#primer_apellido').val(),
                            'segundo_apellido': $('#segundo_apellido').val(),
                            'nro_identificacion': $('#nro_identificacion').val(),
                            'razon_social': $('#razon_social').val(),
                            'telefono': $('#telefono').val(),
                            'nro_celular': $('#nro_celular').val(),
                            'direccion': $('#direccion').val(),
                            'correo_electronico': $('#correo_electronico').val(),

                            'placa': $('#placa').val(),
                            'marca': $('#marca').val(),
                            'fk_id_tipo_vehiculo': $('#fk_id_tipo_vehiculo').val(),
                            'observacion': $('#obs_vehiculo').val(),
                        }
                    };
                    $.httpRequest('/query/tercero', {'method': 'PATCH', 'dataJson': json}, function () {}, function (dataUpdated) {
                        /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                        listado();
                        alertify.success(dataUpdated[0].msj);
                    });
                } else if ($('#enviar_guardar').val() === 'new') { // PROCESO PARA LA CREACION DE UN REGISTRO
                    json = {
                        '_token': "{{ csrf_token() }}",
                        'data': {
                            'tipo_identificacion_id': $('#tipo_identificacion_id').val(),
                            'naturaleza_id': $('#naturaleza_id').val(),
                            'primer_nombre': $('#primer_nombre').val(),
                            'segundo_nombre': $('#segundo_nombre').val(),
                            'primer_apellido': $('#primer_apellido').val(),
                            'segundo_apellido': $('#segundo_apellido').val(),
                            'nro_identificacion': $('#nro_identificacion').val(),
                            'razon_social': $('#razon_social').val(),
                            'telefono': $('#telefono').val(),
                            'nro_celular': $('#nro_celular').val(),
                            'direccion': $('#direccion').val(),
                            'correo_electronico': $('#correo_electronico').val(),

                            'placa': $('#placa').val(),
                            'marca': $('#marca').val(),
                            'fk_id_tipo_vehiculo': $('#fk_id_tipo_vehiculo').val(),
                            'observacion': $('#obs_vehiculo').val(),
                        }
                    };

                    $.httpRequest('/query/tercero', {'method': 'POST', 'dataJson': json}, function () {}, function (dataInsert) {
                        /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                        listado();
                        alertify.success(dataInsert[0].msj);
                    });
                }
            }else{
                //alertify.warning('TODOS LOS CAMPOS SON OBLIGATORIOS');
                swal('ATENCION!!!', 'TODOS LOS CAMPOS CON EL * SON OBLIGATORIOS', "warning")
            }
        });
        /*FIN DE GUARDAMOS O  ACTUALIZAMOS LA INFORMACION*/

        /*ELIMINACION POR SOFTWARE DEL REGISTRO*/
        $(document).on('click', '.delete', function () {
            idElimina = $(this).data('val');
        });

        $(document).on('click', '#eliminar', function () {
            if(idElimina !== null){
                json = {
                    '_token': "{{ csrf_token() }}",
                    'data': {
                        'id': idElimina
                    }
                };

                $.httpRequest('/query/tercero', {'method': 'DELETE', 'dataJson': json}, function(){}, function (dataDelete) {
                    /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                    listado();
                    alertify.success(dataDelete[0].msj);
                });
            }
        });
        /*ELIMINACION POR SOFTWARE DEL REGISTRO*/





        /************************************AUTOCOMPLETAR PARA EL PRODUCTO************************************/
        var nombreCompleto = '';
        $('#busqueda').autocomplete({
            source: function( request, response ) {
                $.ajax( {
                    url: "/query/tercero_vehiculo/autocompletar",
                    method: "POST",
                    dataType: "json",
                    data: {'_token': "{{ csrf_token() }}", filtro: request.term},
                    success: function( data ) {
                        $('#placa_autocomplete').val("");
                        $('.borrar').val('');

                        var dataAutocomplete = [];
                        if(data.data.length === 0) {
                            $('#busqueda').val("");
                            //$('#placa_autocomplete').val("");
                            alertify.info('No existe ningun registro con la informacion que acaba de ingresar, porfavor verifique');
                        }else {
                            $.each(data.data, function (idx, val) {
                                /*VALIDAMOS SI EN LA CONSULTA VIENE UN PARAMETRO LLAMADO VALUE
                                * ESTE PARAMETRO INDICA QUE LA CONSULTA NO ARROJO RESULTADOS*/
                                // EN CASO DE QUE SI EXISTA INFORMACION DEL TERCERO QUE SE ESTA CONSULTANDO
                                nombreCompleto = val.primer_nombre + " " + val.segundo_nombre + " " + val.primer_apellido + " " + val.segundo_apellido;
                                if (val.razon_social) {
                                    nombreCompleto = val.razon_social;
                                }
                                dataAutocomplete.push(
                                    {
                                        id: val.id,
                                        value: val.nro_identificacion + ' | ' + nombreCompleto + ' | ' + val.placa + ' | ' + val.marca,
                                        nombre_completo: nombreCompleto,
                                        primer_nombre: val.primer_nombre,
                                        segundo_nombre: val.segundo_nombre,
                                        primer_apellido: val.primer_apellido,
                                        segundo_apellido: val.segundo_apellido,
                                        razon_social: val.razon_social,
                                        tipo_identificacion_id: val.tipo_identificacion_id,
                                        nro_identificacion: val.nro_identificacion,
                                        naturaleza_id: val.naturaleza_id,
                                        desc_naturaleza: val.desc_naturaleza,
                                        desc_tipo_identificacion: val.desc_tipo_identificacion,
                                        telefono: val.telefono,
                                        direccion: val.direccion,
                                        correo_electronico: val.correo_electronico,
                                        nro_celular: val.nro_celular,

                                        placa: val.placa,
                                        marca: val.marca,
                                        fk_id_tipo_vehiculo: val.fk_id_tipo_vehiculo,
                                        obs_vehiculo: val.obs_vehiculo,
                                    }
                                );

                            });
                        }
                        response( dataAutocomplete );
                    }
                });
            },
            minLength: 1,
            select: function( event, ui ) {
                $('#tipo_identificacion_id > option[value="' + ui.item.tipo_identificacion_id + '"]').attr('selected', 'selected');
                $('#naturaleza_id > option[value="' + ui.item.naturaleza_id + '"]').attr('selected', 'selected');
                //$('#placa_autocomplete').val(ui.item.id);
                $('#primer_nombre').val(ui.item.primer_nombre);
                $('#segundo_nombre').val(ui.item.segundo_nombre);
                $('#primer_apellido').val(ui.item.primer_apellido);
                $('#segundo_apellido').val(ui.item.segundo_apellido);
                $('#razon_social').val(ui.item.razon_social);
                $('#nro_identificacion').val(ui.item.nro_identificacion);
                $('#telefono').val(ui.item.telefono);
                $('#nro_celular').val(ui.item.nro_celular);
                $('#direccion').val(ui.item.direccion);
                $('#correo_electronico').val(ui.item.correo_electronico);

                $('#placa').val(ui.item.placa);
                $('#marca').val(ui.item.marca);
                $('#obs_vehiculo').val(ui.item.obs_vehiculo);
                $('#fk_id_tipo_vehiculo > option[value="' + ui.item.fk_id_tipo_vehiculo + '"]').attr('selected', 'selected');
            },
            change: function (event, ui) {
                if(!ui.item){
                    $('#busqueda').val("").focus();
                }
            }
        });
        /****************************** FIN DE AUTOCOMPLETAR PARA EL PRODUCTO**********************************/


    });

</script>
