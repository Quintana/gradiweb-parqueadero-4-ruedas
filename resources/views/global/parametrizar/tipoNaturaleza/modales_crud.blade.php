<!-- Modal para la creacion y actualizacion de la tabla -->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
    <div class="modal-dialog" style="min-width: 60%" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="col-md-12"></div>
            </div>

            <div class="modal-body">
                <div id="spinner" style="text-align: center">
                    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                    <span class="sr-only">Cargando...</span>
                </div>
                <div id="mostrar_info" style="display: none">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-8">
                                <label for="descripcion">Descripci&oacute;n</label>
                                <input type="text" id="descripcion" name="descripcion" placeholder="Ingrese una descripci&oacute;n" class="form-control borrar data-required">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="sigla">Sigla</label>
                                <input type="text" id="sigla" name="sigla" placeholder="Ingrese una Sigla" class="form-control borrar data-required">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer" id="footer">
                <button id="cerrar" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button class="btn btn-primary" id="guardar">Guardar</button>
                <input type="hidden" id="enviar_guardar">
                {{csrf_field()}}
            </div>
        </div>
    </div>
</div>

<!-- fin Modal para la creacion y actualizacion de la informacion de la tabla -->

<!-- Modal para la Eliminacion de la informacion de la tabla -->

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="col-md-12"></div>
            </div>

            <div class="modal-body">
                <h2>Oprima el boton <strong>'Continuar'</strong> SI Desea Proceder con la eliminaci&oacute;n del registro?</h2>
            </div>

            <div class="modal-footer" id="footer2">
                <button id="cerrar" class="btn btn-default" data-dismiss="modal">Salir</button>
                <button class="btn btn-danger" id="eliminar" data-dismiss="modal">Continuar</button>
                {{csrf_field()}}
            </div>
        </div>
    </div>
</div>
<!--FIN MODAL-->
