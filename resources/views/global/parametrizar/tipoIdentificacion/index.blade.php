@extends('layouts.app')

@section('css')
    <style>
        .modal-xl {
            min-width: 90%;
        }
    </style>
@endsection

@section('content')

    <!-- boton flotante -->
    <div class="row" id="small-chat">
        <div data-toggle="modal" data-target="#infoModal">
            <button id="mostrar" class="btn btn-circle btn-info btn-lg mostrar" data-toggle="tooltip" data-placement="top" title="Crear Registro">
                <i class="fa fa-plus"></i>
            </button>
        </div>


        <!--<button class="btn btn-circle btn-info btn-lg" data-toggle="tooltip" data-placement="top" title="Crear Registro">
            <i class="fa fa-plus"></i>
        </button>-->
    </div>
    <!-- fin boton flotante -->

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h1 class="pull-left">Listado de Tipo de Documentos del usuario</h1>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    @include('global.parametrizar.tipoIdentificacion.listado')
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @include('global.parametrizar.tipoIdentificacion.js')
@endsection
