<div class="ibox-title">
    <input type="hidden" id="tipo_filtro">
    <div style="float: left;">
        <h3>Listado de Men&uacute;s en el sistema</h3>
    </div>

    <div class="ibox-content">
        <div class="row">
            <table id="listado" class="display responsive cell-border listado" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Aplicativo</th>
                        <th>M&oacute;dulo</th>
                        <th>Descripci&oacute;n</th>
                        <th>URL</th>
                        <th>Descripcion Icono</th>
                        <th>Orden</th>
                        <th>Mostrar en men&uacute;</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>


    <div class="row">
        @include('global.menu.menu.modales_crud')
    </div>
</div>
