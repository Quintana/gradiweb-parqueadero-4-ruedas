<script type="text/javascript">

    $(document).ready(function () {

        var json = []; // definimos la variable vacia que va a contener los datos para dibujar el datatable
        var id = ''; // definimos la variable que va a contener el id para la actualizacion del registro
        var idElimina = ''; // definimos la variable que va a contener el id para la eliminacion del registro

        function dibujarDatatable(data) {
            json = []; // siempre que se llame a esta funcion el va a limpiar la variable para volverla a llenar con la informacion de la tabla
            /*recorremos los y definimos los datos para dibujar en el datatable asi mismo creamos el objeto "opciones" que contiene las diferentes acciones que se van a realizar por cada registro*/
            $.each(data.data, function (idx, val) {
                json.push({
                    id: val.id,
                    descripcion_aplicativo: val.descripcion_aplicativo,
                    descripcion: val.descripcion,
                    icono: val.icono,
                    orden: val.orden,
                    opciones:   '<div class="form-inline">' +
                                    '<div data-toggle="modal" class="form-group" data-target="#deleteModal">' +
                                        '<button id="delete" class="delete btn btn-danger btn-circle btn-outline" data-toggle="tooltip"' +
                                            ' data-placement="top" title="Eliminar Registro"' +
                                            ' data-val="' + val.id + '">' +
                                            '<span class="fa fa-trash"></span>' +
                                        '</button>' +
                                        '</div>&nbsp;' +
                                        '<div data-toggle="modal" class="form-group" data-target="#infoModal">' +
                                            '<button id="mostrar" class="mostrar btn btn-success btn-circle btn-outline" data-toggle="tooltip"' +
                                            ' data-placement="top" title="Editar informaci&oacute;n detallada "' +
                                            ' data-val="' + val.id + '">' +
                                            '<span class="fa fa-pencil"></span>' +
                                        '</button>' +
                                    '</div>&nbsp;' +
                                '</div>'
                });
            });

            /*cabecera del datatable*/
            var columns = [
                {'data' : 'id'},
                {'data' : 'descripcion_aplicativo'},
                {'data' : 'descripcion'},
                {'data' : 'icono'},
                {'data' : 'orden'},
                {'data' : 'opciones'}
            ];

            /*llamamos el plugin para dibujar los datos en el datatable segun el id o class asignado*/
            $('#listado_aplicativo').drawDataTable(json, columns);
        }

        /*FUNCION PARA GENERAR EL LISTADO DE CONSULTA DE LA TABLA*/
        function listado() {
            /*LLAMAMOS EL PLUGIN Y LE PASAOS LOS PARAMETROS PARA DIBUJAR EL DATATABLE*/
            $.httpRequest('/query/listado/modulos', {'blockUI': true}, function() {}, function (dataListado) {
                //console.log(dataListado);
                dibujarDatatable(dataListado); //enviamos los datos a la funcion anterior para procesar la informacion y mostrarla posteriormente
            });
        }

        listado(); // generamos la consulta para el listado de todos los datos de la tabla

        /*ACTIVAMOS EL EVENTO CLICK DE LA CLASS "MOSTRAR" Y IDENTIFICAMOS SI ES PARA INSERTAR O PARA ACTUALIZAR UN REGITRO*/
        $(document).on('click', '.mostrar', function() {
            $('#spinner').show(); // activamos el spinner para que el usuario sepa que hay una carga de la informacion
            $('#mostrar_info').hide(); // ocultamos el div en donde se va a mostrar la informacion para actualizar o para insertar

            var actualizaRegistro = $(this).data('val'); // variable para distinguir si es para actualizar o crear un registro

            $.httpRequest('/query/listado/aplicativos/', {}, function() {}, function (dataAplicativos) {
                $('#fk_id_aplicativo').empty();

                $('<option>').val('').text('Seleccione...').appendTo('#fk_id_aplicativo');
                $.each(dataAplicativos.data, function (idx, val) {
                    $('<option>').val(val.id).text(val.descripcion).appendTo('#fk_id_aplicativo');
                });

                $.httpRequest('/query/listado/modulos/' + actualizaRegistro, {}, function() {}, function (dataModulos) {
                    // validamos que sea para actualizar la informacion sino entra al else para la insercion de la informacion
                    if(actualizaRegistro){ // actualizamos la informacion
                        id = dataModulos.data[0].id;
                        $('#enviar_guardar').val('act');
                        $('#spinner').hide();
                        $('#mostrar_info').show();
                        $('#fk_id_aplicativo > option[value="' + dataModulos.data[0].fk_id_aplicativo + '"]').attr('selected', 'selected');
                        $('#descripcion').val(dataModulos.data[0].descripcion);
                        $('#icono').val(dataModulos.data[0].icono);
                        $('#orden').val(dataModulos.data[0].orden);
                    }else{ // insertamos la informacion
                        id = '';
                        $('#enviar_guardar').val('new');
                        $('#spinner').hide();
                        $('#mostrar_info').show();
                        $('.borrar').val('');
                    }
                });
            });
        });
        /*FIN DE ACTIVAMOS EL EVENTO CLICK DE LA CLASS "MOSTRAR" Y IDENTIFICAMOS SI ES PARA INSERTAR O PARA ACTUALIZAR UN REGITRO*/

        /*GUARDAMOS O  ACTUALIZAMOS LA INFORMACION*/
        $(document).on('click', '#guardar', function () {
            /*VALIDAMOS QUE TODOS LOS ELEMNTOS HTML QUE TENGAN LA CLASE 'data-required' CONTENGAN DATOS DE LOS CONTRARIO
            * NO PODRA ACTUALIZAR NI INSERTAR*/
            var validarDatos = true;

            $(".data-required").each(function(){
                if($(this).val() == ''){
                    validarDatos = false;
                }
            });
            /*SIN DE LA VALIDACION*/

            if(validarDatos === true) {
                if ($('#enviar_guardar').val() === 'act') { // PROCESO DE ACTUALIZACION
                    json = {
                        '_token': $('#footer input[name=_token]').val(),
                        'data': {
                            'id': id,
                            'fk_id_aplicativo': $('#fk_id_aplicativo').val(),
                            'descripcion': $('#descripcion').val(),
                            'icono': $('#icono').val(),
                            'orden': $('#orden').val()
                        }
                    };
                    $.httpRequest('/query/modulos', {'method': 'PATCH', 'dataJson': json}, function () {}, function (dataUpdated) {
                        /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                        listado();
                        alertify.success(dataUpdated[0].msj);
                    });
                } else if ($('#enviar_guardar').val() === 'new') { // PROCESO PARA LA CREACION DE UN REGISTRO
                    json = {
                        '_token': $('#footer input[name=_token]').val(),
                        'data': {
                            'fk_id_aplicativo': $('#fk_id_aplicativo').val(),
                            'descripcion': $('#descripcion').val(),
                            'icono': $('#icono').val(),
                            'orden': $('#orden').val()
                        }
                    };

                    $.httpRequest('/query/modulos', {'method': 'POST', 'dataJson': json}, function () {}, function (dataInsert) {
                        /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                        listado();
                        alertify.success(dataInsert[0].msj);
                    });
                }
            }else{
                alertify.warning('TODOS LOS CAMPOS SON OBLIGATORIOS')
            }
        });
        /*FIN DE GUARDAMOS O  ACTUALIZAMOS LA INFORMACION*/

        /*ELIMINACION POR SOFTWARE DEL REGISTRO*/
        $(document).on('click', '.delete', function () {
            idElimina = $(this).data('val');
        });

        $(document).on('click', '#eliminar', function () {
            if(idElimina !== null){
                json = {
                    '_token': $('#footer2 input[name=_token]').val(),
                    'data': {
                        'id': idElimina
                    }
                };

                $.httpRequest('/query/modulos', {'method': 'DELETE', 'dataJson': json}, function(){}, function (dataDelete) {
                    /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                    listado();
                    alertify.success(dataDelete[0].msj);
                });
            }
        });
        /*ELIMINACION POR SOFTWARE DEL REGISTRO*/

    });

</script>
