@extends('layouts.app')

@section('css')
    <style>
        .modal-xl {
            min-width: 90%;
        }
    </style>
@endsection

@section('content')
    <!-- boton flotante -->
    <div class="row" id="small-chat">
        <a href="{{ route('register') }}">
            <button class="btn btn-circle btn-info btn-lg" data-toggle="tooltip" data-placement="top" title="Crear usuario">
                <i class="fa fa-plus"></i>
            </button>
        </a>
    </div>
    <!-- fin boton flotante -->

    <!-- contenido -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Listado de Usuarios</h2></div>

                <div class="panel-body">
                    <div class="col-md-12">
                        <div style="text-align: center">
                            <i id="spinner" class="fa fa-spinner fa-spin fa-5x" style="display: none"></i>
                        </div>

                        <table id="listado" class="display responsive cell-border listado table-responsive" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Identificaci&oacute;n</th>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Tipo Identifica</th>
                                    <th>Direcci&oacute;n</th>
                                    <th>Email</th>
                                    <th>Tel&eacute;fono Fijo</th>
                                    <th>Tel&eacute;fono Movil</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- fin contenido -->

    <!-- Modal -->
    <div class="modal fade" id="infoUsuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Actualizar Informaci&oacute;n del usuario</h4>
                </div>
                <div class="modal-body">
                    @include('global.editarUsuario')
                </div>
                <div class="modal-footer" id="footer">
                    {{csrf_field()}}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="actualizar" class="btn btn-primary">Actualizar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal para la Eliminacion de la informacion de la tabla -->

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="col-md-12"></div>
                </div>

                <div class="modal-body">
                    <h2>Oprima el boton <strong>'Continuar'</strong> SI Desea Proceder con la eliminaci&oacute;n del registro?</h2>
                </div>

                <div class="modal-footer" id="footer2">
                    <button id="cerrar" class="btn btn-default" data-dismiss="modal">Salir</button>
                    <button class="btn btn-danger" id="eliminar" data-dismiss="modal">Continuar</button>
                    {{csrf_field()}}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="asignarMenu" tabindex="-1" role="dialog" aria-labelledby="asignarMenuLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="col-md-12"></div>
                </div>

                <div class="modal-body">
                    @include('global.asignarMenuUsuario')
                </div>

                <div class="modal-footer" id="footer2">
                    <button id="cerrar" class="btn btn-default" data-dismiss="modal">Salir</button>
                    <button class="btn btn-primary" id="asignar_menu">Continuar</button>
                    {{csrf_field()}}
                </div>
            </div>
        </div>
    </div>
    <!--FIN MODAL-->
@endsection

@section('js')
    @yield('js_usuario')
    @yield('js_menu_usuario')
    <script type="text/javascript">
        $('#spinner').show();
        $('#listado').hide();
        var idElimina = '';
        var adicionar_menu = '';

        function cargaUsuario() {
            $.httpRequest("/query/listado/usuarios/", {'blockUI': true}, function (){}, function (data) {
                var jsonListadoPredios = [];
                $.each(data.data, function (idx, val) {
                    jsonListadoPredios.push({
                        'id': val.id,
                        'identificacion': val.identificacion,
                        'nombres': val.nombres,
                        'apellidos': val.apellidos,
                        'des_tipo_identificacion': val.sigla_tipo_identificacion + ' - ' + val.des_tipo_identificacion,
                        'direccion': val.direccion,
                        'email': val.email,
                        'telefono_fijo': val.telefono_fijo,
                        'telefono_movil': val.telefono_movil,
                        'opciones': '<div class="form-inline">' +
                                        '<div data-toggle="modal" class="form-group" data-target="#deleteModal">'+
                                            '<button id="delete" class="btn btn-danger btn-circle btn-sm delete" data-toggle="tooltip"'+
                                                ' data-placement="top" title="Eliminar Registro"'+
                                                ' data-val="'+val.id+'">'+
                                                    '<i class="fa fa-trash"></i>'+
                                            '</button>'+
                                        '</div>&nbsp;' +
                                        '<div data-toggle="modal" class="form-group" data-target="#infoUsuario">'+
                                            '<button id="mostrar" class="btn btn-primary btn-circle btn-sm mostrar" data-toggle="tooltip"'+
                                                ' data-placement="top" title="Editar Informaci&oacute;n"'+
                                                ' data-val="'+val.id+'">'+
                                                    '<i class="fa fa-pencil"></i>'+
                                            '</button>'+
                                        '</div>&nbsp;' +
                                        '<div data-toggle="modal" class="form-group" data-target="#asignarMenu">'+
                                            '<button id="adicionar_menu" class="btn btn-info btn-circle btn-sm adicionar_menu" data-toggle="tooltip"'+
                                                ' data-placement="top" title="Asignar Men&uacute;"'+
                                                ' data-val="'+val.id+'">'+
                                                    '<i class="fa fa-cog"></i>'+
                                            '</button>'+
                                        '</div>&nbsp;' +
                                    '<div>'
                    });
                });

                /*CARGAMOS EL DATATABLE SEGUN LA INFORMACION PROCESADA ANTERIORMENTE */
                var jsonListado = [ //DATOS DE LA CABECERA
                    {'data': 'id'},
                    {'data': 'identificacion'},
                    {'data': 'nombres'},
                    {'data': 'apellidos'},
                    {'data': 'des_tipo_identificacion'},
                    {'data': 'direccion'},
                    {'data': 'email'},
                    {'data': 'telefono_fijo'},
                    {'data': 'telefono_movil'},
                    {'data': 'opciones'}
                ];
                $(".listado").drawDataTable(jsonListadoPredios, jsonListado, function () { //dibujamos el datatable
                    $('#spinner').hide();
                    $('#listado').show();
                    alertify.success('CARGA CORRECTA DE TODOS LOS USUARIOS');
                })
            });
        }

        cargaUsuario();

        /*ELIMINACION POR SOFTWARE DEL REGISTRO*/
        $(document).on('click', '.delete', function () {
            idElimina = $(this).data('val');
        });

        $(document).on('click', '#eliminar', function () {
            if(idElimina !== null){
                json = {
                    '_token': $('#footer2 input[name=_token]').val(),
                    'data': {
                        'id': idElimina
                    }
                };

                $.httpRequest('/query/usuarios', {'method': 'DELETE', 'dataJson': json}, function(){}, function (dataDelete) {
                    /*LISTAMOS NUEVAMENTE EL DATATABLE*/
                    cargaUsuario();
                    alertify.success(dataDelete[0].msj);
                });
            }
        });

        $(document).on('click', '.adicionar_menu', function () {
            cargar_todo_el_menu = '';
            cuenta_aplicativos = 0;
            cuenta_modulos = 0;
            cuenta_menu = 0;
            jsonMenuUsuarios = [];
            seleccionados = [];
            $('#spinner_menu').show();
            $('.tree').hide();
            adicionar_menu = $(this).data('val');
            $('#accionar_menu').trigger('click');
        });
    </script>
@endsection