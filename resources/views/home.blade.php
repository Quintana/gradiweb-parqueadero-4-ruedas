@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <form class="form-horizontal" method="POST">
                        @if(Session::has('message'))
                            <p class="alert alert-info">{{ Session::get('message') }}</p>
                        @endif
                    </form>
                    Bienvenido(a) a Parqueadero 4 Ruedas

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-body" style="text-align: center">
                                LOGO
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
