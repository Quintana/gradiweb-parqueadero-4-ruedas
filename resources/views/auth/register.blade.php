@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Registrar Usuario</h2></div>
                <div class="panel-body">

                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        @if(Session::has('message'))
                            <p class="alert alert-info">{{ Session::get('message') }}</p>
                        @endif
                        <div class="form-group">
                            <div class="row col-md-12">

                                <div class="{{ $errors->has('nombres') ? ' has-error' : '' }}">
                                    <div class="col-md-3">
                                        <label for="nombres" class="control-label">Nombres:</label>
                                        <input id="nombres" type="text" class="form-control" name="nombres" value="{{ old('nombres') }}" required>
                                        @if ($errors->has('nombres'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('nombres') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="{{ $errors->has('apellidos') ? ' has-error' : '' }}">
                                    <div class="col-md-3">
                                        <label for="apellidos" class="control-label">Apellidos:</label>
                                        <input id="apellidos" type="text" class="form-control" name="apellidos" value="{{ old('apellidos') }}" required>

                                        @if ($errors->has('apellidos'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('apellidos') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="{{ $errors->has('fk_id_tipo_identificacion') ? ' has-error' : '' }}">
                                    <div class="col-md-3">
                                        <label for="fk_id_tipo_identificacion" class="control-label">Tipo Identificaci&oacute;n:</label>
                                        <select id="fk_id_tipo_identificacion" name="fk_id_tipo_identificacion" class="form-control" value="{{ old('fk_id_tipo_identificacion') }}" required>
                                            <option value="">Seleccione...</option>
                                            <option value="1">TI - TARJETA DE IDENTIDAD</option>
                                            <option value="2">CC - CEDULA DE CIUDADANIA</option>
                                            <option value="3">CC - CEDULA DE EXTRANJERIA</option>
                                        </select>
                                        <!--<input id="fk_id_tipo_identificacion" type="number" class="form-control" name="fk_id_tipo_identificacion" value="{{ old('fk_id_tipo_identificacion') }}" required>-->

                                        @if ($errors->has('identificacion'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('identificacion') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="{{ $errors->has('identificacion') ? ' has-error' : '' }}">
                                    <div class="col-md-3">
                                        <label for="identificacion" class="control-label">Identificaci&oacute;n:</label>
                                        <input id="identificacion" type="number" class="form-control" name="identificacion" value="{{ old('identificacion') }}" required>

                                        @if ($errors->has('identificacion'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('identificacion') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                            </div>

                            <div class="row col-md-12">

                                <div class="{{ $errors->has('direccion') ? ' has-error' : '' }}">
                                    <div class="col-md-3">
                                        <label for="direccion" class="control-label">Direcci&oacute;n:</label>
                                        <input id="direccion" type="text" class="form-control" name="direccion" value="{{ old('direccion') }}" required>

                                        @if ($errors->has('direccion'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('direccion') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="{{ $errors->has('telefono_fijo') ? ' has-error' : '' }}">
                                    <div class="col-md-3">
                                        <label for="telefono_fijo" class="control-label">Tel&eacute;fono Fijo:</label>
                                        <input id="telefono_fijo" type="text" class="form-control" name="telefono_fijo" value="{{ old('telefono_fijo') }}" required>

                                        @if ($errors->has('telefono_fijo'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('telefono_fijo') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="{{ $errors->has('telefono_movil') ? ' has-error' : '' }}">
                                    <div class="col-md-3">
                                        <label for="telefono_movil" class="control-label">Tel&eacute;fono Movil:</label>
                                        <input id="telefono_movil" type="text" class="form-control" name="telefono_movil" value="{{ old('telefono_movil') }}" required>

                                        @if ($errors->has('telefono_movil'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('telefono_movil') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-md-3">
                                        <label for="email" class="control-label">Correo Electronico</label>
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                            </div>

                            <div class="row col-md-12">

                                <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <div class="col-md-3">
                                        <label for="password" class="control-label">Contrase&ntilde;a</label>
                                        <input id="password" type="password" class="form-control" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <label for="password-confirm" class="control-label">Confirmar Contrase&ntilde;a</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12" style="text-align: center">
                                <button type="submit" class="btn btn-primary">
                                    Guardar
                                </button>
                            </div>
                        </div>
                    </form>
                    <div id="small-chat">
                        <a href="{{ url('admin/usuarios') }}">
                            <button class="btn btn-circle btn-info btn-lg" data-toggle="tooltip" data-placement="top" title="Volver a listar usuario">
                                <i class="fa fa-reply"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
