<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Inicio de Sesi&oacute;n</title>
    <!-- Styles -->
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <!-- BOOTSTRAP 3.3.7 -->
    <link href="{{ asset('library/bootstrap-3.3.7-dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- FONT-AWESOME -->
    <link href="{{ asset('library/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
</head>

<body class="gray-bg">
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name">
                LOGO
            </h1>
        </div>
        <!-- <h3>Ingreso a la plataforma Gissela Contigo</h3>
       <p>Perfectly designed and precisely prepared admin theme with over 50 pages with extra new web app views.
            Continually expanded and constantly improved Inspinia Admin Them (IN+)
        </p>-->
        <form class="m-t" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="input-group m-b">
                    <span class="input-group-addon"><div class="fa fa-user"></div></span>
                    <input id="email" type="email" class="form-control" name="email" placeholder="Correo Electr&oacute;nico" value="{{ old('email') }}" required autofocus>
                </div>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="input-group m-b">
                    <span class="input-group-addon"><div class="fa fa-key"></div></span>
                    <input id="password" type="password" class="form-control" name="password" placeholder="Contrase&ntilde;a" required>
                </div>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recordarme
                    </label>
                </div>
            </div>
            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <a href="/password/reset"><small>Recuperar Contrase&ntilde;a?</small></a>
                    </label>
                </div>
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Ingresar</button><br><br>

            <!--<a href="/password/reset"><small>Forgot password?</small></a>
            <p class="text-muted text-center"><small>Do not have an account?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="{{ route('register') }}">Crear Usuario</a>-->
        </form>
        <!--<p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>-->
    </div>
</div>

<!-- Scripts -->
<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
<!-- Scripts -->
<script src="{{ asset('library/bootstrap-3.3.7-dist/js/bootstrap.min.js') }}"></script>

</body>

</html>
